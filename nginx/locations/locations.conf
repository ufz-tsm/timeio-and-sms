location /keycloak {
    proxy_pass http://keycloak:8080;
    proxy_redirect off;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $remote_addr;
    proxy_set_header X-Forwarded-Proto $scheme;
}

### SMS # START ###################################################### 

location /backend {
    proxy_pass http://backend:5000;
    proxy_set_header X-Forwarded-Host $host;
    proxy_set_header X-Forwarded-Server $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
}

location /cv {
    proxy_set_header X-Forwarded-Host $host;
    proxy_set_header X-Forwarded-Server $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_pass http://vocabulary:8000;
}

location / {
    proxy_pass http://sms-frontend:3000;
}

location /sms {
    rewrite ^ / redirect;
}

location /sms-attachments {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Host $http_host;

    proxy_connect_timeout 300;
    # Default is HTTP/1, keepalive is only enabled in HTTP/1.1
    proxy_http_version 1.1;
    proxy_set_header Connection "";
    chunked_transfer_encoding off;
    proxy_pass http://minio:9000;
}

location /static/ {
    alias /usr/share/nginx/html/static/;
}

location /idl {
    proxy_set_header X-Forwarded-Host $host;
    proxy_set_header X-Forwarded-Server $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_pass http://idl:8001;
}
### SMS # END ######################################################
        

### timeio # START ######################################################

# Object storage with minio
location /object-storage/ {
    proxy_pass http://object-storage.:9091;
    rewrite   ^/object-storage/(.*) /$1 break;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_redirect off;
    
    # To support websocket
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
}

location /visualization/ {
    if ($request_uri = "/visualization/login/generic_oauth") {
        set $test A;
    }
if ($http_referer ~ "^https?://([^/]+)/visualization/login") {
        set $test "${test}B";
    }
    if ($test = AB) {
        rewrite ^ /frontend/oidc/login/?next=/visualization/login/generic_oauth redirect;
    }
    proxy_set_header Host $http_host;
    proxy_pass http://visualization.:3000;
}

# Proxy Grafana Live WebSocket connections.
location /visualization/api/live/ {
    rewrite  ^/visualization/(.*)  /$1 break;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_set_header Host $http_host;
    proxy_pass http://visualization.:3000/api/live/;
}

location /tsmdl/ {
    proxy_pass http://tsmdl.:8000/;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_redirect off;
}

location /sta/ {
    proxy_pass http://frost.:8080/;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_redirect off;
}

location /frontend/ {
    proxy_pass http://frontend.:8000/;
    rewrite  ^/frontend/(.*)  /$1 break;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_redirect off;
}

location /frontend/static/ {
    alias /home/appuser/app/static/;
}

location /timeio {
    alias   /usr/share/nginx/html;
    index  index.html index.htm;
}

