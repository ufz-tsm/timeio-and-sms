#!/bin/bash

echo "only starting keycloak and keycloak database"
docker compose -f sms/docker-compose.yml --env-file ./sms/docker/env.dev up keycloak -d
docker compose -f timeIO/docker-compose.yml -f timeIO/docker-compose-dev.yml up keycloak -d

echo "wait one minute for keycloak to be fully started"
sleep 60

echo "set ssl_required to none to allow http auth"
docker compose -f sms/docker-compose.yml --env-file ./sms/docker/env.dev exec keycloak-db psql -U keycloak -c "update REALM set ssl_required='NONE';"
docker compose -f timeIO/docker-compose.yml -f timeIO/docker-compose-dev.yml exec keycloak-postgres psql -U keycloak -c "update REALM set ssl_required='NONE';"

echo "stopping keycloak and keycloak database"
docker compose -f sms/docker-compose.yml --env-file ./sms/docker/env.dev down
docker compose -f timeIO/docker-compose.yml -f timeIO/docker-compose-dev.yml down
