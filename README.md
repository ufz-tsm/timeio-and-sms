# timeio-and-sms

## Setup
- sichergehen dass auf den nätigen files Leseberechtigungen für alle Nutzenden gesetzt sind 
## Host
- `host:` https://rdm-test.intranet.ufz.de
- Darauf wird im folgenden bezug genommen
## Start
- you can stay in root directory
- first start keycloak
  - `docker compose up -d keycloak`
  - Make sure it runs using `docker compose logs -f keycloak`
    - you should see some something like `Listening on: http://0.0.0.0:8080`
- start the rest:
  - `docker compose up -d`
- visit the sensor management system:
  - `host`
- visit the timeIO:
  - `host`/landing 
- To log in, use
  - username: `user1`
  - password: `password` 
## Wichtige URLS
- SMS: `host`
- timeIO frontend: `host/frontend`
- timeIO object storage: `host/object-storage`
- timeIO visualization: `host/visualization`
- timeIO sta: `host/sta`

## Demo Workflow
### Create a thing
- visit: `host/frontend`
  - click on `Frontend`
- Click on `Sign in with Helmholtz AAI`
  - Log in using the dummy user (`user1`, `password`)
- In the `Csv parsers` row, click on `+Add`
- Fill in the form:
  - Project: `VO:Group1`
  - Name: `Parser CSV DemoFile`
  - Column delimiter: `,`
  - Number of headlines to exclude: 3
  - Number of footlines to exclude: 0
  - Timestamp column: 1
  - Timestamp format: `%Y/%m/%d %H:%M:%S`
  - `Save`
-   In the header menu "Home > TSM > Csv parser", click on `TSM`
- In the `Things` row, click on `+Add`
- Fill in the form:
  - Name: `Demo Thing`
  - Project: `VO:Group1`
  - Ingest Type: `SFTP`
  - Filename patter: `f`
  - Select parser type: `CsvParser`
  - Select parser: `Parser CSV DemoFile`
  - `Save`
- click on the thing you've just created
  - under `SFTP-Server Settings`, remember the `Username`
- You can log out, if you want to

### Upload Dummy Data
- visit: `host/object-storage`
- log in using: `minioadmin` (pw: `minioadmin`)
- In left menu, click on `Object Browser`
- You should see a Bucket with the `name` of the username (from the `SFTP-Server Settings` of the thing in the frontend)
  - click on that one
- In the top right, click on `Upload` > `Upload File`
  - select the `DemoFile.csv` provided in the root of this repository
- You can log out, if you want to

### Create a TSM Linking in the SMS
- visit: `host`
- click on the icon in the top right
  - Log in using the dummy user (`user1`, `password`)
- Agree to the pop-up
#### Creating a device
- In the `Device` card, click on the green `+` (to create a new device)
- Fill in the form:
  - Visibility: `Public` 
  - short name: `Demo Device`
  - Permission groups: `VO:Group1`
  - that's enough (you can add additional information, if you want to)
  - click `create` in the top right
    - Click on `Save anyway` (it's a demo device, it doesn't need a serial number)
- You're now in `BASIC DATA` tab (see top row) of the created device
- Click on the tab `MEASURED QUANTITIES`
- Click `Add MEASURED QUANTITY`
- Fill in the form:
  - Label: `Demo Measured Quantity 1`
  - Measured Quantity: `Abudance`
  - Click: `ADD`
#### Creating a configuration
- Click on the burger menu in the top left
- Click on `Configurations`
- Click on `New Configuration` top left
- Fill in the form:
  - Visibility: `Public`
  - Permission groups: `VO:Group1`
  - Label: `Demo configuration`
  - Click `Create`
- You're now in `BASIC DATA` tab (see top row) of the created configuration
#### Mount the device to the configuration
- Click on the tab `PLATFORMS AND DEVICES` (see top row) of the created configuration
- Click `Mount Platform or device` in the top right
  - In the first step `Select a date`
    - As `Select begin date` choose `2014-01-01 00:00` (Observations from DemoFile are from 2014) 
    - Click `Continue`
  - In the second step `Choose paren platform`
    - Click on `Configuration - Demo configuration`
    - Click `Continue`
  - In the third step `Select`
    - the `Devices` tab of this search is preselected
    - Click on `Search`
    - Click on the `Demo Device`
    - Click on `Confirm Selection`
  - In the fourth step `Add mount information`
    - Click `Continue`
  - In the fifth step `Submit`
    - Click `SUBMIT`
- You have mounted the device to the configuration
#### Add a static location to the configuration
-  CLick on the tab `LOCATIONS` (see top row) of the created configuration
- Click on `START STATIC LOCATION`
  - As `Begin date` choose `2014-01-01 00:00` (Observations from DemoFile are from 2014) 
  - Either type in `x` and `y` coordinates or click a location on the map.
  - Click on `ADD CURRENT USER` below the map
  - Click `Save`
- You have created a static location to your device
#### Adding a tsm linking to the configuration
- Click on the tab `TSM LINKINGS` (see top row) of the created configuration
- Click on `ADD TSM LINKING`
  - In the first step `Select devices and date ranges`
    - Click in the field
    - Click on the checkbox of `Demo Device`
    - Click out of the field
    - Under the field appeared `Select mounting date ranges`
      - Click on the checkbox of `Demo Device`
    - Click `Continue`
  - In the second step `Select measured quantities`
    - Click on the checkbox of `Measured quantities without linking`
    - Click `Continue`
  - In the third step `Add linking information`
    - Click on the card, where `Device` is `Demo Device`
    - Fill in the form:
      - Select endpoint: `timeIO`
      - Select datasource: `vo_group1_<...>`
      - Select thing: `Demo Thing`
      - Select datastream: `Demo Thing/0`
    - Click `Continue`
  - In the fourth step `Review and Submit`
    - Click `Submit`

