#!/usr/bin/env bash
DIR_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# mirror group permissions for others on all files recursively
# to allow container user 1000 to execute/read mounted files
chmod -R o=g "${DIR_SCRIPT}"
# start SMS first to create sms-docker-network
"${DIR_SCRIPT}/sms/dc.sh" up -d --pull always
# database needs to access sms-docker-network
"${DIR_SCRIPT}/timeIO/dc-with-dev.sh"  up -d --pull always
