#!/usr/bin/env bash
DIR_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${DIR_SCRIPT}/timeIO/dc-with-dev.sh"  down --remove-orphans -v -t 0
"${DIR_SCRIPT}/sms/dc.sh"  down --remove-orphans -v -t 0
