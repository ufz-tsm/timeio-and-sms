#!/usr/bin/env bash
DIR_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker compose -f "${DIR_SCRIPT}/docker-compose.yml" --env-file "${DIR_SCRIPT}/docker/env.dev" $@