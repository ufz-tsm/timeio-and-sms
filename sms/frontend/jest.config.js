/*
Web client of the Sensor Management System software developed within
the Helmholtz DataHub Initiative by GFZ and UFZ.

Copyright (C) 2020-2021
- Nils Brinckmann (GFZ, nils.brinckmann@gfz-potsdam.de)
- Marc Hanisch (GFZ, marc.hanisch@gfz-potsdam.de)
- Helmholtz Centre Potsdam - GFZ German Research Centre for
  Geosciences (GFZ, https://www.gfz-potsdam.de)

Parts of this program were developed within the context of the
following publicly funded projects or measures:
- Helmholtz Earth and Environment DataHub
  (https://www.helmholtz.de/en/research/earth_and_environment/initiatives/#h51095)

Licensed under the HEESIL, Version 1.0 or - as soon they will be
approved by the "Community" - subsequent versions of the HEESIL
(the "Licence").

You may not use this work except in compliance with the Licence.

You may obtain a copy of the Licence at:
https://gitext.gfz-potsdam.de/software/heesil

Unless required by applicable law or agreed to in writing, software
distributed under the Licence is distributed on an "AS IS" basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied. See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
module.exports = {
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/$1',
    '^~/(.*)$': '<rootDir>/$1',
    '^vue$': 'vue/dist/vue.common.js'
  },
  moduleFileExtensions: [
    'ts',
    'js',
    'vue',
    'json'
  ],
  transform: {
    '^.+\\.ts$': 'ts-jest',
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest'
  },
  // coverage is now off by default but can be collected by using:
  // --collectCoverage
  collectCoverage: false,
  collectCoverageFrom: [
    '<rootDir>/components/**/*.vue',
    '<rootDir>/mixins/**/*.vue',
    '<rootDir>/models/**/*.ts',
    '<rootDir>/modelUtils/**/*.ts',
    '<rootDir>/serializers/**/*.ts',
    '<rootDir>/utils/**/*.ts',
    '<rootDir>/devtools/**/*.ts',
    '<rootDir>/viewmodels/**/*.ts'
  ],
  testEnvironment: 'jsdom',
  // from the documentation:
  //
  // "After the worker has executed a test the memory usage of it is checked. If
  // it exceeds the value specified the worker is killed and restarted."
  // so we restrict the memory usage of a worker to maximum 20% of system RAM
  //
  // before it is restarted:
  workerIdleMemoryLimit: 0.2,
  // just use one worker to avoid memory problems
  maxWorkers: 1,
  verbose: true
}
