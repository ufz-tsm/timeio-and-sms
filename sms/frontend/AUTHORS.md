The following list contains all people (in alphabetical order), that committed
changes to the project.

  * Martin Abbrent (UFZ, martin.abbrent@ufz.de)
  * Kotyba Alhaj Taha (UFZ, kotyba.alhaj-taha@ufz.de)
  * Nils Brinckmann (GFZ, nils.brinckmann@gfz-potsdam.de)
  * Tim Eder (UFZ, tim.eder@ufz.de)
  * Marc Hanisch (GFZ, marc.hanisch@gfz-potsdam.de)
  * Tobias Kuhnert (UFZ, tobias.kuhnert@ufz.de)
