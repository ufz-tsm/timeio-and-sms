/**
 * @license
 * Web client of the Sensor Management System software developed within
 * the Helmholtz DataHub Initiative by GFZ and UFZ.
 *
 * Copyright (C) 2020-2023
 * - Nils Brinckmann (GFZ, nils.brinckmann@gfz-potsdam.de)
 * - Marc Hanisch (GFZ, marc.hanisch@gfz-potsdam.de)
 * - Tobias Kuhnert (UFZ, tobias.kuhnert@ufz.de)
 * - Maximilian Schaldach (UFZ, maximilian.schaldach@ufz.de)
 * - Helmholtz Centre Potsdam - GFZ German Research Centre for
 *   Geosciences (GFZ, https://www.gfz-potsdam.de)
 * - Helmholtz Centre for Environmental Research GmbH - UFZ
 *   (UFZ, https://www.ufz.de)
 *
 * Parts of this program were developed within the context of the
 * following publicly funded projects or measures:
 * - Helmholtz Earth and Environment DataHub
 *   (https://www.helmholtz.de/en/research/earth_and_environment/initiatives/#h51095)
 *
 * Licensed under the HEESIL, Version 1.0 or - as soon they will be
 * approved by the "Community" - subsequent versions of the HEESIL
 * (the "Licence").
 *
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://gitext.gfz-potsdam.de/software/heesil
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import { ConfigurationsTreeNode } from '@/viewmodels/ConfigurationsTreeNode'
import { IOffsets } from '@/utils/configurationInterfaces'

/**
 * sums the offsets of all nodes in an array
 *
 * @param {ConfigurationsTreeNode[]} nodes - the nodes to calculate the offsets from
 * @returns {IOffsets} an object containing all summed offsets
 */
export function sumOffsets (nodes: ConfigurationsTreeNode[]): IOffsets {
  const result = {
    offsetX: 0,
    offsetY: 0,
    offsetZ: 0
  }
  nodes.forEach((node) => {
    result.offsetX += node.unpack().offsetX
    result.offsetY += node.unpack().offsetY
    result.offsetZ += node.unpack().offsetZ
  })
  return result
}
