// due to https://github.com/nuxt/eslint-config/issues/192
module.exports = {
  rules: {
    'vue/multi-word-component-names': 'off'
  }
}
