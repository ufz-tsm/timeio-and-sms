#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0


"""Helper script to solve coding problems for the app."""

import pathlib

import click
import requests


@click.group()
def main():
    """Run the codeassist."""
    pass


@main.group()
def update():
    """Update something."""
    pass


@update.command()
def requirements():
    """Update the requirements.txt files for the backend."""
    requiremets_files = (pathlib.Path(".") / "src").glob("*requirements.txt")
    for requiremets_file in requiremets_files:
        with requiremets_file.open() as infile:
            lines = infile.readlines()
        result = []
        for line in lines:
            if not line.startswith("#") and line.strip():
                # A line may looks like "psycopg[binary,pool]==3.1.12  # fixed"
                if "==" in line:
                    splitted = line.split("==")
                    name = splitted[0]
                    extras = None
                    if "[" in name:
                        name, extras = name.split("[", 1)
                        extras = extras[:-1]
                    current_version = splitted[1].strip()
                    if "# fixed" in current_version:
                        updated_version = current_version
                    else:
                        response = requests.get(f"https://pypi.org/pypi/{name}/json")
                        response.raise_for_status()
                        lib_data = response.json()
                        updated_version = lib_data["info"]["version"]
                        out_name = name
                        if extras:
                            out_name = f"{name}[{extras}]"
                    result.append(f"{out_name}=={updated_version}\n")
                else:
                    result.append(line)
            else:
                result.append(line)
        with requiremets_file.open("w") as outfile:
            for line in result:
                outfile.write(line)


if __name__ == "__main__":
    main()
