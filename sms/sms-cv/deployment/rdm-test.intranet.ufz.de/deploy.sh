#!/usr/bin/env sh

# SPDX-FileCopyrightText: 2020
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

cd "$(dirname "$0")"
docker-compose pull && docker-compose up -d