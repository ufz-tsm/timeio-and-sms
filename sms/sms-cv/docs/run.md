<!--
SPDX-FileCopyrightText: 2020 - 2022
- Martin Abbrent <martin.abbrent@ufz.de>
- Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
- Norman Ziegner <norman.ziegner@ufz.de>
- Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)

SPDX-License-Identifier: HEESIL-1.0
-->

# How to Run Locally

There are multiple ways to run this server.  But we recommend to run it with Docker so that you 
all dependencies will be automatic installed. However you can still do it without docker.

## Run With Docker Compose

To run it with Compose you only need to type this command in your
terminal:
```
docker-compose up -d
```
To check the Logs:
```
docker-compose logs --follow
```

When you do changes to the models you have to create migration files to
update database schemas:
```
docker-compose exec web python manage.py makemigrations # generate migration instructions
docker-compose exec web python manage.py migrate # apply generated migration instructions

```

To Test connection you can visit the ping endpoint
[<host>/api/v1/ping](http://0.0.0.0:8000/cv/api/v1/ping) in your browser. You should see:

```
HTTP 200 OK
Allow: GET, OPTIONS
Content-Type: application/vnd.api+json
Vary: Accept
```
```json
{
    "data": {
        "status": "success",
        "message": "Hello, world!"
    }
}
```

> Note: you may have to change the host manually 

now you can visit the docs site:

   - With OepnAPI 
        `http://localhost:8000/cv/api/v1/swagger`
        
   - Redoc 
        `http://localhost:8000/cv/api/v1/redoc`
        
## Run Without Docker

To be able to use this API you need a database connection, which can be added direct to the [Django-setting](../src/cv/settings.py)
and then you have to install the dependencies in the [requirements](../src/requirements.txt) file in your 
virtual environment :

`pip install -r requirements.txt`

after that:

```
python3 manage.py migrate
python3 manage.py loaddata initial_data.json
```
you can now run it with this command:

`python3 manage.py runserver` 


## Initialize database
Run the following commands to initialize the database:
```shell
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata initial_data
python manage.py loaddata default_community_data
python manage.py loaddata hydro_community_data
```

Afterwards you have to create an admin user account:
* Option a:
  ```shell
  python manage.py createsuperuser
  ```
* Option b:
  ```shell
  python3 manage.py shell -c "from accounts.models import CustomUser; CustomUser.objects.create_superuser('admin', 'admin+sms-cv-test@ufz.de', 'admin')"
  ```
* Option c:
  ```python
  from django.contrib.auth.models import User
  User.objects.create_superuser('admin', 'admin@example.com', 'pass')
  ```
