<!--
SPDX-FileCopyrightText: 2020 - 2022
- Martin Abbrent <martin.abbrent@ufz.de>
- Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
- Jannes Breier <jannes.breier@gfz-potsdam.de>
- Norman Ziegner <norman.ziegner@ufz.de>
- Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
- Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)

SPDX-License-Identifier: HEESIL-1.0
-->

# controlled-vocabulary API for sensor metadata [![licenses](https://img.shields.io/badge/License-HEESIL-informational)](https://git.gfz-potsdam.de/id2/software/heesil/-/raw/master/LICENSE) [![version](https://img.shields.io/badge/version-v1.0-lightgrey.svg)](./README.md) [![python](https://img.shields.io/badge/python-3.6+-blue.svg?style=?style=plastic&logo=python)](#) [![openapi](https://img.shields.io/badge/OpenAPI-3.0-green.svg?style=?style=plastic&logo=openapi-initiative)](https://gitlab.hzdr.de/hub-terra/sms/sms-cv/-/jobs/artifacts/master/raw/docs/openapi.yml?job=generate-openapi-spec)

This is a Python/Django web application with RESTful API service to
manage controlled vocabularies for the sensor metadata management
system.  
The schema is inspired by the [ODM2](https://github.com/ODM2/ODM2)
vocabulary, and the vocabulary used by the [UFZ](https://www.ufz.de) in
the data management portals logger component.

## Curation concept/How to work with

If you want to curate your own vocabulary at GFZ please contact [Nils Brinckmann](mailto:nils.brinckmann@gfz-potsdam.de) to get access.

You can join an existing community or create your own to upload and manage your community specific vocabulary.  
New vocabulary in terms of definition uniqueness get their own root entry while existing ones (similar ones regarding its definition) must be mapped to the existing root entry (Synonym) to guarantee maintability and avoid duplicates.  
Your vocabulary can also be extended by existing vocabulary of the root or other communities using the tag functionality.  

Your community can soon be selected in the Sensor Management System (SMS) to serve your community specific wording to the SMS and thus allow a multi-tenancy capability based on the community.

## Built With

- [Python/Django](https://docs.djangoproject.com/en/3.0/intro/overview/)
- [Django-Rest-Framework DRF](https://www.django-rest-framework.org/)
- Inspired by [ODM2](https://github.com/ODM2/ODM2)
  [schema](https://github.com/ODM2/ODM2/blob/master/src/blank_schema_scripts/postgresql/ODM2_for_PostgreSQL.sql)


## Get started

You can take a look at the [how to run](./docs/run.md) steps. 

### Migration 

To generate migrations as SQL scripts, instead of running them against the database use `sqlmigrate`:

`python manage.py sqlmigrate <app_name> <migration_file_name> > migration.sql`

An example:

`python manage.py sqlmigrate app 0014_alter_contactrolecommunity_root > migration.sql`

## Schema

- Latest OpenAPI spec:
  <https://gitlab.hzdr.de/hub-terra/sms/sms-cv/-/jobs/artifacts/master/raw/docs/openapi.yml?job=generate-openapi-spec>
- Current postgres database DDL:
  <https://gitlab.hzdr.de/hub-terra/sms/sms-cv/-/jobs/artifacts/master/raw/sql/sms-cv-ddl.sql?job=generate-plain-sql>
- Current initial dataset as postgres DML:
  <https://gitlab.hzdr.de/hub-terra/sms/sms-cv/-/jobs/artifacts/master/raw/sql/sms-cv-data.sql?job=generate-plain-sql>


## Requirements

* Python 3.6+
* Docker

## License

This project will be licensed under
[HEESIL](https://git.gfz-potsdam.de/id2/software/heesil/-/raw/master/LICENSE)
in the future.
