<!--
SPDX-FileCopyrightText: 2023 - 2024
- Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
- Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
- Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)

SPDX-License-Identifier: HEESIL-1.0
-->
## 1.3.0 (unreleased)

Added:
- RDF output for measured quantities

## 1.2.0 - 2024-03-25

Added:
- Due date and model specific labels for the automatic gitlab issue creation

## 1.1.0 - 2024-01-18

Added:
- django-simple-history plugin to keep track of all the changes within the CV

Fixed:
- made select lists for site usage for site types editable for super users

## 1.0.0 - 2023-10-20
