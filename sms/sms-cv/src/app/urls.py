# SPDX-FileCopyrightText: 2020 - 2023
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Urls for the sms-cv app."""

from django.conf import settings
from django.urls import include, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.routers import DefaultRouter
from rest_framework_extensions.routers import ExtendedSimpleRouter

from .views import ActionCategoryCommunityRelationshipView  # community views
from .views import (
    ActionCategoryCommunityViewSet,
    ActionCategoryRelationshipView,
    ActionCategoryViewSet,
    ActionTypeCommunityRelationshipView,
    ActionTypeCommunityViewSet,
    ActionTypeRelationshipView,
    ActionTypeViewSet,
    AggregationTypeCommunityRelationshipView,
    AggregationTypeCommunityViewSet,
    AggregationTypeRelationshipView,
    AggregationTypeViewSet,
    CommunityRelationshipView,
    CommunityViewSet,
    CompartmentCommunityRelationshipView,
    CompartmentCommunityViewSet,
    CompartmentRelationshipView,
    CompartmentViewSet,
    ContactRoleCommunityRelationshipView,
    ContactRoleCommunityViewSet,
    ContactRoleRelationshipView,
    ContactRoleViewSet,
    CountryRelationshipView,
    CountryViewSet,
    EquipmentStatusCommunityRelationshipView,
    EquipmentStatusCommunityViewSet,
    EquipmentStatusRelationshipView,
    EquipmentStatusViewSet,
    EquipmentTypeCommunityRelationshipView,
    EquipmentTypeCommunityViewSet,
    EquipmentTypeRelationshipView,
    EquipmentTypeViewSet,
    GlobalProvenanceRelationshipView,
    GlobalProvenanceViewSet,
    LicenseRelationshipView,
    LicenseViewSet,
    ManufacturerCommunityRelationshipView,
    ManufacturerCommunityViewSet,
    ManufacturerRelationshipView,
    ManufacturerViewSet,
    MeasuredQuantityCommunityRelationshipView,
    MeasuredQuantityCommunityViewSet,
    MeasuredQuantityRelationshipView,
    MeasuredQuantityUnitRelationshipView,
    MeasuredQuantityUnitViewSet,
    MeasuredQuantityViewSet,
    PlatformTypeCommunityRelationshipView,
    PlatformTypeCommunityViewSet,
    PlatformTypeRelationshipView,
    PlatformTypeViewSet,
    SamplingMediumCommunityRelationshipView,
    SamplingMediumCommunityViewSet,
    SamplingMediumRelationshipView,
    SamplingMediumViewSet,
    SiteTypeCommunityRelationshipView,
    SiteTypeCommunityViewSet,
    SiteTypeRelationshipView,
    SiteTypeViewSet,
    SiteUsageCommunityRelationshipView,
    SiteUsageCommunityViewSet,
    SiteUsageRelationshipView,
    SiteUsageViewSet,
    SoftwareTypeCommunityRelationshipView,
    SoftwareTypeCommunityViewSet,
    SoftwareTypeRelationshipView,
    SoftwareTypeViewSet,
    UnitCommunityRelationshipView,
    UnitCommunityViewSet,
    UnitRelationshipView,
    UnitViewSet,
)
from .views.ping import ping

api_router = DefaultRouter()
action_categories = api_router.register("actioncategories", ActionCategoryViewSet)
action_types = api_router.register("actiontypes", ActionTypeViewSet)
aggregation_types = api_router.register("aggregationtypes", AggregationTypeViewSet)
global_provenances = api_router.register("globalprovenances", GlobalProvenanceViewSet)
compartments = api_router.register("compartments", CompartmentViewSet)
roles = api_router.register("contactroles", ContactRoleViewSet)
countries = api_router.register("countries", CountryViewSet)
equipment_status = api_router.register("equipmentstatus", EquipmentStatusViewSet)
equipment_types = api_router.register("equipmenttypes", EquipmentTypeViewSet)
licenses = api_router.register("licenses", LicenseViewSet)
manufacturers = api_router.register("manufacturers", ManufacturerViewSet)
measured_quantity_units = api_router.register(
    "measuredquantityunits", MeasuredQuantityUnitViewSet
)
measured_quantities = api_router.register("measuredquantities", MeasuredQuantityViewSet)
platform_types = api_router.register("platformtypes", PlatformTypeViewSet)
sampling_medium = api_router.register("samplingmedia", SamplingMediumViewSet)
site_types = api_router.register("sitetypes", SiteTypeViewSet)
site_usages = api_router.register("siteusages", SiteUsageViewSet)
software_types = api_router.register("softwaretypes", SoftwareTypeViewSet)
units = api_router.register("units", UnitViewSet)


# community views
# simple routes
action_categories_communities = api_router.register(
    "actioncategories-communities", ActionCategoryCommunityViewSet
)
action_types_communities = api_router.register(
    "actiontypes-communities", ActionTypeCommunityViewSet
)
aggregation_types_communities = api_router.register(
    "aggregationtypes-communities", AggregationTypeCommunityViewSet
)
compartments_communities = api_router.register(
    "compartments-communities", CompartmentCommunityViewSet
)
roles_communities = api_router.register(
    "contactroles-communities", ContactRoleCommunityViewSet
)
equipment_status_communities = api_router.register(
    "equipmentstatus-communities", EquipmentStatusCommunityViewSet
)
equipment_types_communities = api_router.register(
    "equipmenttypes-communities", EquipmentTypeCommunityViewSet
)
manufacturers_communities = api_router.register(
    "manufacturers-communities", ManufacturerCommunityViewSet
)
measured_quantities_communities = api_router.register(
    "measuredquantities-communities", MeasuredQuantityCommunityViewSet
)
platform_types_communities = api_router.register(
    "platformtypes-communities", PlatformTypeCommunityViewSet
)
sampling_medium_communities = api_router.register(
    "samplingmedia-communities", SamplingMediumCommunityViewSet
)
site_types_communities = api_router.register(
    "sitetypes-communities", SiteTypeCommunityViewSet
)
site_usages_communities = api_router.register(
    "siteusages-communities", SiteUsageCommunityViewSet
)
software_types_communitites = api_router.register(
    "softwaretypes-communities", SoftwareTypeCommunityViewSet
)
units_communities = api_router.register("units-communities", UnitCommunityViewSet)

# # nested routes community specific
nested_api_router = ExtendedSimpleRouter()

community_routes = nested_api_router.register(
    r"communities", CommunityViewSet, basename="community"
)
community_routes.register(
    r"actioncategories",
    ActionCategoryCommunityViewSet,
    basename="community-actioncategories",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"actiontypes",
    ActionTypeCommunityViewSet,
    basename="community-actiontypes",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"aggregationtypes",
    AggregationTypeCommunityViewSet,
    basename="community-aggregationtypes",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"compartments",
    CompartmentCommunityViewSet,
    basename="community-compartments",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"contactroles",
    ContactRoleCommunityViewSet,
    basename="community-contactroles",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"equipmentstatus",
    EquipmentStatusCommunityViewSet,
    basename="community-equipmentstatus",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"equipmenttypes",
    EquipmentTypeCommunityViewSet,
    basename="community-equipmenttypes",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"manufacturers",
    ManufacturerCommunityViewSet,
    basename="community-manufacturers",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"measuredquantities",
    MeasuredQuantityCommunityViewSet,
    basename="community-measuredquantities",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"platformtypes",
    PlatformTypeCommunityViewSet,
    basename="community-platformtypes",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"samplingmedia",
    SamplingMediumCommunityViewSet,
    basename="community-samplingmedia",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"sitetypes",
    SiteTypeCommunityViewSet,
    basename="community-sitetypes",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"siteusages",
    SiteUsageCommunityViewSet,
    basename="community-siteusages",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"softwaretypes",
    SoftwareTypeCommunityViewSet,
    basename="community-softwaretypes",
    parents_query_lookups=["community_id"],
)
community_routes.register(
    r"units",
    UnitCommunityViewSet,
    basename="community-units",
    parents_query_lookups=["community_id"],
)

api_router.registry.extend(nested_api_router.registry)

schema_view = get_schema_view(
    openapi.Info(
        title="Controlled Vocabulary API",
        default_version="v0.1",
        description="A Python/Django REST API for managing Controlled Vocabularies.",
        terms_of_service="#",
        contact=openapi.Contact(email="kotyba.alhaj-taha@ufz.de"),
        license=openapi.License(name="MIT"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    re_path(r"^", include(api_router.urls)),
    # url(r"^", include(nested_api_router.urls)),
    # action_category
    re_path(
        r"^actioncategories/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        ActionCategoryViewSet.as_view({"get": "retrieve_related"}),
        name="action_category-related",
    ),
    re_path(
        r"^actioncategories/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        ActionCategoryRelationshipView.as_view(),
        name="action_category-relationships",
    ),
    # action_type
    re_path(
        r"^actiontypes/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        ActionTypeViewSet.as_view({"get": "retrieve_related"}),
        name="action_type-related",
    ),
    re_path(
        r"^actiontypes/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        ActionTypeRelationshipView.as_view(),
        name="action_type-relationships",
    ),
    # aggregation_type
    re_path(
        r"^aggregationtypes/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        AggregationTypeViewSet.as_view({"get": "retrieve_related"}),
        name="aggregation_type-related",
    ),
    re_path(
        r"^aggregationtypes/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        AggregationTypeRelationshipView.as_view(),
        name="aggregation_type-relationships",
    ),
    # global_provenances
    re_path(
        r"^globalprovenances/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        GlobalProvenanceViewSet.as_view({"get": "retrieve_related"}),
        name="global_provenance-related",
    ),
    re_path(
        r"^globalprovenances/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        GlobalProvenanceRelationshipView.as_view(),
        name="global_provenance-relationships",
    ),
    # countries
    re_path(
        r"^countries/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        CountryViewSet.as_view({"get": "retrieve_related"}),
        name="country-related",
    ),
    re_path(
        r"^countries/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        CountryRelationshipView.as_view(),
        name="country-relationships",
    ),
    # compartments
    re_path(
        r"^compartments/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        CompartmentViewSet.as_view({"get": "retrieve_related"}),
        name="compartment-related",
    ),
    re_path(
        r"^compartments/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        CompartmentRelationshipView.as_view(),
        name="compartment-relationships",
    ),
    # contact roles
    re_path(
        r"^contactroles/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        ContactRoleViewSet.as_view({"get": "retrieve_related"}),
        name="contact_roles-related",
    ),
    re_path(
        r"^contactroles/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        ContactRoleRelationshipView.as_view(),
        name="contact_roles-relationships",
    ),
    # equipment_status
    re_path(
        r"^equipmentstatus/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        EquipmentStatusViewSet.as_view({"get": "retrieve_related"}),
        name="equipment_status-related",
    ),
    re_path(
        r"^equipmentstatus/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        EquipmentStatusRelationshipView.as_view(),
        name="equipment_status-relationships",
    ),
    # equipment_type
    re_path(
        r"^equipmenttypes/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        EquipmentTypeViewSet.as_view({"get": "retrieve_related"}),
        name="equipment_type-related",
    ),
    re_path(
        r"^equipmenttypes/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        EquipmentTypeRelationshipView.as_view(),
        name="equipment_type-relationships",
    ),
    # licenses
    re_path(
        r"^licenses/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        LicenseViewSet.as_view({"get": "retrieve_related"}),
        name="license-related",
    ),
    re_path(
        r"^licenses/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        LicenseRelationshipView.as_view(),
        name="license-relationships",
    ),
    # manufacturers
    re_path(
        r"^manufacturers/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        ManufacturerViewSet.as_view({"get": "retrieve_related"}),
        name="manufacturer-related",
    ),
    re_path(
        r"^manufacturers/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        ManufacturerRelationshipView.as_view(),
        name="manufacturer-relationships",
    ),
    # community
    re_path(
        r"^communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        CommunityViewSet.as_view({"get": "retrieve_related"}),
        name="community-related",
    ),
    re_path(
        r"^communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        CommunityRelationshipView.as_view(),
        name="community-relationships",
    ),
    # measured_quantity_unit
    re_path(
        r"^measuredquantityunits/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        MeasuredQuantityUnitViewSet.as_view({"get": "retrieve_related"}),
        name="measured_quantity_unit-related",
    ),
    re_path(
        r"^measuredquantityunits/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        MeasuredQuantityUnitRelationshipView.as_view(),
        name="measured_quantity_unit-relationships",
    ),
    # measured_quantity
    re_path(
        r"^measuredquantities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        MeasuredQuantityViewSet.as_view({"get": "retrieve_related"}),
        name="measured_quantity-related",
    ),
    re_path(
        r"^measuredquantities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        MeasuredQuantityRelationshipView.as_view(),
        name="measured_quantity-relationships",
    ),
    # platform_types
    re_path(
        r"^platformtypes/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        PlatformTypeViewSet.as_view({"get": "retrieve_related"}),
        name="platform_type-related",
    ),
    re_path(
        r"^platformtypes/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        PlatformTypeRelationshipView.as_view(),
        name="platform_type-relationships",
    ),
    # sampling_media
    re_path(
        r"^samplingmedia/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        SamplingMediumViewSet.as_view({"get": "retrieve_related"}),
        name="sampling_medium-related",
    ),
    re_path(
        r"^samplingmedia/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        SamplingMediumRelationshipView.as_view(),
        name="sampling_medium-relationships",
    ),
    # site_types
    re_path(
        r"^sitetypes/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        SiteTypeViewSet.as_view({"get": "retrieve_related"}),
        name="site_type-related",
    ),
    re_path(
        r"^sitetypes/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        SiteTypeRelationshipView.as_view(),
        name="site_type-relationships",
    ),
    # site_usages
    re_path(
        r"^siteusages/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        SiteUsageViewSet.as_view({"get": "retrieve_related"}),
        name="site_usage-related",
    ),
    re_path(
        r"^siteusages/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        SiteUsageRelationshipView.as_view(),
        name="site_usage-relationships",
    ),
    # software_types
    re_path(
        r"^softwaretypes/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        SoftwareTypeViewSet.as_view({"get": "retrieve_related"}),
        name="software_type-related",
    ),
    re_path(
        r"^softwaretypes/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        SoftwareTypeRelationshipView.as_view(),
        name="software_type-relationships",
    ),
    # units
    re_path(
        r"^units/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        UnitViewSet.as_view({"get": "retrieve_related"}),
        name="unit-related",
    ),
    re_path(
        r"^units/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        UnitRelationshipView.as_view(),
        name="unit-relationships",
    ),
    # action_categories_communities
    re_path(
        r"^actioncategories-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        ActionCategoryCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="action_category_community-related",
    ),
    re_path(
        r"^actioncategories-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        ActionCategoryCommunityRelationshipView.as_view(),
        name="action_category_community-relationships",
    ),
    # action_type_communities
    re_path(
        r"^actiontypes-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        ActionTypeCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="action_type_community-related",
    ),
    re_path(
        r"^actiontypes-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        ActionTypeCommunityRelationshipView.as_view(),
        name="action_type_community-relationships",
    ),
    # aggregation_type_communities
    re_path(
        r"^aggregationtypes-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        AggregationTypeCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="aggregation_type_community-related",
    ),
    re_path(
        r"^aggregationtypes-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        AggregationTypeCommunityRelationshipView.as_view(),
        name="aggregation_type_community-relationships",
    ),
    # compartments_communities
    re_path(
        r"^compartments-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        CompartmentCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="compartment_community-related",
    ),
    re_path(
        r"^compartments-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        CompartmentCommunityRelationshipView.as_view(),
        name="compartment_community-relationships",
    ),
    # contact-roles_communities
    re_path(
        r"^contactroles-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        ContactRoleCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="contact_roles_community-related",
    ),
    re_path(
        r"^contactroles-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        ContactRoleCommunityRelationshipView.as_view(),
        name="contact_roles_community-relationships",
    ),
    # equipment_status_communities
    re_path(
        r"^equipmentstatus-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        EquipmentStatusCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="equipment_status_community-related",
    ),
    re_path(
        r"^equipmentstatus-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        EquipmentStatusCommunityRelationshipView.as_view(),
        name="equipment_status_community-relationships",
    ),
    # equipment_type_communities
    re_path(
        r"^equipmenttypes-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        EquipmentTypeCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="equipment_type_community-related",
    ),
    re_path(
        r"^equipmenttypes-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        EquipmentTypeCommunityRelationshipView.as_view(),
        name="equipment_type_community-relationships",
    ),
    # manufacturers_communities
    re_path(
        r"^manufacturers-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        ManufacturerCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="manufacturer_community-related",
    ),
    re_path(
        r"^manufacturers-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        ManufacturerCommunityRelationshipView.as_view(),
        name="manufacturer_community-relationships",
    ),
    # measured_quantity_communities
    re_path(
        r"^measuredquantities-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        MeasuredQuantityCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="measured_quantity_community-related",
    ),
    re_path(
        r"^measuredquantities-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        MeasuredQuantityCommunityRelationshipView.as_view(),
        name="measured_quantity_community-relationships",
    ),
    # platform_types_communities
    re_path(
        r"^platformtypes-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        PlatformTypeCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="platform_type_community-related",
    ),
    re_path(
        r"^platformtypes-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        PlatformTypeCommunityRelationshipView.as_view(),
        name="platform_type_community-relationships",
    ),
    # sampling_media_communities
    re_path(
        r"^samplingmedia-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        SamplingMediumCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="sampling_medium_community-related",
    ),
    re_path(
        r"^samplingmedia-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        SamplingMediumCommunityRelationshipView.as_view(),
        name="sampling_medium_community-relationships",
    ),
    # site_types_communities
    re_path(
        r"^sitetypes-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        SiteTypeCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="site_type_community-related",
    ),
    re_path(
        r"^sitetypes-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        SiteTypeCommunityRelationshipView.as_view(),
        name="site_type_community-relationships",
    ),
    # site_usages_communities
    re_path(
        r"^siteusages-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        SiteUsageCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="site_usage_community-related",
    ),
    re_path(
        r"^siteusages-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        SiteUsageCommunityRelationshipView.as_view(),
        name="site_usage_community-relationships",
    ),
    # software_types_communities
    re_path(
        r"^softwaretypes-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        SoftwareTypeCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="software_type_community-related",
    ),
    re_path(
        r"^softwaretypes-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        SoftwareTypeCommunityRelationshipView.as_view(),
        name="software_type_community-relationships",
    ),
    # units_communities
    re_path(
        r"^units-communities/(?P<pk>[^/.]+)/(?P<related_field>\w+)/$",
        UnitCommunityViewSet.as_view({"get": "retrieve_related"}),
        name="unit_community-related",
    ),
    re_path(
        r"^units-communities/(?P<pk>[^/.]+)/relationships/(?P<related_field>\w+)$",
        UnitCommunityRelationshipView.as_view(),
        name="unit_community-relationships",
    ),
    # check
    re_path(r"^ping", ping, name="ping"),
    re_path(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
]
if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
