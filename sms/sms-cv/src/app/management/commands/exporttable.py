#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0


import csv
import inspect
import sys

from django.core.management.base import BaseCommand
from django.db.models import Model as BaseModel
from app import models as app_models


class CsvExporter:
    """
    Exporter for csv.

    Easier way to have it ready for spreadsheets.
    """

    def export(self, queryset):
        """Export the content as csv to stdout."""
        model = queryset.model
        fieldnames = []
        for field in model._meta.fields:
            fieldnames.append(field.name)
        writer = csv.DictWriter(sys.stdout, fieldnames=fieldnames)
        writer.writeheader()
        for entry in queryset:
            as_dict = {}
            for field in fieldnames:
                as_dict[field] = getattr(entry, field)
            writer.writerow(as_dict)


class ModuleInspector:
    """Helper class to inspect what is inside of a module."""

    def __init__(self, module):
        """Init the object."""
        self.module = module

    def get_sub_classes(self, base_class):
        """Get all the classes of the module that are subclasses of the given base class."""
        result = []
        for entry in dir(self.module):
            python_object = getattr(self.module, entry)
            if inspect.isclass(python_object) and issubclass(python_object, base_class):
                result.append(python_object)
        return result


class Command(BaseCommand):
    """Command to export the tables."""

    # We support some formats (maybe we extend that later).
    formats = {
        "csv": CsvExporter,
    }
    # And we have a lookup for our models by a given model name.
    # Those are equal to the class names.
    # This extraction here uses the hack that all the models
    # of an app in django must be accessable from the <app>.models
    # module. (Otherwise we would not have migration support in django.)
    models = {
        x.__name__: x for x in ModuleInspector(app_models).get_sub_classes(BaseModel)
    }

    def add_arguments(self, parser):
        """
        Add the arguments that we want to handle.

        First one is the format of the output.
        Second one is the model that we want to export.
        """
        parser.add_argument("format", choices=self.formats.keys())
        parser.add_argument("model", choices=self.models.keys())

    def handle(self, *args, **kwargs):
        """Run the export."""
        exporter = self.formats[kwargs["format"]]()
        model = self.models[kwargs["model"]]
        queryset = model.objects.order_by("id").all()
        exporter.export(queryset)
