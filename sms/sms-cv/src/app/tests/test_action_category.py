# SPDX-FileCopyrightText: 2021 - 2023
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding
from rest_framework import status

from app.models import ActionCategory, GlobalProvenance
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase


class ActionCategoryTestCase(TestCase):
    list_url = reverse("actioncategory-list")

    def setUp(self):
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        ac = ActionCategory.objects.create(
            id=1,
            term="Test Action Category",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        ac.save()
        self.detail_url = reverse(
            "actioncategory-detail", kwargs={"pk": ActionCategory.pk}
        )

    def test_str(self):
        """
        ActionCategory
        :return:
        """
        ac = ActionCategory.objects.get(id=1)
        self.assertEqual(ac.term, "Test Action Category")

    def test_global_provenance(self):
        ac = ActionCategory.objects.get(id=1)
        self.assertEqual(ac.global_provenance_id, 1)

    def test_delete(self):
        """
        Ensure that we can't delete an action category using the views.
        """
        ac = ActionCategory.objects.get(id=1)
        url = reverse("actioncategory-detail", kwargs={"pk": ac.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_put(self):
        """
        Ensure that we can't change our data with using the views.
        """
        ac = ActionCategory.objects.get(id=1)
        url = reverse("actioncategory-detail", kwargs={"pk": ac.pk})
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        data = get_response.json()

        content_type = "application/vnd.api+json"
        put_response = self.client.put(url, data=data, content_type=content_type)
        self.assertEqual(put_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post(self):
        """
        Ensure that we can't change our data with using the views.
        """
        ac = ActionCategory.objects.get(id=1)
        url = reverse("actioncategory-detail", kwargs={"pk": ac.pk})
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        data = get_response.json()
        data["data"]["id"] = None

        # To make sure we don't run into unique constraints
        ac.delete()

        content_type = "application/vnd.api+json"
        post_response = self.client.post(
            self.list_url, data=data, content_type=content_type
        )
        self.assertEqual(post_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_all(self):
        """
        Ensure the result has all attributes in 'ActionCategory'
        """
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        cc = ActionCategory.objects.all()[0]
        expected = {
            "links": {
                "first": "http://testserver/api/v1/actioncategories/?page%5Bnumber%5D=1",
                "last": "http://testserver/api/v1/actioncategories/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "ActionCategory",
                    "id": encoding.force_str(cc.pk),
                    "attributes": {
                        "term": cc.term,
                        "definition": cc.definition,
                        "provenance": cc.provenance,
                        "provenance-uri": cc.provenance_uri,
                        "category": cc.category,
                        "note": cc.note,
                        "status": cc.status,
                        "requested-by-email": cc.requested_by_email,
                    },
                    "relationships": {
                        "global-provenance": {
                            "links": {
                                "self": "http://testserver/api/v1/actioncategories/1/relationships/global_provenance",
                                "related": "http://testserver/api/v1/actioncategories/1/global_provenance/",
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(cc.global_provenance_id),
                            },
                        },
                        "action-types": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "http://testserver/api/v1/actioncategories/1/relationships/action_types",
                                "related": "http://testserver/api/v1/actioncategories/1/action_types/",
                            },
                        },
                        "successor": {
                            "links": {
                                "self": "http://testserver/api/v1/actioncategories/1/relationships/successor",
                                "related": "http://testserver/api/v1/actioncategories/1/successor/",
                            }
                        },
                    },
                    "links": {"self": "http://testserver/api/v1/actioncategories/1/"},
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }

        assert expected == response.json()

    def test_sort(self):
        """
        test sort
        """
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """
        test admin interface
        """
        obj = ActionCategory.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        # test_admin.delete_view_responding()
