# SPDX-FileCopyrightText: 2021 - 2023
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

import copy

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding
from rest_framework import status

from app.models import (
    Community,
    Compartment,
    GlobalProvenance,
    SamplingMedium,
    SamplingMediumCommunity,
)
from app.serializers import SamplingMediumCommunitySerializer
from app.tests.Base_class import BaseTestCase


class SamplingMediumCommunityTestCase(TestCase):
    list_url = reverse("samplingmediumcommunity-list")

    def setUp(self):
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        co = Community.objects.create(
            id=1,
            term="Test Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
        )
        co.save()

        c = Compartment.objects.create(
            id=1,
            term="Test Compartment",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        c.save()

        sm = SamplingMedium.objects.create(
            id=1,
            term="Test Sampling Medium",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            compartment_id=1,
        )
        sm.save()

        smco = SamplingMediumCommunity.objects.create(
            id=1,
            term="Test Sampling Medium Community",
            abbreviation="TSMC",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            note="Test1",
            compartment_id=1,
            community_id=1,
            root_id=1,
        )
        smco.save()
        self.detail_url = reverse(
            "samplingmediumcommunity-detail", kwargs={"pk": SamplingMediumCommunity.pk}
        )

    def test_str(self):
        """
        SamplingMediumCommunity
        :return:
        """
        smco = SamplingMediumCommunity.objects.get(id=1)
        self.assertEqual(smco.term, "Test Sampling Medium Community")

    def test_serializer(self):
        """
        Test the serializer.

        It should just work fine with the given data.
        But the root & the community are both required.
        """
        smco = SamplingMediumCommunity.objects.get(id=1)
        data = SamplingMediumCommunitySerializer(smco, context={"request": None}).data
        data_without_root = copy.copy(data)
        data_without_root["root"] = None
        data_without_community = copy.copy(data)
        data_without_community["community"] = None

        # delete, so that we don't run into unique constraints
        smco.delete()

        serializer_ok = SamplingMediumCommunitySerializer(
            data=data, context={"request": None}
        )
        self.assertTrue(serializer_ok.is_valid())

        # We still don't save it.
        self.assertEqual(0, SamplingMediumCommunity.objects.count())

        serializer_without_root = SamplingMediumCommunitySerializer(
            data=data_without_root, context={"request": None}
        )
        self.assertFalse(serializer_without_root.is_valid())

        serializer_without_community = SamplingMediumCommunitySerializer(
            data=data_without_community, context={"request": None}
        )
        self.assertFalse(serializer_without_community.is_valid())

    def test_delete(self):
        """
        Ensure that we can't delete a sampling medium community using the views.
        """
        smco = SamplingMediumCommunity.objects.get(id=1)
        url = reverse("samplingmediumcommunity-detail", kwargs={"pk": smco.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_put(self):
        """
        Ensure that we can't change our data with using the views.
        """
        smco = SamplingMediumCommunity.objects.get(id=1)
        url = reverse("samplingmediumcommunity-detail", kwargs={"pk": smco.pk})
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        data = get_response.json()

        content_type = "application/vnd.api+json"
        put_response = self.client.put(url, data=data, content_type=content_type)
        self.assertEqual(put_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_filter_root_term(self):
        """Ensure that we can use the root.term filter."""
        sm = SamplingMedium.objects.get(id=1)
        resp_root_term_filter = self.client.get(reverse("samplingmediumcommunity-list"), {"filter[root.term]": sm.term})
        self.assertEqual(resp_root_term_filter.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp_root_term_filter.json()["data"]), 1)

    def test_post(self):
        """
        Ensure that we can't change our data with using the views.
        """
        smco = SamplingMediumCommunity.objects.get(id=1)
        url = reverse("samplingmediumcommunity-detail", kwargs={"pk": smco.pk})
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        data = get_response.json()
        data["data"]["id"] = None

        # To make sure we don't run into unique constraints
        smco.delete()

        content_type = "application/vnd.api+json"
        post_response = self.client.post(
            self.list_url, data=data, content_type=content_type
        )
        self.assertEqual(post_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_relationship(self):
        """
        Ensure that we can't delete using the relationship view.
        """
        smco = SamplingMediumCommunity.objects.get(id=1)
        url = reverse(
            "sampling_medium_community-relationships",
            kwargs={"pk": smco.pk, "related_field": "community"},
        )
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_put_relationship(self):
        """
        Ensure that we can't override using the relationship view.
        """
        smco = SamplingMediumCommunity.objects.get(id=1)
        url = reverse(
            "sampling_medium_community-relationships",
            kwargs={"pk": smco.pk, "related_field": "community"},
        )
        content_type = "application/vnd.api+json"
        response = self.client.put(url, data={}, content_type=content_type)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_post_relationship(self):
        """
        Ensure that we can't add using the relationship view.
        """
        smco = SamplingMediumCommunity.objects.get(id=1)
        url = reverse(
            "sampling_medium_community-relationships",
            kwargs={"pk": smco.pk, "related_field": "community"},
        )
        content_type = "application/vnd.api+json"
        response = self.client.post(url, data={}, content_type=content_type)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_compartment(self):
        smco = SamplingMediumCommunity.objects.get(id=1)
        self.assertEqual(smco.compartment_id, 1)
        self.assertEqual(smco.compartment.term, "Test Compartment")

    def test_root(self):
        smco = SamplingMediumCommunity.objects.get(id=1)
        self.assertEqual(smco.root_id, 1)

    def test_community(self):
        smco = SamplingMediumCommunity.objects.get(id=1)
        self.assertEqual(smco.community_id, 1)

    def test_get_all(self, url_name=None):
        """
        Ensure the result has all attributes in 'SamplingMediumCommunity'
        """
        smco = SamplingMediumCommunity.objects.first()
        if not url_name:
            path = self.list_url
        else:
            path = reverse(
                url_name, kwargs={"parent_lookup_community_id": smco.community_id}
            )
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(path)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": f"http://testserver{path}?page%5Bnumber%5D=1",
                "last": f"http://testserver{path}?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "SamplingMediumCommunity",
                    "id": encoding.force_str(smco.pk),
                    "attributes": {
                        "term": smco.term,
                        "abbreviation": smco.abbreviation,
                        "definition": smco.definition,
                        "provenance": smco.provenance,
                        "provenance-uri": smco.provenance_uri,
                        "note": smco.note,
                    },
                    "relationships": {
                        "compartment": {
                            "links": {
                                "self": "http://testserver/api/v1/samplingmedia-communities/1/relationships/compartment",
                                "related": "http://testserver/api/v1/samplingmedia-communities/1/compartment/",
                            },
                            "data": {
                                "type": "Compartment",
                                "id": encoding.force_str(smco.compartment_id),
                            },
                        },
                        "root": {
                            "links": {
                                "self": "http://testserver/api/v1/samplingmedia-communities/1/relationships/root",
                                "related": "http://testserver/api/v1/samplingmedia-communities/1/root/",
                            },
                            "data": {
                                "type": "SamplingMedium",
                                "id": encoding.force_str(smco.root_id),
                            },
                        },
                        "community": {
                            "links": {
                                "self": "http://testserver/api/v1/samplingmedia-communities/1/relationships/community",
                                "related": "http://testserver/api/v1/samplingmedia-communities/1/community/",
                            },
                            "data": {
                                "type": "Community",
                                "id": encoding.force_str(smco.community_id),
                            },
                        },
                    },
                    "links": {
                        "self": "http://testserver/api/v1/samplingmedia-communities/1/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """
        test sort
        """
        BaseTestCase().sort(self.list_url, self.client)

    def test_community_routes(self):
        """
        test community routes
        """
        self.test_get_all(url_name="community-samplingmedia-list")

    def test_community_filter(self):
        """
        filter for a communities term
        """
        response = self.client.get(
            self.list_url, data={"filter[community.term]": "Test Community"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)

    def test_rootstatus_filter(self):
        """
        filter for a root status
        """
        response = self.client.get(
            self.list_url, data={"filter[root.status]": "PENDING"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)
