# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Test cases for the util functions."""

from django.test import TestCase

from ..utils import kw_alias


class KwAliasTestCase(TestCase):
    """Test case for the kw_alias function."""

    def test_in_replace_function(self):
        """Ensure the kw_alias decorator works in an example replace function."""

        @kw_alias(
            {
                "the_string": "value_to_be_replaced",
                "with_value": "replacement",
                "in_text": "text",
            }
        )
        def replace(text, value_to_be_replaced, replacement):
            """Replace some text."""
            return text.replace(value_to_be_replaced, replacement)

        result = replace(
            the_string="foo", with_value="bar", in_text="example value is foo"
        )
        self.assertEqual(result, "example value is bar")
