# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Test classes for site usages."""
from django.conf import settings
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import GlobalProvenance, SiteUsage
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase


class SiteUsageTestCase(TestCase):
    """Test the site usages."""

    list_url = reverse("siteusage-list")

    def setUp(self):
        """Set some data up for the tests."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        su = SiteUsage.objects.create(
            id=1,
            term="Test SiteUsage",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        su.save()
        self.detail_url = reverse("siteusage-detail", kwargs={"pk": SiteUsage.pk})

    def test_str(self):
        """Test the conversion to string."""
        su = SiteUsage.objects.get(id=1)
        self.assertEqual(su.term, "Test SiteUsage")

    def test_global_provenance(self):
        """Test the global provenance relationship."""
        su = SiteUsage.objects.get(id=1)
        self.assertEqual(su.global_provenance_id, 1)

    def test_get_all(self):
        """Ensure the result has all attributes in 'SiteUsage'."""
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        su = SiteUsage.objects.all()[0]
        expected = {
            "links": {
                "first": f"http://testserver/{settings.CV_BASE_URL}api/v1/siteusages/?page%5Bnumber%5D=1",
                "last": f"http://testserver/{settings.CV_BASE_URL}api/v1/siteusages/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "SiteUsage",
                    "id": encoding.force_str(su.pk),
                    "attributes": {
                        "term": su.term,
                        "definition": su.definition,
                        "provenance": su.provenance,
                        "provenance-uri": su.provenance_uri,
                        "category": su.category,
                        "note": su.note,
                        "status": su.status,
                        "requested-by-email": su.requested_by_email,
                    },
                    "relationships": {
                        "global-provenance": {
                            "links": {
                                "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/siteusages/1/relationships/global_provenance",
                                "related": f"http://testserver/{settings.CV_BASE_URL}api/v1/siteusages/1/global_provenance/",
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(su.global_provenance_id),
                            },
                        },
                        "site-types": {
                            "links": {
                                "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/siteusages/1/relationships/site_types",
                                "related": f"http://testserver/{settings.CV_BASE_URL}api/v1/siteusages/1/site_types/",
                            },
                            "data": [],
                            "meta": {
                                "count": 0,
                            },
                        },
                        "successor": {
                            "links": {
                                "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/siteusages/1/relationships/successor",
                                "related": f"http://testserver/{settings.CV_BASE_URL}api/v1/siteusages/1/successor/",
                            }
                        },
                    },
                    "links": {
                        "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/siteusages/1/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }

        assert expected == response.json()

    def test_sort(self):
        """Test the sorted output."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """Test some admin interface functionality."""
        obj = SiteUsage.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
