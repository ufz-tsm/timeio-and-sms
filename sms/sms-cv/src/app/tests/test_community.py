# SPDX-FileCopyrightText: 2021 - 2023
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import Community, GlobalProvenance
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase


class CommunityTestCase(TestCase):
    list_url = reverse("community-list")

    def setUp(self):
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()
        m = Community.objects.create(
            id=1,
            term="Test Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
        )
        m.save()
        self.detail_url = reverse("community-detail", kwargs={"pk": Community.pk})

    def test_str(self):
        """
        Community
        :return:
        """
        m = Community.objects.get(id=1)
        self.assertEqual(m.term, "Test Community")

    def test_global_provenance(self):
        m = Community.objects.get(id=1)
        self.assertEqual(m.global_provenance_id, 1)

    def test_get_all(self):
        """
        Ensure the result has all attributes in 'Community'
        """
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        co = Community.objects.all()[0]
        expected = {
            "links": {
                "first": "http://testserver/api/v1/communities/?page%5Bnumber%5D=1",
                "last": "http://testserver/api/v1/communities/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "Community",
                    "id": encoding.force_str(co.pk),
                    "attributes": {
                        "term": co.term,
                        "definition": co.definition,
                        "provenance": co.provenance,
                        "provenance-uri": co.provenance_uri,
                        "category": co.category,
                        "note": co.note,
                        "status": co.status,
                        "requested-by-email": co.requested_by_email,
                    },
                    "relationships": {
                        "global-provenance": {
                            "links": {
                                "self": "http://testserver/api/v1/communities/1/relationships/global_provenance",
                                "related": "http://testserver/api/v1/communities/1/global_provenance/",
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(co.global_provenance_id),
                            },
                        }
                    },
                    "links": {"self": "http://testserver/api/v1/communities/1/"},
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """
        test sort
        """
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """
        test admin interface
        """
        obj = Community.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        test_admin.delete_view_responding()
