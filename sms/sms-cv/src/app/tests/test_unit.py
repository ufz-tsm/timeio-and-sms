# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Tests for the units."""

import datetime
from unittest.mock import patch

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding
from rest_framework import status

from app.auth.external import IdpUser
from app.models import GlobalProvenance, Unit
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase
from app.views.Base_viewset import BaseFilterViewSet


class FakeResponse:
    """Helper class for mocks of requests functions."""

    def __init__(self, status_code, json):
        """Init the object."""
        self.status_code = status_code
        self._json = json

    def json(self):
        """Return json."""
        return self._json()

    def raise_for_status(self):
        """Raise an exception if the status code is not good."""
        # Not needed to make a test here for our usage so far.
        pass


class UnitTestCase(TestCase):
    """Test the unit model & views."""

    list_url = reverse("unit-list")

    def setUp(self):
        """Set some test entries up in the table."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        self.u = Unit.objects.create(
            term="Test Unit",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        self.detail_url = reverse("unit-detail", kwargs={"pk": self.u.pk})

    def test_str(self):
        """Ensure our str method uses the term entry."""
        self.assertEqual(self.u.term, "Test Unit")

    def test_global_provenance(self):
        """Ensure that we have a global provenance entry."""
        self.assertEqual(self.u.global_provenance_id, 1)

    def test_get_all(self):
        """Ensure the result has all attributes in 'Unit'."""
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": "http://testserver/api/v1/units/?page%5Bnumber%5D=1",
                "last": "http://testserver/api/v1/units/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "Unit",
                    "id": encoding.force_str(self.u.pk),
                    "attributes": {
                        "term": self.u.term,
                        "definition": self.u.definition,
                        "provenance": self.u.provenance,
                        "provenance-uri": self.u.provenance_uri,
                        "category": self.u.category,
                        "note": self.u.note,
                        "status": self.u.status,
                        "requested-by-email": self.u.requested_by_email,
                    },
                    "relationships": {
                        "measured-quantity-units": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "http://testserver/api/v1/units/1/relationships/measured_quantity_units",
                                "related": "http://testserver/api/v1/units/1/measured_quantity_units/",
                            },
                        },
                        "global-provenance": {
                            "links": {
                                "self": "http://testserver/api/v1/units/1/relationships/global_provenance",
                                "related": "http://testserver/api/v1/units/1/global_provenance/",
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(self.u.global_provenance_id),
                            },
                        },
                        "successor": {
                            "links": {
                                "self": "http://testserver/api/v1/units/1/relationships/successor",
                                "related": "http://testserver/api/v1/units/1/successor/",
                            }
                        },
                    },
                    "links": {"self": "http://testserver/api/v1/units/1/"},
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Ensure we can sort."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """Test that we can test some admin interface functionality."""
        test_admin = TestAdminInterface().setUpAdmin(self.client, self.u)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        # test_admin.delete_view_responding()

    def test_post_no_login(self):
        """Ensure we can't post without user information."""
        example_payload = {"data": {"type": "Unit", "attributes": {"term": "newdummy"}}}
        response = self.client.post(
            self.list_url, data=example_payload, content_type="application/vnd.api+json"
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_with_user(self):
        """Ensure we can post with user information."""
        example_payload = {"data": {"type": "Unit", "attributes": {"term": "newdummy"}}}
        with override_settings(GITLAB_TOKEN="ABC"):
            with patch.object(
                BaseFilterViewSet.authentication_classes[0], "authenticate"
            ) as authenticate:
                with patch("requests.post") as post:
                    test_date = datetime.date(2024, 1, 1)
                    with patch("datetime.date") as date:
                        date.today.return_value = test_date
                        post.return_value = FakeResponse(status_code=200, json={})
                        authenticate.return_value = (
                            IdpUser({"email": "dummy.user@localhost"}),
                            None,
                        )
                        response = self.client.post(
                            self.list_url,
                            data=example_payload,
                            content_type="application/vnd.api+json",
                        )
                        post.assert_called_once()
                        called_url = post.call_args.args[0]
                        self.assertEqual(
                            called_url,
                            "https://codebase.helmholtz.cloud/api/v4/projects/3260/issues",
                        )
                        used_headers = post.call_args.kwargs["headers"]
                        self.assertEqual(used_headers["PRIVATE-TOKEN"], "ABC")
                        used_payload = post.call_args.args[1]
                        self.assertEqual(
                            used_payload["labels"], "SMS-CV,Suggestion,Unit"
                        )
                        self.assertEqual(used_payload["due_date"], "2024-01-29")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response_data = response.json()["data"]
        # The term is as we set
        self.assertEqual(response_data["attributes"]["term"], "newdummy")
        # The status needs to be PENDING
        self.assertEqual(response_data["attributes"]["status"], "PENDING")
        self.assertEqual(
            response_data["attributes"]["requested_by_email"], "dummy.user@localhost"
        )
