# SPDX-FileCopyrightText: 2022
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Tests for external authentification mechanisms."""

from unittest.mock import patch

from requests.exceptions import HTTPError
from rest_framework.test import APIRequestFactory, APITestCase

from app.auth.external import (
    HifisDevIdpAccessTokenAuthentification,
    HifisProdIdpAccessTokenAuthentification,
    IdpUser,
    SmsLocalApiKeyAuthenification,
    SmsUser,
)


class SuccessfullFakeResponse:
    """Class to fake requests responses."""

    def __init__(self, json_data, status_code, exception):
        """Init the fake object."""
        self.json_data = json_data
        self.status_code = status_code
        self.exception = exception

    def raise_for_status(self):
        """Raise an exception if one is given."""
        if self.exception:
            raise self.exception

    def json(self):
        """Return the json of the response."""
        return self.json_data


class TestIdpUser(APITestCase):
    """Tests for the IdpUser class."""

    def test_is_authenticated(self):
        """Ensure is_authenticated returns True."""
        user = IdpUser({})
        self.assertTrue(user.is_authenticated)

    def test_email(self):
        """Ensure we return the email."""
        user = IdpUser({"email": "test.email@localhost"})
        self.assertEqual(user.email, "test.email@localhost")

    def test_eq(self):
        """Test that the eq check can succeed."""
        user1 = IdpUser({"email": "test.email@localhost"})
        user2 = IdpUser({"email": "test.email@localhost"})
        self.assertEqual(user1, user2)

    def test_not_eq(self):
        """Test that the eq check can fail."""
        user1 = IdpUser({"email": "test.email@localhost"})
        user2 = IdpUser({"email": "not.test.email@localhost"})
        self.assertNotEqual(user1, user2)


class TestSmsUser(APITestCase):
    """Tests for the SmsUser class."""

    def test_is_authenticated(self):
        """Ensure is_authenticated returns True."""
        user = SmsUser({})
        self.assertTrue(user.is_authenticated)

    def test_email(self):
        """Ensure we return the email."""
        user = SmsUser({"attributes": {"subject": "test.email@localhost"}})
        self.assertEqual(user.email, "test.email@localhost")

    def test_eq(self):
        """Test that the eq check can succeed."""
        user1 = SmsUser({"attributes": {"subject": "test.email@localhost"}})
        user2 = SmsUser({"attributes": {"subject": "test.email@localhost"}})
        self.assertEqual(user1, user2)

    def test_not_eq(self):
        """Test that the eq check can fail."""
        user1 = SmsUser({"attributes": {"subject": "test.email@localhost"}})
        user2 = SmsUser({"attributes": {"subject": "no.test.email@localhost"}})
        self.assertNotEqual(user1, user2)


class TestHifisProdIdpAccessTokenAuthentification(APITestCase):
    """Tests for the HifisProdIdpAccessTokenAuthentification class."""

    def test_authenticate_no_header(self):
        """Ensure we don't return a user if we have no header at all."""
        auth_mech = HifisProdIdpAccessTokenAuthentification()
        fake_request = APIRequestFactory().post("/dummy")

        result = auth_mech.authenticate(fake_request)
        self.assertIsNone(result)

    def test_authenticate_with_header_no_bearer_token(self):
        """Ensure we don't return a user if there is no bearer token."""
        auth_mech = HifisProdIdpAccessTokenAuthentification()
        fake_request = APIRequestFactory().post("/dummy")

        # It seems that we can't set the headers with the factory.
        # However, we can still mock those.
        headers = {"Authorization": "abc"}
        fake_request.headers = headers

        result = auth_mech.authenticate(fake_request)
        self.assertIsNone(result)

    def test_authenticate_with_header_external_fail(self):
        """Ensure that we check the case that the external authorization fails."""
        auth_mech = HifisProdIdpAccessTokenAuthentification()
        fake_request = APIRequestFactory().post("/dummy")

        headers = {"Authorization": "Bearer XYZ"}
        fake_request.headers = headers

        with patch.object(auth_mech, "idp_config") as config_mock:
            config_mock.return_value = {
                "userinfo_endpoint": "https://login.helmholtz.de/oauth2/userinfo"
            }
            with patch("requests.get") as userinfo_mock:
                # Normally it is not the get that throws this exception.
                # but the raise_for_status.
                # However, we don't want to run the real request,
                # so we raise the exception earlier.
                userinfo_mock.side_effect = HTTPError(
                    "401 Client Error: Unauthorized for url"
                )
                result = auth_mech.authenticate(fake_request)
        self.assertIsNone(result)

    def test_authenticate_with_header_external_success(self):
        """Ensure that we can use the external authentication and get a user."""
        auth_mech = HifisProdIdpAccessTokenAuthentification()
        fake_request = APIRequestFactory().post("/dummy")

        headers = {"Authorization": "Bearer XYZ"}
        fake_request.headers = headers

        with patch.object(auth_mech, "idp_config") as config_mock:
            config_mock.return_value = {
                "userinfo_endpoint": "https://login.helmholtz.de/oauth2/userinfo"
            }
            with patch("requests.get") as userinfo_mock:
                # Normally it is not the get that throws this exception.
                # but the raise_for_status.
                # However, we don't want to run the real request,
                # so we raise the exception earlier.
                userinfo_mock.return_value = SuccessfullFakeResponse(
                    json_data={"email": "test@localhost"},
                    status_code=200,
                    exception=None,
                )
                result = auth_mech.authenticate(fake_request)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], IdpUser({"email": "test@localhost"}))


class TestHifisDevIdpAccessTokenAuthentification(APITestCase):
    """Tests for the HifisDevIdpAccessTokenAuthentification class."""

    def test_authenticate_no_header(self):
        """Ensure we don't return a user if we have no header at all."""
        auth_mech = HifisDevIdpAccessTokenAuthentification()
        fake_request = APIRequestFactory().post("/dummy")

        result = auth_mech.authenticate(fake_request)
        self.assertIsNone(result)

    def test_authenticate_with_header_no_bearer_token(self):
        """Ensure we don't return a user if there is no bearer token."""
        auth_mech = HifisDevIdpAccessTokenAuthentification()
        fake_request = APIRequestFactory().post("/dummy")

        # It seems that we can't set the headers with the factory.
        # However, we can still mock those.
        headers = {"Authorization": "abc"}
        fake_request.headers = headers

        result = auth_mech.authenticate(fake_request)
        self.assertIsNone(result)

    def test_authenticate_with_header_external_fail(self):
        """Ensure that we check the case that the external authorization fails."""
        auth_mech = HifisDevIdpAccessTokenAuthentification()
        fake_request = APIRequestFactory().post("/dummy")

        headers = {"Authorization": "Bearer XYZ"}
        fake_request.headers = headers

        with patch.object(auth_mech, "idp_config") as config_mock:
            config_mock.return_value = {
                "userinfo_endpoint": "https://login-dev.helmholtz.de/oauth2/userinfo"
            }
            with patch("requests.get") as userinfo_mock:
                # Normally it is not the get that throws this exception.
                # but the raise_for_status.
                # However, we don't want to run the real request,
                # so we raise the exception earlier.
                userinfo_mock.side_effect = HTTPError(
                    "401 Client Error: Unauthorized for url"
                )
                result = auth_mech.authenticate(fake_request)
        self.assertIsNone(result)

    def test_authenticate_with_header_external_success(self):
        """Ensure that we can use the external authentication and get a user."""
        auth_mech = HifisDevIdpAccessTokenAuthentification()
        fake_request = APIRequestFactory().post("/dummy")

        headers = {"Authorization": "Bearer XYZ"}
        fake_request.headers = headers

        with patch.object(auth_mech, "idp_config") as config_mock:
            config_mock.return_value = {
                "userinfo_endpoint": "https://login-dev.helmholtz.de/oauth2/userinfo"
            }
            with patch("requests.get") as userinfo_mock:
                # Normally it is not the get that throws this exception.
                # but the raise_for_status.
                # However, we don't want to run the real request,
                # so we raise the exception earlier.
                userinfo_mock.return_value = SuccessfullFakeResponse(
                    json_data={"email": "test@localhost"},
                    status_code=200,
                    exception=None,
                )
                result = auth_mech.authenticate(fake_request)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], IdpUser({"email": "test@localhost"}))


class TestSmsLocalApiKeyAuthenification(APITestCase):
    """Tests for the SmsLocalApiKeyAuthenification class."""

    def test_authenticate_no_header(self):
        """Ensure we don't return a user if we have no header at all."""
        auth_mech = SmsLocalApiKeyAuthenification()
        fake_request = APIRequestFactory().post("/dummy")

        result = auth_mech.authenticate(fake_request)
        self.assertIsNone(result)

    def test_authenticate_with_header_external_fail(self):
        """Ensure that we check the case that the external authorization fails."""
        auth_mech = SmsLocalApiKeyAuthenification()
        fake_request = APIRequestFactory().post("/dummy")

        headers = {"X-APIKEY": "XYZ"}
        fake_request.headers = headers

        with patch("requests.get") as userinfo_mock:
            # Normally it is not the get that throws this exception.
            # but the raise_for_status.
            # However, we don't want to run the real request,
            # so we raise the exception earlier.
            userinfo_mock.side_effect = HTTPError(
                "401 Client Error: Unauthorized for url"
            )
            result = auth_mech.authenticate(fake_request)
        self.assertIsNone(result)

    def test_authenticate_with_header_external_success(self):
        """Ensure that we can use the external authentication and get a user."""
        auth_mech = SmsLocalApiKeyAuthenification()
        fake_request = APIRequestFactory().post("/dummy")

        headers = {"X-APIKEY": "Bearer XYZ"}
        fake_request.headers = headers

        with patch("requests.get") as userinfo_mock:
            # Normally it is not the get that throws this exception.
            # but the raise_for_status.
            # However, we don't want to run the real request,
            # so we raise the exception earlier.
            userinfo_mock.return_value = SuccessfullFakeResponse(
                json_data={"data": {"attributes": {"subject": "test@localhost"}}},
                status_code=200,
                exception=None,
            )
            result = auth_mech.authenticate(fake_request)

        self.assertEqual(len(result), 2)
        self.assertEqual(
            result[0], SmsUser({"attributes": {"subject": "test@localhost"}})
        )
