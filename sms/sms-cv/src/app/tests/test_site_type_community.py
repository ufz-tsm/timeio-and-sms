# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Test classes for the site type community entries."""

import copy

from django.conf import settings
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import (
    Community,
    GlobalProvenance,
    SiteType,
    SiteTypeCommunity,
    SiteUsage,
)
from app.serializers import SiteTypeCommunitySerializer
from app.tests.Base_class import BaseTestCase


class SiteTypeTestCaseCommunity(TestCase):
    """Test class for the site type community model."""

    list_url = reverse("sitetypecommunity-list")

    def setUp(self):
        """Set some data up for the tests."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        co = Community.objects.create(
            id=1,
            term="Test Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
        )
        co.save()

        su = SiteUsage.objects.create(
            id=1,
            term="Test SiteUsage",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        su.save()

        st = SiteType.objects.create(
            id=1,
            term="Test Site Type Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            site_usage_id=1,
        )
        st.save()
        stc = SiteTypeCommunity.objects.create(
            id=1,
            term="Test Site Type Community",
            abbreviation="TSTC",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            note="Test1",
            community_id=1,
            root_id=1,
        )
        stc.save()
        self.detail_url = reverse(
            "sitetypecommunity-detail", kwargs={"pk": SiteTypeCommunity.pk}
        )

    def test_str(self):
        """Test the string conversion."""
        stco = SiteTypeCommunity.objects.get(id=1)
        self.assertEqual(stco.term, "Test Site Type Community")

    def test_serializer(self):
        """
        Test the serializer.

        It should just work fine with the given data.
        But the root & the community are both required.
        """
        stco = SiteTypeCommunity.objects.get(id=1)
        data = SiteTypeCommunitySerializer(stco, context={"request": None}).data
        data_without_root = copy.copy(data)
        data_without_root["root"] = None
        data_without_community = copy.copy(data)
        data_without_community["community"] = None

        # delete, so that we don't run into unique constraints
        stco.delete()

        serializer_ok = SiteTypeCommunitySerializer(
            data=data, context={"request": None}
        )
        self.assertTrue(serializer_ok.is_valid())

        # We still don't save it.
        self.assertEqual(0, SiteTypeCommunity.objects.count())

        serializer_without_root = SiteTypeCommunitySerializer(
            data=data_without_root, context={"request": None}
        )
        self.assertFalse(serializer_without_root.is_valid())

        serializer_without_community = SiteTypeCommunitySerializer(
            data=data_without_community, context={"request": None}
        )
        self.assertFalse(serializer_without_community.is_valid())

    def test_root(self):
        """Test the root relationship."""
        stco = SiteTypeCommunity.objects.get(id=1)
        self.assertEqual(stco.root_id, 1)

    def test_community(self):
        """Test the community relationship."""
        stco = SiteTypeCommunity.objects.get(id=1)
        self.assertEqual(stco.community_id, 1)

    def test_get_all(self, url_name=None):
        """Ensure the result has all attributes in 'SiteTypeCommunity'."""
        stco = SiteTypeCommunity.objects.first()
        if not url_name:
            path = self.list_url
        else:
            path = reverse(
                url_name, kwargs={"parent_lookup_community_id": stco.community_id}
            )
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(path)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": f"http://testserver{path}?page%5Bnumber%5D=1",
                "last": f"http://testserver{path}?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "SiteTypeCommunity",
                    "id": encoding.force_str(stco.pk),
                    "attributes": {
                        "term": stco.term,
                        "abbreviation": stco.abbreviation,
                        "definition": stco.definition,
                        "provenance": stco.provenance,
                        "provenance-uri": stco.provenance_uri,
                        "note": stco.note,
                    },
                    "relationships": {
                        "root": {
                            "links": {
                                "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/sitetypes-communities/1/relationships/root",
                                "related": f"http://testserver/{settings.CV_BASE_URL}api/v1/sitetypes-communities/1/root/",
                            },
                            "data": {
                                "type": "SiteType",
                                "id": encoding.force_str(stco.root_id),
                            },
                        },
                        "community": {
                            "links": {
                                "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/sitetypes-communities/1/relationships/community",
                                "related": f"http://testserver/{settings.CV_BASE_URL}api/v1/sitetypes-communities/1/community/",
                            },
                            "data": {
                                "type": "Community",
                                "id": encoding.force_str(stco.community_id),
                            },
                        },
                    },
                    "links": {
                        "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/sitetypes-communities/1/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Test that the output is sorted."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_community_routes(self):
        """Test the get list endpoint."""
        self.test_get_all(url_name="community-sitetypes-list")

    def test_community_filter(self):
        """Test the filter for the community term."""
        response = self.client.get(
            self.list_url, data={"filter[community.term]": "Test Community"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)

    def test_rootstatus_filter(self):
        """Test the filter for the root entry."""
        response = self.client.get(
            self.list_url, data={"filter[root.status]": "PENDING"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)
