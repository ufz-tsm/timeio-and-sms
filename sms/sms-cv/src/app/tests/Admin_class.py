# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse


class TestAdminInterface(TestCase):
    def setUpAdmin(self, client, obj):
        if not User.objects.filter(username="superuser"):
            User.objects.create_superuser(
                username="superuser", password="secret", email="admin@example.com"
            )
        self.obj = obj
        self.client = client
        self.client.login(username="superuser", password="secret")
        return self

    def list_view_responding(self):
        response = self.client.get(get_admin_action_url(self.obj, "_changelist"))
        self.assertEqual(response.status_code, 200)

    def change_view_responding(self):
        response = self.client.get(get_admin_action_url(self.obj, "_change"))
        self.assertEqual(response.status_code, 200)

    def add_view_responding(self):
        response = self.client.get(get_admin_action_url(self.obj, "_add"))
        self.assertEqual(response.status_code, 200)

    def delete_view_responding(self):
        response = self.client.get(get_admin_action_url(self.obj, "_delete"))
        self.assertEqual(response.status_code, 200)


def get_admin_action_url(obj, url_string=None):
    if url_string == "_change" or url_string == "_delete":
        return reverse(
            "admin:{}_{}{}".format(
                obj._meta.app_label, type(obj).__name__.lower(), url_string
            ),
            args=(obj.pk,),
        )
    else:
        return reverse(
            "admin:{}_{}{}".format(
                obj._meta.app_label, type(obj).__name__.lower(), url_string
            )
        )
