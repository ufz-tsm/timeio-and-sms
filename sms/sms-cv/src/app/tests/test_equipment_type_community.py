# SPDX-FileCopyrightText: 2021 - 2023
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

import copy

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import (
    Community,
    EquipmentType,
    EquipmentTypeCommunity,
    GlobalProvenance,
)
from app.serializers import EquipmentTypeCommunitySerializer
from app.tests.Base_class import BaseTestCase


class EquipmentTypeTestCase(TestCase):
    list_url = reverse("equipmenttypecommunity-list")

    def setUp(self):
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        co = Community.objects.create(
            id=1,
            term="Test Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
        )
        co.save()

        et = EquipmentType.objects.create(
            id=1,
            term="Test Equipment Type",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        et.save()

        etco = EquipmentTypeCommunity.objects.create(
            id=1,
            term="Test Equipment Type Community",
            abbreviation="TETC",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            note="Test1",
            community_id=1,
            root_id=1,
        )
        etco.save()
        self.detail_url = reverse(
            "equipmenttypecommunity-detail", kwargs={"pk": EquipmentTypeCommunity.pk}
        )

    def test_str(self):
        """
        EquipmentTypeCommunity
        :return:
        """
        etco = EquipmentTypeCommunity.objects.get(id=1)
        self.assertEqual(etco.term, "Test Equipment Type Community")

    def test_serializer(self):
        """
        Test the serializer.

        It should just work fine with the given data.
        But the root & the community are both required.
        """
        etco = EquipmentTypeCommunity.objects.get(id=1)
        data = EquipmentTypeCommunitySerializer(etco, context={"request": None}).data
        data_without_root = copy.copy(data)
        data_without_root["root"] = None
        data_without_community = copy.copy(data)
        data_without_community["community"] = None

        # delete, so that we don't run into unique constraints
        etco.delete()

        serializer_ok = EquipmentTypeCommunitySerializer(
            data=data, context={"request": None}
        )
        self.assertTrue(serializer_ok.is_valid())

        # We still don't save it.
        self.assertEqual(0, EquipmentTypeCommunity.objects.count())

        serializer_without_root = EquipmentTypeCommunitySerializer(
            data=data_without_root, context={"request": None}
        )
        self.assertFalse(serializer_without_root.is_valid())

        serializer_without_community = EquipmentTypeCommunitySerializer(
            data=data_without_community, context={"request": None}
        )
        self.assertFalse(serializer_without_community.is_valid())

    def test_root(self):
        etco = EquipmentTypeCommunity.objects.get(id=1)
        self.assertEqual(etco.root_id, 1)

    def test_community(self):
        etco = EquipmentTypeCommunity.objects.get(id=1)
        self.assertEqual(etco.community_id, 1)

    def test_get_all(self, url_name=None):
        """
        Ensure the result has all attributes in 'EquipmentTypeCommunity'
        """
        etco = EquipmentTypeCommunity.objects.first()
        if not url_name:
            path = self.list_url
        else:
            path = reverse(
                url_name, kwargs={"parent_lookup_community_id": etco.community_id}
            )
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(path)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": f"http://testserver{path}?page%5Bnumber%5D=1",
                "last": f"http://testserver{path}?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "EquipmentTypeCommunity",
                    "id": encoding.force_str(etco.pk),
                    "attributes": {
                        "term": etco.term,
                        "abbreviation": etco.abbreviation,
                        "definition": etco.definition,
                        "provenance": etco.provenance,
                        "provenance-uri": etco.provenance_uri,
                        "note": etco.note,
                    },
                    "relationships": {
                        "root": {
                            "links": {
                                "self": "http://testserver/api/v1/equipmenttypes-communities/1/relationships/root",
                                "related": "http://testserver/api/v1/equipmenttypes-communities/1/root/",
                            },
                            "data": {
                                "type": "EquipmentType",
                                "id": encoding.force_str(etco.root_id),
                            },
                        },
                        "community": {
                            "links": {
                                "self": "http://testserver/api/v1/equipmenttypes-communities/1/relationships/community",
                                "related": "http://testserver/api/v1/equipmenttypes-communities/1/community/",
                            },
                            "data": {
                                "type": "Community",
                                "id": encoding.force_str(etco.community_id),
                            },
                        },
                    },
                    "links": {
                        "self": "http://testserver/api/v1/equipmenttypes-communities/1/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """
        test sort
        """
        BaseTestCase().sort(self.list_url, self.client)

    def test_community_routes(self):
        """
        test community routes
        """
        self.test_get_all(url_name="community-equipmenttypes-list")

    def test_community_filter(self):
        """
        filter for a communities term
        """
        response = self.client.get(
            self.list_url, data={"filter[community.term]": "Test Community"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)

    def test_rootstatus_filter(self):
        """
        filter for a root status
        """
        response = self.client.get(
            self.list_url, data={"filter[root.status]": "PENDING"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)
