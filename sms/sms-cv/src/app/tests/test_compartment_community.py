# SPDX-FileCopyrightText: 2021 - 2023
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

import copy

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import Community, Compartment, CompartmentCommunity, GlobalProvenance
from app.serializers import CompartmentCommunitySerializer
from app.tests.Base_class import BaseTestCase


class CompartmentCommunityTestCase(TestCase):
    list_url = reverse("compartmentcommunity-list")

    def setUp(self):
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        co = Community.objects.create(
            id=1,
            term="Test Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
        )
        co.save()

        com = Compartment.objects.create(
            id=1,
            term="Test Compartment",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        com.save()

        comco = CompartmentCommunity.objects.create(
            id=1,
            term="Test Compartment Community",
            abbreviation="TCC",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            note="Test1",
            community_id=1,
            root_id=1,
        )
        comco.save()
        self.detail_url = reverse(
            "compartmentcommunity-detail", kwargs={"pk": CompartmentCommunity.pk}
        )

    def test_str(self):
        """
        CompartmentCommunity
        :return:
        """
        comco = CompartmentCommunity.objects.get(id=1)
        self.assertEqual(comco.term, "Test Compartment Community")

    def test_serializer(self):
        """
        Test the serializer.

        It should just work fine with the given data.
        But the root & the community are both required.
        """
        comco = CompartmentCommunity.objects.get(id=1)
        data = CompartmentCommunitySerializer(comco, context={"request": None}).data
        data_without_root = copy.copy(data)
        data_without_root["root"] = None
        data_without_community = copy.copy(data)
        data_without_community["community"] = None

        # delete, so that we don't run into unique constraints
        comco.delete()

        serializer_ok = CompartmentCommunitySerializer(
            data=data, context={"request": None}
        )
        self.assertTrue(serializer_ok.is_valid())

        # We still don't save it.
        self.assertEqual(0, CompartmentCommunity.objects.count())

        serializer_without_root = CompartmentCommunitySerializer(
            data=data_without_root, context={"request": None}
        )
        self.assertFalse(serializer_without_root.is_valid())

        serializer_without_community = CompartmentCommunitySerializer(
            data=data_without_community, context={"request": None}
        )
        self.assertFalse(serializer_without_community.is_valid())

    def test_root(self):
        comco = CompartmentCommunity.objects.get(id=1)
        self.assertEqual(comco.root_id, 1)

    def test_community(self):
        comco = CompartmentCommunity.objects.get(id=1)
        self.assertEqual(comco.community_id, 1)

    def test_get_all(self, url_name=None):
        """
        Ensure the result has all attributes in 'CompartmentCommunity'
        """
        comco = CompartmentCommunity.objects.first()
        if not url_name:
            path = self.list_url
        else:
            path = reverse(
                url_name, kwargs={"parent_lookup_community_id": comco.community_id}
            )
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(path)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": f"http://testserver{path}?page%5Bnumber%5D=1",
                "last": f"http://testserver{path}?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "CompartmentCommunity",
                    "id": encoding.force_str(comco.pk),
                    "attributes": {
                        "term": comco.term,
                        "abbreviation": comco.abbreviation,
                        "definition": comco.definition,
                        "provenance": comco.provenance,
                        "provenance-uri": comco.provenance_uri,
                        "note": comco.note,
                    },
                    "relationships": {
                        "root": {
                            "links": {
                                "self": "http://testserver/api/v1/compartments-communities/1/relationships/root",
                                "related": "http://testserver/api/v1/compartments-communities/1/root/",
                            },
                            "data": {
                                "type": "Compartment",
                                "id": encoding.force_str(comco.root_id),
                            },
                        },
                        "community": {
                            "links": {
                                "self": "http://testserver/api/v1/compartments-communities/1/relationships/community",
                                "related": "http://testserver/api/v1/compartments-communities/1/community/",
                            },
                            "data": {
                                "type": "Community",
                                "id": encoding.force_str(comco.community_id),
                            },
                        },
                    },
                    "links": {
                        "self": "http://testserver/api/v1/compartments-communities/1/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """
        test sort
        """
        BaseTestCase().sort(self.list_url, self.client)

    def test_community_routes(self):
        """
        test community routes
        """
        self.test_get_all(url_name="community-compartments-list")

    def test_community_filter(self):
        """
        filter for a communities term
        """
        response = self.client.get(
            self.list_url, data={"filter[community.term]": "Test Community"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)

    def test_rootstatus_filter(self):
        """
        filter for a root status
        """
        response = self.client.get(
            self.list_url, data={"filter[root.status]": "PENDING"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)
