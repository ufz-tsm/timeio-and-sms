# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Tests for the licenses."""

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from django.utils import encoding

from app.models import GlobalProvenance, License


class LicenseTestCase(TestCase):
    """Test cases for the licenses."""

    list_url = reverse("license-list")

    def setUp(self):
        """Set up some example data."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()
        self.license = License.objects.create(
            global_provenance_id=gl.id,
            term="CC-1.0",
            status="ACCEPTED",
            definition="Creative Commons Zero v1.0 Universal",
            provenance_uri="https://spdx.org/licenses/CC0-1.0.html",
            provenance="Software Package Data Exchange",
        )
        self.detail_url = reverse("license-detail", kwargs={"pk": self.license.pk})

    def test_str(self):
        """Test that the str gives the name."""
        as_str = str(self.license)
        self.assertEqual(as_str, "CC-1.0")

    def test_get_all(self):
        """Test that we can get all the licenses in a list."""
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/?page%5Bnumber%5D=1",
                "last": f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "License",
                    "id": str(self.license.id),
                    "attributes": {
                        "term": self.license.term,
                        "definition": self.license.definition,
                        "provenance": self.license.provenance,
                        "provenance_uri": self.license.provenance_uri,
                        "category": None,
                        "note": None,
                        "status": "ACCEPTED",
                        "requested_by_email": self.license.requested_by_email,
                    },
                    "relationships": {
                        "global_provenance": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/",
                                        f"{self.license.id}/relationships/global_provenance",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/",
                                        f"{self.license.id}/global_provenance/",
                                    ]
                                ),
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(
                                    self.license.global_provenance_id
                                ),
                            },
                        },
                        "successor": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/",
                                        f"{self.license.id}/relationships/successor",
                                    ]
                                ),
                                "related": "".join(
                                    [
                                        f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/",
                                        f"{self.license.id}/successor/",
                                    ]
                                ),
                            }
                        },
                    },
                    "links": {
                        "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/{self.license.id}/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_get_detail(self):
        """Test that we can get one license."""
        response = self.client.get(self.detail_url)
        self.assertEqual(response.status_code, 200)

        expected = {
            "data": {
                "type": "License",
                "id": str(self.license.id),
                "attributes": {
                    "term": self.license.term,
                    "definition": self.license.definition,
                    "provenance": self.license.provenance,
                    "provenance_uri": self.license.provenance_uri,
                    "category": None,
                    "note": None,
                    "status": "ACCEPTED",
                    "requested_by_email": self.license.requested_by_email,
                },
                "relationships": {
                    "global_provenance": {
                        "links": {
                            "self": "".join(
                                [
                                    f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/",
                                    f"{self.license.id}/relationships/global_provenance",
                                ]
                            ),
                            "related": "".join(
                                [
                                    f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/",
                                    f"{self.license.id}/global_provenance/",
                                ]
                            ),
                        },
                        "data": {
                            "type": "GlobalProvenance",
                            "id": encoding.force_str(self.license.global_provenance_id),
                        },
                    },
                    "successor": {
                        "links": {
                            "self": "".join(
                                [
                                    f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/",
                                    f"{self.license.id}/relationships/successor",
                                ]
                            ),
                            "related": "".join(
                                [
                                    f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/",
                                    f"{self.license.id}/successor/",
                                ]
                            ),
                        }
                    },
                },
                "links": {
                    "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/licenses/{self.license.id}/"
                },
            }
        }
        assert expected == response.json()
