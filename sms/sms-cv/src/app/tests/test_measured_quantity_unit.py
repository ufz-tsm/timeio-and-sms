# SPDX-FileCopyrightText: 2020 - 2023
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Tests for the measured quantity units."""

from unittest.mock import patch

from django.conf import settings
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding
from rest_framework import status

from app.auth.external import IdpUser
from app.models import (
    AggregationType,
    Compartment,
    GlobalProvenance,
    MeasuredQuantity,
    MeasuredQuantityUnit,
    SamplingMedium,
    Unit,
)
from app.views.measuredquantity_unit_viewset import MeasuredQuantityUnitViewSet


class MeasuredQuantityUnitTestCase(TestCase):
    """Test class for the measured quantity unit relation."""

    list_url = reverse("measuredquantityunit-list")

    def setUp(self):
        """Set some test data up."""
        self.gp = GlobalProvenance.objects.create(
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        self.gp.save()
        self.c = Compartment.objects.create(
            id=1,
            term="Test Compartment",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance=self.gp,
            successor_id=None,
        )
        self.c.save()
        self.at = AggregationType.objects.create(
            term="Sum",
            definition=None,
            provenance=None,
            provenance_uri=None,
            category=None,
            note=None,
            global_provenance=self.gp,
            successor_id=None,
        )
        self.at.save()
        self.sm = SamplingMedium.objects.create(
            term="Test SamplingMedium",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance=self.gp,
            successor_id=None,
            compartment=self.c,
        )
        self.sm.save()

        self.mq = MeasuredQuantity.objects.create(
            term="Test MeasuredQuantity",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance=self.gp,
            successor_id=None,
            sampling_media=self.sm,
            aggregation_type=self.at,
        )
        self.mq.save()

        self.u = Unit.objects.create(
            term="Test Unit",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance=self.gp,
            successor_id=None,
        )
        self.u.save()
        self.mqu = MeasuredQuantityUnit.objects.create(
            default_limit_min=None,
            default_limit_max=None,
            unit=self.u,
            measured_quantity=self.mq,
        )
        self.mqu.save()
        self.detail_url = reverse(
            "measuredquantityunit-detail", kwargs={"pk": MeasuredQuantityUnit.pk}
        )

    def test_access_measured_quantity_term(self):
        """Test accessing the term of the measured quantity."""
        self.assertEqual(self.mqu.measured_quantity.term, "Test MeasuredQuantity")

    def test_get_all(self):
        """Ensure the result has all attributes in 'MeasuredQuantityUnit'."""
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        m = self.mqu
        expected = {
            "links": {
                "first": f"http://testserver/{settings.CV_BASE_URL}api/v1/measuredquantityunits/?page%5Bnumber%5D=1",
                "last": f"http://testserver/{settings.CV_BASE_URL}api/v1/measuredquantityunits/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "MeasuredQuantityUnit",
                    "id": encoding.force_str(m.pk),
                    "attributes": {
                        "default-limit-min": m.default_limit_max,
                        "default-limit-max": m.default_limit_max,
                    },
                    "relationships": {
                        "unit": {
                            "links": {
                                "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/measuredquantityunits/1/relationships/unit",
                                "related": f"http://testserver/{settings.CV_BASE_URL}api/v1/measuredquantityunits/1/unit/",
                            },
                            "data": {
                                "type": "Unit",
                                "id": encoding.force_str(m.unit_id),
                            },
                        },
                        "measured-quantity": {
                            "links": {
                                "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/measuredquantityunits/1/relationships/measured_quantity",
                                "related": f"http://testserver/{settings.CV_BASE_URL}api/v1/measuredquantityunits/1/measured_quantity/",
                            },
                            "data": {
                                "type": "MeasuredQuantity",
                                "id": encoding.force_str(m.measured_quantity_id),
                            },
                        },
                    },
                    "links": {
                        "self": f"http://testserver/{settings.CV_BASE_URL}api/v1/measuredquantityunits/1/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Test to sort the output."""
        response = self.client.get(self.list_url, data={"sort": "default_limit_min"})
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        headlines = [c["attributes"]["default_limit_min"] for c in dja_response["data"]]
        sorted_headlines = sorted(headlines)
        self.assertEqual(headlines, sorted_headlines)

    def test_post_no_login(self):
        """Ensure we can't post without user information."""
        unit = Unit.objects.create(term="fancy new unit")
        measured_quantity = self.mq

        example_payload = {
            "data": {
                "type": "MeasuredQuantityUnit",
                "attributes": {"default_limit_min": None, "default_limit_max": None},
                "relationships": {
                    "unit": {
                        "data": {
                            "id": unit.id,
                            "type": "Unit",
                        }
                    },
                    "measured_quantity": {
                        "data": {"id": measured_quantity.id, "type": "MeasuredQuantity"}
                    },
                },
            }
        }
        response = self.client.post(
            self.list_url, data=example_payload, content_type="application/vnd.api+json"
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_with_user(self):
        """Ensure we can post with user information."""
        unit = Unit.objects.create(term="fancy new unit")
        measured_quantity = self.mq
        count_start = MeasuredQuantityUnit.objects.count()

        self.assertEqual(count_start, 1)
        example_payload = {
            "data": {
                "type": "MeasuredQuantityUnit",
                "attributes": {"default_limit_min": None, "default_limit_max": None},
                "relationships": {
                    "unit": {
                        "data": {
                            "id": unit.id,
                            "type": "Unit",
                        }
                    },
                    "measured_quantity": {
                        "data": {"id": measured_quantity.id, "type": "MeasuredQuantity"}
                    },
                },
            }
        }
        with override_settings(GITLAB_TOKEN=""):
            with patch.object(
                MeasuredQuantityUnitViewSet.authentication_classes[0], "authenticate"
            ) as mock:
                mock.return_value = (IdpUser({"email": "dummy.user@localhost"}), None)
                response = self.client.post(
                    self.list_url,
                    data=example_payload,
                    content_type="application/vnd.api+json",
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        count_end = MeasuredQuantityUnit.objects.count()
        self.assertEqual(count_end, 2)
