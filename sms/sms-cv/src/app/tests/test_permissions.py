# SPDX-FileCopyrightText: 2022
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Tests for the new permission classes."""
from rest_framework.test import APIRequestFactory, APITestCase

from app.permissions import PostRequest, ReadOnly


class TestReadOnly(APITestCase):
    """Tests for the ReadOnly permission class."""

    def test_get(self):
        """Ensure we allow a get request."""
        fake_request = APIRequestFactory().get("/dummy")
        test_instance = ReadOnly()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertTrue(result)

    def test_head(self):
        """Ensure we allow a head request."""
        fake_request = APIRequestFactory().head("/dummy")
        test_instance = ReadOnly()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertTrue(result)

    def test_options(self):
        """Ensure we allow an options request."""
        fake_request = APIRequestFactory().options("/dummy")
        test_instance = ReadOnly()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertTrue(result)

    def test_post(self):
        """Ensure we don't allow a post request."""
        fake_request = APIRequestFactory().post("/dummy")
        test_instance = ReadOnly()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertFalse(result)

    def test_patch(self):
        """Ensure we don't allow a patch request."""
        fake_request = APIRequestFactory().patch("/dummy")
        test_instance = ReadOnly()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertFalse(result)

    def test_put(self):
        """Ensure we don't allow a put request."""
        fake_request = APIRequestFactory().put("/dummy")
        test_instance = ReadOnly()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertFalse(result)

    def test_delete(self):
        """Ensure we don't allow a delete request."""
        fake_request = APIRequestFactory().delete("/dummy")
        test_instance = ReadOnly()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertFalse(result)


class TestPostRequest(APITestCase):
    """Tests for the PostRequest permission class."""

    def test_get(self):
        """Ensure we don't allow a get request."""
        fake_request = APIRequestFactory().get("/dummy")
        test_instance = PostRequest()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertFalse(result)

    def test_head(self):
        """Ensure we don't allow a head request."""
        fake_request = APIRequestFactory().head("/dummy")
        test_instance = PostRequest()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertFalse(result)

    def test_options(self):
        """Ensure we don't allow an options request."""
        fake_request = APIRequestFactory().options("/dummy")
        test_instance = PostRequest()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertFalse(result)

    def test_post(self):
        """Ensure we allow a post request."""
        fake_request = APIRequestFactory().post("/dummy")
        test_instance = PostRequest()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertTrue(result)

    def test_patch(self):
        """Ensure we don't allow a patch request."""
        fake_request = APIRequestFactory().patch("/dummy")
        test_instance = PostRequest()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertFalse(result)

    def test_put(self):
        """Ensure we don't allow a put request."""
        fake_request = APIRequestFactory().put("/dummy")
        test_instance = PostRequest()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertFalse(result)

    def test_delete(self):
        """Ensure we don't allow a delete request."""
        fake_request = APIRequestFactory().delete("/dummy")
        test_instance = PostRequest()

        result = test_instance.has_permission(fake_request, view=None)
        self.assertFalse(result)
