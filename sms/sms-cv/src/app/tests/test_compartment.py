# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Tests for for the compartments."""

import io

import rdflib
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import Compartment, GlobalProvenance, SamplingMedium
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase, GraphAssertionMixin


class CompartmentTestCase(TestCase, GraphAssertionMixin):
    """Test case for the compartments."""

    list_url = reverse("compartment-list")

    def setUp(self):
        """Set some entries up so that we can use them in the tests."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        c = Compartment.objects.create(
            id=1,
            term="Test Compartment",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        c.save()
        self.detail_url = reverse("compartment-detail", kwargs={"pk": Compartment.pk})

    def test_term(self):
        """Test that we can use the term field."""
        c = Compartment.objects.get(id=1)
        self.assertEqual(c.term, "Test Compartment")

    def test_global_provenance(self):
        """Test that we can use the foreign key to the global provenance."""
        c = Compartment.objects.get(id=1)
        self.assertEqual(c.global_provenance_id, 1)

    def test_get_all(self):
        """Ensure the get request result has all attributes in 'Compartment'."""
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        c = Compartment.objects.all()[0]
        expected = {
            "links": {
                "first": "http://testserver/api/v1/compartments/?page%5Bnumber%5D=1",
                "last": "http://testserver/api/v1/compartments/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "Compartment",
                    "id": encoding.force_str(c.pk),
                    "attributes": {
                        "term": c.term,
                        "definition": c.definition,
                        "provenance": c.provenance,
                        "provenance-uri": c.provenance_uri,
                        "category": c.category,
                        "note": c.note,
                        "status": c.status,
                        "requested-by-email": c.requested_by_email,
                    },
                    "relationships": {
                        "global-provenance": {
                            "links": {
                                "self": "http://testserver/api/v1/compartments/1/relationships/global_provenance",
                                "related": "http://testserver/api/v1/compartments/1/global_provenance/",
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(c.global_provenance_id),
                            },
                        },
                        "sampling-media": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "http://testserver/api/v1/compartments/1/relationships/sampling_media",
                                "related": "http://testserver/api/v1/compartments/1/sampling_media/",
                            },
                        },
                        "successor": {
                            "links": {
                                "self": "http://testserver/api/v1/compartments/1/relationships/successor",
                                "related": "http://testserver/api/v1/compartments/1/successor/",
                            }
                        },
                    },
                    "links": {"self": "http://testserver/api/v1/compartments/1/"},
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }

        assert expected == response.json()

    def test_sort(self):
        """Ensure that the sorting works as expected."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """Ensure that we can use the django admin interface."""
        obj = Compartment.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        # test_admin.delete_view_responding()

    def test_rdf_output(self):
        """Ensure we can deliver the data for one comparment also in RDF format."""
        c = Compartment.objects.get(id=1)
        response = self.client.get(
            f"{self.list_url}{c.id}/", headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{c.id}/"))

        # Now lets check the content.
        self.assertTripleInGraph(
            graph, uri_ref, rdflib.namespace.RDF.type, rdflib.namespace.SKOS.Concept
        )
        self.assertTripleInGraph(
            graph,
            uri_ref,
            rdflib.namespace.SKOS.prefLabel,
            rdflib.Literal(c.term, lang="en"),
        )
        self.assertTripleInGraph(
            graph,
            uri_ref,
            rdflib.namespace.SKOS.definition,
            rdflib.Literal(c.definition, lang="en"),
        )

        self.assertIsNone(c.provenance_uri)
        # As we don't have a provenance uri set, we expect that we don't add
        # an exactMatch relation.
        # None is a placeholder for any kind of value.
        self.assertTripleNotInGraph(
            graph, uri_ref, rdflib.namespace.SKOS.exactMatch, None
        )

        # However, we can add it.
        c.provenance_uri = "https://some.definition.in/the/web"
        c.save()

        # And fetch the content again...
        response = self.client.get(
            f"{self.list_url}{c.id}/", headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")

        self.assertTripleInGraph(
            graph,
            uri_ref,
            rdflib.namespace.SKOS.exactMatch,
            rdflib.URIRef(c.provenance_uri),
        )
        # And in case that we have a sampling medium that links to the compartment we
        # want that this one is linked in the rdf.
        sm = SamplingMedium.objects.create(
            id=1,
            term="Test SamplingMedium",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            compartment_id=c.id,
        )
        sm.save()
        response = self.client.get(
            f"{self.list_url}{c.id}/", headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")
        sampling_medium_list_url = reverse("samplingmedium-list")
        narrower_triples = self.assertTripleInGraph(
            graph, uri_ref, rdflib.namespace.SKOS.narrower, None
        )
        self.assertEqual(len(narrower_triples), 1)
        narrower_triples[0][2].toPython().endswith(
            f"{sampling_medium_list_url}{sm.id}/"
        )

        # And we can fetch the content of the list view.
        response = self.client.get(
            self.list_url, headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph_for_list = rdflib.Graph()
        graph_for_list.parse(io.BytesIO(response.getvalue()), format="xml")

        # And in the graph for the list endpoint are all the entries of the graph
        # for a single element.
        for subject, predicate, object_ in graph.triples((None, None, None)):
            self.assertTripleInGraph(graph_for_list, subject, predicate, object_)

        # And we want to ensure that we don't adjust the links, due to the .xml format
        # suffix.
        response = self.client.get(f"{self.list_url}{c.id}.xml/")
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{c.id}/"))

    def test_ttl_output(self):
        """Ensure we can also deliver the RDF data in TTL format."""
        c = Compartment.objects.get(id=1)
        response = self.client.get(
            f"{self.list_url}{c.id}/", headers={"accept": "application/x-turtle"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="ttl")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{c.id}/"))

        # And the same also for the .ttl format endpoint
        response = self.client.get(f"{self.list_url}{c.id}.ttl/")
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="ttl")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{c.id}/"))
