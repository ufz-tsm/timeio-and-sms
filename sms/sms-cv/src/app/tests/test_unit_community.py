# SPDX-FileCopyrightText: 2021 - 2023
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

import copy

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding
from rest_framework import status

from app.models import Community, GlobalProvenance, Unit, UnitCommunity
from app.serializers import UnitCommunitySerializer
from app.tests.Base_class import BaseTestCase


class UnitCommunityTestCase(TestCase):
    list_url = reverse("unitcommunity-list")

    def setUp(self):
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        co = Community.objects.create(
            id=1,
            term="Test Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
        )
        co.save()

        u = Unit.objects.create(
            id=1,
            term="Test Unit",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        u.save()
        uco = UnitCommunity.objects.create(
            id=1,
            term="Test Unit Community",
            abbreviation="TUC",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            note="Test1",
            community_id=1,
            root_id=1,
        )
        uco.save()
        self.detail_url = reverse(
            "unitcommunity-detail", kwargs={"pk": UnitCommunity.pk}
        )

    def test_str(self):
        """
        UnitCommunity
        :return:
        """
        uco = UnitCommunity.objects.get(id=1)
        self.assertEqual(uco.term, "Test Unit Community")

    def test_serializer(self):
        """
        Test the serializer.

        It should just work fine with the given data.
        But the root & the community are both required.
        """
        uco = UnitCommunity.objects.get(id=1)
        data = UnitCommunitySerializer(uco, context={"request": None}).data
        data_without_root = copy.copy(data)
        data_without_root["root"] = None
        data_without_community = copy.copy(data)
        data_without_community["community"] = None

        # delete, so that we don't run into unique constraints
        uco.delete()

        serializer_ok = UnitCommunitySerializer(data=data, context={"request": None})
        self.assertTrue(serializer_ok.is_valid())

        # We still don't save it.
        self.assertEqual(0, UnitCommunity.objects.count())

        serializer_without_root = UnitCommunitySerializer(
            data=data_without_root, context={"request": None}
        )
        self.assertFalse(serializer_without_root.is_valid())

        serializer_without_community = UnitCommunitySerializer(
            data=data_without_community, context={"request": None}
        )
        self.assertFalse(serializer_without_community.is_valid())

    def test_delete(self):
        """
        Ensure that we can't delete a unit community using the views.
        """
        uco = UnitCommunity.objects.get(id=1)
        url = reverse("unitcommunity-detail", kwargs={"pk": uco.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_put(self):
        """
        Ensure that we can't change our data with using the views.
        """
        uco = UnitCommunity.objects.get(id=1)
        url = reverse("unitcommunity-detail", kwargs={"pk": uco.pk})
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        data = get_response.json()

        content_type = "application/vnd.api+json"
        put_response = self.client.put(url, data=data, content_type=content_type)
        self.assertEqual(put_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post(self):
        """
        Ensure that we can't change our data with using the views.
        """
        uco = UnitCommunity.objects.get(id=1)
        url = reverse("unitcommunity-detail", kwargs={"pk": uco.pk})
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        data = get_response.json()
        data["data"]["id"] = None

        # To make sure we don't run into unique constraints
        uco.delete()

        content_type = "application/vnd.api+json"
        post_response = self.client.post(
            self.list_url, data=data, content_type=content_type
        )
        self.assertEqual(post_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_relationship(self):
        """
        Ensure that we can't delete using the relationship view.
        """
        uco = UnitCommunity.objects.get(pk=1)
        url = reverse(
            "unit_community-relationships",
            kwargs={"pk": uco.pk, "related_field": "community"},
        )
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_put_relationship(self):
        """
        Ensure that we can't override using the relationship view.
        """
        uco = UnitCommunity.objects.get(pk=1)
        url = reverse(
            "unit_community-relationships",
            kwargs={"pk": uco.pk, "related_field": "community"},
        )
        content_type = "application/vnd.api+json"
        response = self.client.put(url, data={}, content_type=content_type)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_post_relationship(self):
        """
        Ensure that we can't add using the relationship view.
        """
        uco = UnitCommunity.objects.get(pk=1)
        url = reverse(
            "unit_community-relationships",
            kwargs={"pk": uco.pk, "related_field": "community"},
        )
        content_type = "application/vnd.api+json"
        response = self.client.post(url, data={}, content_type=content_type)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_root(self):
        uco = UnitCommunity.objects.get(id=1)
        self.assertEqual(uco.root_id, 1)

    def test_community(self):
        uco = UnitCommunity.objects.get(id=1)
        self.assertEqual(uco.community_id, 1)

    def test_get_all(self, url_name=None):
        """
        Ensure the result has all attributes in 'UnitCommunity'
        """
        uco = UnitCommunity.objects.first()
        if not url_name:
            path = self.list_url
        else:
            path = reverse(
                url_name, kwargs={"parent_lookup_community_id": uco.community_id}
            )
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(path)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": f"http://testserver{path}?page%5Bnumber%5D=1",
                "last": f"http://testserver{path}?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "UnitCommunity",
                    "id": encoding.force_str(uco.pk),
                    "attributes": {
                        "term": uco.term,
                        "abbreviation": uco.abbreviation,
                        "definition": uco.definition,
                        "provenance": uco.provenance,
                        "provenance-uri": uco.provenance_uri,
                        "note": uco.note,
                    },
                    "relationships": {
                        "root": {
                            "links": {
                                "self": "http://testserver/api/v1/units-communities/1/relationships/root",
                                "related": "http://testserver/api/v1/units-communities/1/root/",
                            },
                            "data": {
                                "type": "Unit",
                                "id": encoding.force_str(uco.root_id),
                            },
                        },
                        "community": {
                            "links": {
                                "self": "http://testserver/api/v1/units-communities/1/relationships/community",
                                "related": "http://testserver/api/v1/units-communities/1/community/",
                            },
                            "data": {
                                "type": "Community",
                                "id": encoding.force_str(uco.community_id),
                            },
                        },
                    },
                    "links": {"self": "http://testserver/api/v1/units-communities/1/"},
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """
        test sort
        """
        BaseTestCase().sort(self.list_url, self.client)

    def test_community_routes(self):
        """
        test community routes
        """
        self.test_get_all(url_name="community-units-list")

    def test_community_filter(self):
        """
        filter for a communities term
        """
        response = self.client.get(
            self.list_url, data={"filter[community.term]": "Test Community"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)

    def test_rootstatus_filter(self):
        """
        filter for a root status
        """
        response = self.client.get(
            self.list_url, data={"filter[root.status]": "PENDING"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)
