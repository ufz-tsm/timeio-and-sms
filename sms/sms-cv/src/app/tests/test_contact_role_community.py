# SPDX-FileCopyrightText: 2022 - 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Tests for the contact role community models/views/serializer."""

import copy

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import Community, ContactRole, ContactRoleCommunity, GlobalProvenance
from app.serializers import ContactRoleCommunitySerializer
from app.tests.Base_class import BaseTestCase


class ContactRoleTestCaseCommunity(TestCase):
    """Test class for the contact role communities."""

    list_url = reverse("contactrolecommunity-list")

    def setUp(self):
        """Set up for all the tests."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        co = Community.objects.create(
            id=1,
            term="Test Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
        )
        co.save()

        cr = ContactRole.objects.create(
            id=1,
            term="Test ContactRole",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        cr.save()

        crco = ContactRoleCommunity.objects.create(
            id=1,
            term="Test Contact Role Community",
            abbreviation="TCRC",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            note="Test1",
            community_id=1,
            root_id=1,
        )
        crco.save()
        self.detail_url = reverse("contactrolecommunity-detail", kwargs={"pk": crco.pk})

    def test_str(self):
        """Test the str() behaviour."""
        crco = ContactRoleCommunity.objects.get(id=1)
        self.assertEqual(str(crco), "Test Contact Role Community")

    def test_serializer(self):
        """
        Test the serializer.

        It should just work fine with the given data.
        But the root & the community are both required.
        """
        crco = ContactRoleCommunity.objects.get(id=1)
        data = ContactRoleCommunitySerializer(crco, context={"request": None}).data
        data_without_root = copy.copy(data)
        data_without_root["root"] = None
        data_without_community = copy.copy(data)
        data_without_community["community"] = None

        # delete, so that we don't run into unique constraints
        crco.delete()

        serializer_ok = ContactRoleCommunitySerializer(
            data=data, context={"request": None}
        )
        self.assertTrue(serializer_ok.is_valid())

        # We still don't save it.
        self.assertEqual(0, ContactRoleCommunity.objects.count())

        serializer_without_root = ContactRoleCommunitySerializer(
            data=data_without_root, context={"request": None}
        )
        self.assertFalse(serializer_without_root.is_valid())

        serializer_without_community = ContactRoleCommunitySerializer(
            data=data_without_community, context={"request": None}
        )
        self.assertFalse(serializer_without_community.is_valid())

    def test_root(self):
        """Test that we get the right root id."""
        crco = ContactRoleCommunity.objects.get(id=1)
        self.assertEqual(crco.root_id, 1)

    def test_community(self):
        """Test that we get the right community id."""
        crco = ContactRoleCommunity.objects.get(id=1)
        self.assertEqual(crco.community_id, 1)

    def test_get_all(self, url_name=None):
        """Ensure the result has all attributes in 'ContactRoleCommunity'."""
        crco = ContactRoleCommunity.objects.first()
        if not url_name:
            path = self.list_url
        else:
            path = reverse(
                url_name, kwargs={"parent_lookup_community_id": crco.community_id}
            )
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(path)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": f"http://testserver{path}?page%5Bnumber%5D=1",
                "last": f"http://testserver{path}?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "ContactRoleCommunity",
                    "id": encoding.force_str(crco.pk),
                    "attributes": {
                        "term": crco.term,
                        "abbreviation": crco.abbreviation,
                        "definition": crco.definition,
                        "provenance": crco.provenance,
                        "provenance-uri": crco.provenance_uri,
                        "note": crco.note,
                    },
                    "relationships": {
                        "root": {
                            "links": {
                                "self": "http://testserver/api/v1/contactroles-communities/1/relationships/root",
                                "related": "http://testserver/api/v1/contactroles-communities/1/root/",
                            },
                            "data": {
                                "type": "ContactRole",
                                "id": encoding.force_str(crco.root_id),
                            },
                        },
                        "community": {
                            "links": {
                                "self": "http://testserver/api/v1/contactroles-communities/1/relationships/community",
                                "related": "http://testserver/api/v1/contactroles-communities/1/community/",
                            },
                            "data": {
                                "type": "Community",
                                "id": encoding.force_str(crco.community_id),
                            },
                        },
                    },
                    "links": {
                        "self": "http://testserver/api/v1/contactroles-communities/1/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Test the sorting."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_community_routes(self):
        """Test that we get data for the community route."""
        self.test_get_all(url_name="community-contactroles-list")

    def test_community_filter(self):
        """Test that we can filter with the community term."""
        response = self.client.get(
            self.list_url, data={"filter[community.term]": "Test Community"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)

    def test_rootstatus_filter(self):
        """Test that we can filter with the root status."""
        response = self.client.get(
            self.list_url, data={"filter[root.status]": "PENDING"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)
