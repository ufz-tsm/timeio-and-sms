# SPDX-FileCopyrightText: 2021 - 2023
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

import copy

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import (
    Community,
    EquipmentStatus,
    EquipmentStatusCommunity,
    GlobalProvenance,
)
from app.serializers import EquipmentStatusCommunitySerializer
from app.tests.Base_class import BaseTestCase


class EquipmentStatusCommunityTestCase(TestCase):
    list_url = reverse("equipmentstatuscommunity-list")

    def setUp(self):
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        co = Community.objects.create(
            id=1,
            term="Test Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
        )
        co.save()

        es = EquipmentStatus.objects.create(
            id=1,
            term="Test Equipment Status",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        es.save()

        esco = EquipmentStatusCommunity.objects.create(
            id=1,
            term="Test Equipment Status Community",
            abbreviation="TESC",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            note="Test1",
            community_id=1,
            root_id=1,
        )
        esco.save()
        self.detail_url = reverse(
            "equipmentstatuscommunity-detail",
            kwargs={"pk": EquipmentStatusCommunity.pk},
        )

    def test_str(self):
        """
        EquipmentStatusCommunity
        :return:
        """
        esco = EquipmentStatusCommunity.objects.get(id=1)
        self.assertEqual(esco.term, "Test Equipment Status Community")

    def test_serializer(self):
        """
        Test the serializer.

        It should just work fine with the given data.
        But the root & the community are both required.
        """
        esco = EquipmentStatusCommunity.objects.get(id=1)
        data = EquipmentStatusCommunitySerializer(esco, context={"request": None}).data
        data_without_root = copy.copy(data)
        data_without_root["root"] = None
        data_without_community = copy.copy(data)
        data_without_community["community"] = None

        # delete, so that we don't run into unique constraints
        esco.delete()

        serializer_ok = EquipmentStatusCommunitySerializer(
            data=data, context={"request": None}
        )
        self.assertTrue(serializer_ok.is_valid())

        # We still don't save it.
        self.assertEqual(0, EquipmentStatusCommunity.objects.count())

        serializer_without_root = EquipmentStatusCommunitySerializer(
            data=data_without_root, context={"request": None}
        )
        self.assertFalse(serializer_without_root.is_valid())

        serializer_without_community = EquipmentStatusCommunitySerializer(
            data=data_without_community, context={"request": None}
        )
        self.assertFalse(serializer_without_community.is_valid())

    def test_root(self):
        esco = EquipmentStatusCommunity.objects.get(id=1)
        self.assertEqual(esco.root_id, 1)

    def test_community(self):
        esco = EquipmentStatusCommunity.objects.get(id=1)
        self.assertEqual(esco.community_id, 1)

    def test_get_all(self, url_name=None):
        """
        Ensure the result has all attributes in 'EquipmentStatusCommunity'
        """
        esco = EquipmentStatusCommunity.objects.first()
        if not url_name:
            path = self.list_url
        else:
            path = reverse(
                url_name, kwargs={"parent_lookup_community_id": esco.community_id}
            )
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(path)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": f"http://testserver{path}?page%5Bnumber%5D=1",
                "last": f"http://testserver{path}?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "EquipmentStatusCommunity",
                    "id": encoding.force_str(esco.pk),
                    "attributes": {
                        "term": esco.term,
                        "abbreviation": esco.abbreviation,
                        "definition": esco.definition,
                        "provenance": esco.provenance,
                        "provenance-uri": esco.provenance_uri,
                        "note": esco.note,
                    },
                    "relationships": {
                        "root": {
                            "links": {
                                "self": "http://testserver/api/v1/equipmentstatus-communities/1/relationships/root",
                                "related": "http://testserver/api/v1/equipmentstatus-communities/1/root/",
                            },
                            "data": {
                                "type": "EquipmentStatus",
                                "id": encoding.force_str(esco.root_id),
                            },
                        },
                        "community": {
                            "links": {
                                "self": "http://testserver/api/v1/equipmentstatus-communities/1/relationships/community",
                                "related": "http://testserver/api/v1/equipmentstatus-communities/1/community/",
                            },
                            "data": {
                                "type": "Community",
                                "id": encoding.force_str(esco.community_id),
                            },
                        },
                    },
                    "links": {
                        "self": "http://testserver/api/v1/equipmentstatus-communities/1/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """
        test sort
        """
        BaseTestCase().sort(self.list_url, self.client)

    def test_community_routes(self):
        """
        test community routes
        """
        self.test_get_all(url_name="community-equipmentstatus-list")

    def test_community_filter(self):
        """
        filter for a communities term
        """
        response = self.client.get(
            self.list_url, data={"filter[community.term]": "Test Community"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)

    def test_rootstatus_filter(self):
        """
        filter for a root status
        """
        response = self.client.get(
            self.list_url, data={"filter[root.status]": "PENDING"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)
