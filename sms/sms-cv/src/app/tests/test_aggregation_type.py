# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Test classes for aggregation types."""

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import AggregationType, GlobalProvenance
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase


class AggregationTypeTestCase(TestCase):
    """Test class for aggregation types."""

    list_url = reverse("aggregationtype-list")

    def setUp(self):
        """Set some test data up for the tests."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()
        gl2 = GlobalProvenance.objects.create(
            id=2,
            name="test global provenance 2",
            description="test global provenance description 2",
            uri="test global provenance uri 2",
        )
        gl2.save()

        at = AggregationType.objects.create(
            id=1,
            term="Sum",
            definition=None,
            provenance=None,
            provenance_uri=None,
            category=None,
            note=None,
            global_provenance_id=1,
            successor_id=None,
        )
        at.save()
        self.detail_url = reverse(
            "aggregationtype-detail", kwargs={"pk": AggregationType.pk}
        )

    def test_term(self):
        """Ensure that the element has a term."""
        at = AggregationType.objects.get(id=1)
        self.assertEqual(at.term, "Sum")

    def test_global_provenance(self):
        """Ensure that the database object has a global provenance."""
        at = AggregationType.objects.get(id=1)
        self.assertEqual(at.global_provenance_id, 1)

    def test_get_all(self):
        """Ensure the result has all attributes in 'AggregationType'."""
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        at = AggregationType.objects.all()[0]
        expected = {
            "links": {
                "first": "http://testserver/api/v1/aggregationtypes/?page%5Bnumber%5D=1",
                "last": "http://testserver/api/v1/aggregationtypes/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "AggregationType",
                    "id": encoding.force_str(at.pk),
                    "attributes": {
                        "term": at.term,
                        "definition": at.definition,
                        "provenance": at.provenance,
                        "provenance-uri": at.provenance_uri,
                        "category": at.category,
                        "note": at.note,
                        "status": at.status,
                        "requested-by-email": at.requested_by_email,
                    },
                    "relationships": {
                        "measured-quantities": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "http://testserver/api/v1/aggregationtypes/1/relationships/measured_quantities",
                                "related": "http://testserver/api/v1/aggregationtypes/1/measured_quantities/",
                            },
                        },
                        "global-provenance": {
                            "links": {
                                "self": "http://testserver/api/v1/aggregationtypes/1/relationships/global_provenance",
                                "related": "http://testserver/api/v1/aggregationtypes/1/global_provenance/",
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(at.global_provenance_id),
                            },
                        },
                        "successor": {
                            "links": {
                                "self": "http://testserver/api/v1/aggregationtypes/1/relationships/successor",
                                "related": "http://testserver/api/v1/aggregationtypes/1/successor/",
                            }
                        },
                    },
                    "links": {"self": "http://testserver/api/v1/aggregationtypes/1/"},
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_404_error_pointer(self):
        """Ensure the error message for an 404 error of the get-details query."""
        not_found_url = reverse("aggregationtype-detail", kwargs={"pk": 100})
        errors = {
            "errors": [
                {
                    "detail": "No AggregationType matches the given query.",
                    "status": "404",
                    "code": "not_found",
                }
            ]
        }

        response = self.client.get(not_found_url)
        assert 404 == response.status_code
        assert errors == response.json()

    def test_sort(self):
        """Ensure that sorting of the elements works."""
        self.add_another_object(2, "Test")
        self.add_another_object(3, "Another Test")
        BaseTestCase().sort(self.list_url, self.client)

    def test_sort_reverse(self):
        """Ensure that a reversed sorting works."""
        self.add_another_object(2, "Test")
        self.add_another_object(3, "Another Test")
        BaseTestCase().sort_reverse(self.list_url, self.client)

    def test_filter_exact(self):
        """Ensure the filter for an exact match works."""
        self.add_another_object(2, "Test")
        self.add_another_object(3, "Another Test")
        BaseTestCase().filter_exact(self.list_url, self.client, "Sum")

    def add_another_object(self, id, name):
        """Add some more aggregation type object to the database."""
        at1 = AggregationType.objects.create(
            id=id,
            term=name,
            definition=None,
            provenance=None,
            provenance_uri=None,
            category=None,
            note=None,
            global_provenance_id=1,
            successor_id=None,
        )
        at1.save()

    def test_filter_exact_fail(self):
        """Ensure a failed search for a missing exact match."""
        BaseTestCase().filter_exact_fail(self.list_url, self.client)

    def test_filter_isnull(self):
        """Ensure that we can filter for null values."""
        entries = AggregationType.objects.all()
        BaseTestCase().filter_isnull(self.list_url, self.client, entries)

    def test_filter_related(self):
        """Ensure that we can filter by attributes of related elements."""
        gl = GlobalProvenance.objects.create(
            id=3,
            name="SMS",
            description="test global provenance2",
            uri="test global provenance uri2",
        )
        gl.save()
        at1 = AggregationType.objects.create(
            id=44,
            term="test_filter_related",
            definition=None,
            provenance=None,
            provenance_uri=None,
            category=None,
            note=None,
            global_provenance_id=3,
            successor_id=None,
        )
        at1.save()
        data = {"filter[global_provenance__name]": "SMS"}

        BaseTestCase().filter_related(self.list_url, self.client, data, id=at1.id)

    def test_filter_no_brackets(self):
        """Test for `filter=foobar` with missing filter[association] name."""
        BaseTestCase().filter_no_brackets(self.list_url, self.client)

    def test_filter_missing_right_bracket(self):
        """Test the filter parameter with a missing right bracket."""
        BaseTestCase().filter_missing_right_bracket(self.list_url, self.client)

    def test_search_keywords_no_match(self):
        """Ensure that we can get empty results based on our filter[search] attribute."""
        self.add_another_object(2, "Test")
        self.add_another_object(3, "Another test")
        expected_result = {
            "links": {
                "first": "http://testserver/api/v1/aggregationtypes/?filter%5Bsearch%5D=nothing&page%5Bnumber%5D=1",
                "last": "http://testserver/api/v1/aggregationtypes/?filter%5Bsearch%5D=nothing&page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 0}},
        }
        BaseTestCase().search_keywords_no_match(
            self.list_url, self.client, expected_result
        )

    def test_admin(self):
        """Run some tests for the admin interface."""
        obj = AggregationType.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        # test_admin.delete_view_responding()
