# SPDX-FileCopyrightText: 2022 - 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Tests for the countries."""

from django.test import TestCase
from django.urls import reverse
from django.utils import encoding

from app.models import Country, GlobalProvenance


class CountryTestCase(TestCase):
    """Test cases for the countries."""

    list_url = reverse("country-list")

    def setUp(self):
        """Set up some example data."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()
        self.country = Country.objects.create(
            global_provenance_id=gl.id,
            iso_code="DEU",
            term="Germany",
            status="ACCEPTED",
            definition="Federal Republic of Germany",
        )
        self.detail_url = reverse("country-detail", kwargs={"pk": self.country.pk})

    def test_str(self):
        """Test that the str gives the name."""
        as_str = str(self.country)
        self.assertEqual(as_str, "Germany")

    def test_get_all(self):
        """Test that we can get all the countries in a list."""
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": "http://testserver/api/v1/countries/?page%5Bnumber%5D=1",
                "last": "http://testserver/api/v1/countries/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "Country",
                    "id": str(self.country.id),
                    "attributes": {
                        "term": self.country.term,
                        "definition": "Federal Republic of Germany",
                        "provenance": None,
                        "provenance_uri": None,
                        "category": None,
                        "note": None,
                        "status": "ACCEPTED",
                        "iso_code": "DEU",
                        "requested_by_email": self.country.requested_by_email,
                    },
                    "relationships": {
                        "global_provenance": {
                            "links": {
                                "self": "".join(
                                    [
                                        f"http://testserver/api/v1/countries/",
                                        f"{self.country.id}/relationships/global_provenance",
                                    ]
                                ),
                                "related": f"http://testserver/api/v1/countries/{self.country.id}/global_provenance/",
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(
                                    self.country.global_provenance_id
                                ),
                            },
                        },
                        "successor": {
                            "links": {
                                "self": f"http://testserver/api/v1/countries/{self.country.id}/relationships/successor",
                                "related": f"http://testserver/api/v1/countries/{self.country.id}/successor/",
                            }
                        },
                    },
                    "links": {
                        "self": f"http://testserver/api/v1/countries/{self.country.id}/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_get_detail(self):
        """Test that we can get one country."""
        response = self.client.get(self.detail_url)
        self.assertEqual(response.status_code, 200)

        expected = {
            "data": {
                "type": "Country",
                "id": str(self.country.id),
                "attributes": {
                    "term": self.country.term,
                    "definition": "Federal Republic of Germany",
                    "provenance": None,
                    "provenance_uri": None,
                    "category": None,
                    "note": None,
                    "status": "ACCEPTED",
                    "iso_code": "DEU",
                    "requested_by_email": self.country.requested_by_email,
                },
                "relationships": {
                    "global_provenance": {
                        "links": {
                            "self": "".join(
                                [
                                    f"http://testserver/api/v1/countries/",
                                    f"{self.country.id}/relationships/global_provenance",
                                ]
                            ),
                            "related": f"http://testserver/api/v1/countries/{self.country.id}/global_provenance/",
                        },
                        "data": {
                            "type": "GlobalProvenance",
                            "id": encoding.force_str(self.country.global_provenance_id),
                        },
                    },
                    "successor": {
                        "links": {
                            "self": f"http://testserver/api/v1/countries/{self.country.id}/relationships/successor",
                            "related": f"http://testserver/api/v1/countries/{self.country.id}/successor/",
                        }
                    },
                },
                "links": {
                    "self": f"http://testserver/api/v1/countries/{self.country.id}/"
                },
            }
        }
        assert expected == response.json()
