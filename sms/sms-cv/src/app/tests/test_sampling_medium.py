# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Tests for the sampling media."""

import io

import rdflib
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding
from rest_framework import status

from app.models import (
    AggregationType,
    Compartment,
    GlobalProvenance,
    MeasuredQuantity,
    SamplingMedium,
)
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase, GraphAssertionMixin


class SamplingMediumTestCase(TestCase, GraphAssertionMixin):
    """Test case for the sampling meida."""

    list_url = reverse("samplingmedium-list")

    def setUp(self):
        """Set some data up, so that we can use them in the tests."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()
        c = Compartment.objects.create(
            id=1,
            term="Test Compartment",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        c.save()

        sm = SamplingMedium.objects.create(
            id=1,
            term="Test SamplingMedium",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            compartment_id=1,
        )
        sm.save()
        self.detail_url = reverse(
            "samplingmedium-detail", kwargs={"pk": SamplingMedium.pk}
        )

    def test_term(self):
        """Test the term field."""
        at = SamplingMedium.objects.get(id=1)
        self.assertEqual(at.term, "Test SamplingMedium")

    def test_delete(self):
        """Ensure that we can't delete a sampling medium using the views."""
        sm = SamplingMedium.objects.get(id=1)
        url = reverse("samplingmedium-detail", kwargs={"pk": sm.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_put(self):
        """Ensure that we can't change our data with using the views."""
        sm = SamplingMedium.objects.get(id=1)
        url = reverse("samplingmedium-detail", kwargs={"pk": sm.pk})
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        data = get_response.json()

        content_type = "application/vnd.api+json"
        put_response = self.client.put(url, data=data, content_type=content_type)
        self.assertEqual(put_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post(self):
        """Ensure that we can't add new data with the normal post request."""
        sm = SamplingMedium.objects.get(id=1)
        url = reverse("samplingmedium-detail", kwargs={"pk": sm.pk})
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        data = get_response.json()
        data["data"]["id"] = None

        # To make sure we don't run into unique constraints
        sm.delete()

        content_type = "application/vnd.api+json"
        post_response = self.client.post(
            self.list_url, data=data, content_type=content_type
        )
        self.assertEqual(post_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_relationship(self):
        """Ensure that we can't delete using the relationship view."""
        sm = SamplingMedium.objects.get(id=1)
        url = reverse(
            "sampling_medium-relationships",
            kwargs={"pk": sm.pk, "related_field": "compartment"},
        )
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_put_relationship(self):
        """Ensure that we can't override using the relationship view."""
        sm = SamplingMedium.objects.get(id=1)
        url = reverse(
            "sampling_medium-relationships",
            kwargs={"pk": sm.pk, "related_field": "compartment"},
        )
        content_type = "application/vnd.api+json"
        response = self.client.put(url, data={}, content_type=content_type)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_post_relationship(self):
        """Ensure that we can't add using the relationship view."""
        sm = SamplingMedium.objects.get(id=1)
        url = reverse(
            "sampling_medium-relationships",
            kwargs={"pk": sm.pk, "related_field": "compartment"},
        )
        content_type = "application/vnd.api+json"
        response = self.client.post(url, data={}, content_type=content_type)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_global_provenance(self):
        """Ensure we link to a global provenance."""
        at = SamplingMedium.objects.get(id=1)
        self.assertEqual(at.global_provenance_id, 1)
        self.assertEqual(at.global_provenance.name, "test global provenance")

    def test_compartment(self):
        """Ensure we link to a compartment."""
        at = SamplingMedium.objects.get(id=1)
        self.assertEqual(at.compartment_id, 1)
        self.assertEqual(at.compartment.term, "Test Compartment")

    def test_get_all(self):
        """Ensure the result of the get request has all attributes in 'SamplingMedium'."""
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        m = SamplingMedium.objects.all()[0]
        expected = {
            "links": {
                "first": "http://testserver/api/v1/samplingmedia/?page%5Bnumber%5D=1",
                "last": "http://testserver/api/v1/samplingmedia/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "SamplingMedium",
                    "id": encoding.force_str(m.pk),
                    "attributes": {
                        "term": m.term,
                        "definition": m.definition,
                        "provenance": m.provenance,
                        "provenance-uri": m.provenance_uri,
                        "category": m.category,
                        "note": m.note,
                        "status": m.status,
                        "requested-by-email": m.requested_by_email,
                    },
                    "relationships": {
                        "measured-quantities": {
                            "meta": {"count": 0},
                            "data": [],
                            "links": {
                                "self": "http://testserver/api/v1/samplingmedia/1/relationships/measured_quantities",
                                "related": "http://testserver/api/v1/samplingmedia/1/measured_quantities/",
                            },
                        },
                        "global-provenance": {
                            "links": {
                                "self": "http://testserver/api/v1/samplingmedia/1/relationships/global_provenance",
                                "related": "http://testserver/api/v1/samplingmedia/1/global_provenance/",
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(m.global_provenance_id),
                            },
                        },
                        "compartment": {
                            "links": {
                                "self": "http://testserver/api/v1/samplingmedia/1/relationships/compartment",
                                "related": "http://testserver/api/v1/samplingmedia/1/compartment/",
                            },
                            "data": {
                                "type": "Compartment",
                                "id": encoding.force_str(m.compartment_id),
                            },
                        },
                        "successor": {
                            "links": {
                                "self": "http://testserver/api/v1/samplingmedia/1/relationships/successor",
                                "related": "http://testserver/api/v1/samplingmedia/1/successor/",
                            }
                        },
                    },
                    "links": {"self": "http://testserver/api/v1/samplingmedia/1/"},
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Ensure the sorting works as expected."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """Ensure we can use the admin interface."""
        obj = SamplingMedium.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        # test_admin.delete_view_responding()

    def test_rdf_output(self):
        """Ensure we can deliver the data for one sampling medium also in RDF format."""
        sm = SamplingMedium.objects.get(id=1)
        response = self.client.get(
            f"{self.list_url}{sm.id}/", headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{sm.id}/"))

        # Now lets check the content.
        self.assertTripleInGraph(
            graph, uri_ref, rdflib.namespace.RDF.type, rdflib.namespace.SKOS.Concept
        )
        self.assertTripleInGraph(
            graph,
            uri_ref,
            rdflib.namespace.SKOS.prefLabel,
            rdflib.Literal(sm.term, lang="en"),
        )
        self.assertTripleInGraph(
            graph,
            uri_ref,
            rdflib.namespace.SKOS.definition,
            rdflib.Literal(sm.definition, lang="en"),
        )
        # And we have the broader definition for the compartment
        compartment_list_url = reverse("compartment-list")
        broader_triples = self.assertTripleInGraph(
            graph, uri_ref, rdflib.namespace.SKOS.broader, None
        )
        self.assertEqual(len(broader_triples), 1)
        broader_triples[0][2].toPython().endswith(
            f"{compartment_list_url}{sm.compartment_id}/"
        )

        self.assertIsNone(sm.provenance_uri)
        # As we don't have a provenance uri set, we expect that we don't add
        # an exactMatch relation.
        # None is a placeholder for any kind of value.
        self.assertTripleNotInGraph(
            graph, uri_ref, rdflib.namespace.SKOS.exactMatch, None
        )

        # However, we can add it.
        sm.provenance_uri = "https://some.definition.in/the/web"
        sm.save()

        # And fetch the content again...
        response = self.client.get(
            f"{self.list_url}{sm.id}/", headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")

        self.assertTripleInGraph(
            graph,
            uri_ref,
            rdflib.namespace.SKOS.exactMatch,
            rdflib.URIRef(sm.provenance_uri),
        )
        # And in case that we have a measured quantity that links to the sampling medium we
        # want that this one is linked in the rdf.
        at = AggregationType.objects.create(
            id=1,
            term="Sum",
            definition=None,
            provenance=None,
            provenance_uri=None,
            category=None,
            note=None,
            global_provenance_id=1,
            successor_id=None,
        )
        at.save()
        mq = MeasuredQuantity.objects.create(
            id=1,
            term="Test MeasuredQuantity",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            sampling_media_id=sm.id,
            aggregation_type_id=1,
        )
        response = self.client.get(
            f"{self.list_url}{sm.id}/", headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")
        measured_quantity_list_url = reverse("measuredquantity-list")
        narrower_triples = self.assertTripleInGraph(
            graph, uri_ref, rdflib.namespace.SKOS.narrower, None
        )
        self.assertEqual(len(narrower_triples), 1)
        narrower_triples[0][2].toPython().endswith(
            f"{measured_quantity_list_url}{mq.id}/"
        )

        # And we can fetch the content of the list view.
        response = self.client.get(
            self.list_url, headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph_for_list = rdflib.Graph()
        graph_for_list.parse(io.BytesIO(response.getvalue()), format="xml")

        # And in the graph for the list endpoint are all the entries of the graph
        # for a single element.
        for subject, predicate, object_ in graph.triples((None, None, None)):
            self.assertTripleInGraph(graph_for_list, subject, predicate, object_)

        # And we want to ensure that we don't adjust the links, due to the .xml format
        # suffix.
        response = self.client.get(f"{self.list_url}{sm.id}.xml/")
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{sm.id}/"))

    def test_ttl_output(self):
        """Ensure we can also deliver the RDF data in TTL format."""
        sm = SamplingMedium.objects.get(id=1)
        response = self.client.get(
            f"{self.list_url}{sm.id}/", headers={"accept": "application/x-turtle"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="ttl")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{sm.id}/"))

        # And the same also for the .ttl format endpoint
        response = self.client.get(f"{self.list_url}{sm.id}.ttl/")
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="ttl")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{sm.id}/"))
