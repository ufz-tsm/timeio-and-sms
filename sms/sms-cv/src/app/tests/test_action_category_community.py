# SPDX-FileCopyrightText: 2021 - 2023
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

import copy

from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import (
    ActionCategory,
    ActionCategoryCommunity,
    Community,
    GlobalProvenance,
)
from app.serializers import ActionCategoryCommunitySerializer
from app.tests.Base_class import BaseTestCase


class ActionCategoryCommunityTestCase(TestCase):
    list_url = reverse("actioncategorycommunity-list")

    def setUp(self):
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()

        ac = ActionCategory.objects.create(
            id=1,
            term="Test Action Category",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        ac.save()

        co = Community.objects.create(
            id=1,
            term="Test Community",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
        )
        co.save()

        acco = ActionCategoryCommunity.objects.create(
            id=1,
            term="Test Action Category Community",
            abbreviation="TACC",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            note="Test1",
            community_id=1,
            root_id=1,
        )
        acco.save()
        self.detail_url = reverse(
            "actioncategorycommunity-detail", kwargs={"pk": ActionCategoryCommunity.pk}
        )

    def test_str(self):
        """
        ActionCategoryCommunity
        :return:
        """
        acco = ActionCategoryCommunity.objects.get(id=1)
        self.assertEqual(acco.term, "Test Action Category Community")

    def test_serializer(self):
        """
        Test the serializer.

        It should just work fine with the given data.
        But the root & the community are both required.
        """
        acco = ActionCategoryCommunity.objects.get(id=1)
        data = ActionCategoryCommunitySerializer(acco, context={"request": None}).data
        data_without_root = copy.copy(data)
        data_without_root["root"] = None
        data_without_community = copy.copy(data)
        data_without_community["community"] = None

        # delete, so that we don't run into unique constraints
        acco.delete()

        serializer_ok = ActionCategoryCommunitySerializer(
            data=data, context={"request": None}
        )
        self.assertTrue(serializer_ok.is_valid())

        # We still don't save it.
        self.assertEqual(0, ActionCategoryCommunity.objects.count())

        serializer_without_root = ActionCategoryCommunitySerializer(
            data=data_without_root, context={"request": None}
        )
        self.assertFalse(serializer_without_root.is_valid())

        serializer_without_community = ActionCategoryCommunitySerializer(
            data=data_without_community, context={"request": None}
        )
        self.assertFalse(serializer_without_community.is_valid())

    def test_root(self):
        acco = ActionCategoryCommunity.objects.get(id=1)
        self.assertEqual(acco.root_id, 1)

    def test_community(self):
        acco = ActionCategoryCommunity.objects.get(id=1)
        self.assertEqual(acco.community_id, 1)

    def test_get_all(self, url_name=None):
        """
        Ensure the result has all attributes in 'ActionCategoryCommunity'
        """
        acco = ActionCategoryCommunity.objects.first()
        if not url_name:
            path = self.list_url
        else:
            path = reverse(
                url_name, kwargs={"parent_lookup_community_id": acco.community_id}
            )

        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(path)
        self.assertEqual(response.status_code, 200)

        expected = {
            "links": {
                "first": f"http://testserver{path}?page%5Bnumber%5D=1",
                "last": f"http://testserver{path}?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "ActionCategoryCommunity",
                    "id": encoding.force_str(acco.pk),
                    "attributes": {
                        "term": acco.term,
                        "abbreviation": acco.abbreviation,
                        "definition": acco.definition,
                        "provenance": acco.provenance,
                        "provenance-uri": acco.provenance_uri,
                        "note": acco.note,
                    },
                    "relationships": {
                        "root": {
                            "links": {
                                "self": "http://testserver/api/v1/actioncategories-communities/1/relationships/root",
                                "related": "http://testserver/api/v1/actioncategories-communities/1/root/",
                            },
                            "data": {
                                "type": "ActionCategory",
                                "id": encoding.force_str(acco.root_id),
                            },
                        },
                        "community": {
                            "links": {
                                "self": "http://testserver/api/v1/actioncategories-communities/1/relationships/community",
                                "related": "http://testserver/api/v1/actioncategories-communities/1/community/",
                            },
                            "data": {
                                "type": "Community",
                                "id": encoding.force_str(acco.community_id),
                            },
                        },
                    },
                    "links": {
                        "self": "http://testserver/api/v1/actioncategories-communities/1/"
                    },
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }

        assert expected == response.json()

    def test_community_routes(self):
        """
        test community routes
        """
        self.test_get_all(url_name="community-actioncategories-list")

    def test_sort(self):
        """
        test sort
        """
        BaseTestCase().sort(self.list_url, self.client)

    def test_community_filter(self):
        """
        filter for a communities term
        """
        response = self.client.get(
            self.list_url, data={"filter[community.term]": "Test Community"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)

    def test_rootstatus_filter(self):
        """
        filter for a root status
        """
        response = self.client.get(
            self.list_url, data={"filter[root.status]": "PENDING"}
        )
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)
