# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework.test import APITestCase


class BaseTestCase(APITestCase):
    def sort(self, url, client):
        """
        test sort
        """
        response = client.get(url, data={"sort": "term"})
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        headlines = [c["attributes"]["term"] for c in dja_response["data"]]
        sorted_headlines = sorted(headlines)
        self.assertEqual(headlines, sorted_headlines)

    def sort_reverse(self, url, client):
        """
        confirm switching the sort order actually works
        """
        response = client.get(url, data={"sort": "-term"})
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        headlines = [c["attributes"]["term"] for c in dja_response["data"]]
        sorted_headlines = sorted(headlines)
        self.assertNotEqual(headlines, sorted_headlines)

    def filter_exact(self, url, client, f_term):
        """
        filter for an exact match
        """
        response = client.get(url, data={"filter[term]": f_term})
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 1)

    def filter_exact_fail(self, url, client):
        """
        failed search for an exact match
        """
        response = client.get(url, data={"filter[term]": "XXXXX"})
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(len(dja_response["data"]), 0)

    def filter_isnull(self, url, client, entries):
        """
        search for null value
        """
        response = client.get(url, data={"filter[definition.isnull]": "true"})
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(
            len(dja_response["data"]),
            len([k for k in entries if k.definition is None]),
        )

    def filter_related(self, url, client, data, id):
        """
        filter via a relationship chain
        """
        response = client.get(url, data=data)
        self.assertEqual(
            response.status_code, 200, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()

        self.assertEqual(dja_response["meta"]["pagination"]["count"], 1)
        self.assertEqual(dja_response["data"][0]["id"], str(id))
        all = client.get(url)
        all_response = all.json()
        self.assertNotEqual(all_response["meta"]["pagination"]["count"], 1)

    def filter_no_brackets(self, url, client):
        """
        test for `filter=foobar` with missing filter[association] name
        """
        response = client.get(url, data={"filter": "foobar"})
        self.assertEqual(
            response.status_code, 400, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(
            dja_response["errors"][0]["detail"], "invalid query parameter: filter"
        )

    def filter_missing_right_bracket(self, url, client):
        """
        test for filter missing right bracket
        """
        response = client.get(url, data={"filter[term": "foobar"})
        self.assertEqual(
            response.status_code, 400, msg=response.content.decode("utf-8")
        )
        dja_response = response.json()
        self.assertEqual(
            dja_response["errors"][0]["detail"],
            "invalid query parameter: filter[term",
        )

    def search_keywords_no_match(self, url, client, expected_result):
        """
        test for `filter[search]="keywords"` where some of the keywords are in the entry and
        others are in the related blog.
        """
        response = client.get(url, data={"filter[search]": "nothing"})
        assert response.json() == expected_result


class GraphAssertionMixin:
    """Mixin class to make assertions with graph objects easier."""

    def assertTripleInGraph(self, graph, subject, predicate, object_):
        """Ensure that the triple specification is in the graph."""
        triples = list(graph.triples((subject, predicate, object_)))
        self.assertTrue(len(triples) > 0)
        return triples

    def assertTripleNotInGraph(self, graph, subject, predicate, object_):
        """Ensure that the triple specification is not in the graph."""
        triples = list(graph.triples((subject, predicate, object_)))
        self.assertTrue(len(triples) == 0)
