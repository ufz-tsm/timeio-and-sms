# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Tests for measured quantities."""

import io

import rdflib
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import encoding

from app.models import (
    AggregationType,
    Compartment,
    GlobalProvenance,
    MeasuredQuantity,
    SamplingMedium,
)
from app.tests.Admin_class import TestAdminInterface
from app.tests.Base_class import BaseTestCase, GraphAssertionMixin


class MeasuredQuantityTestCase(TestCase, GraphAssertionMixin):
    """Test class for measured quantities."""

    list_url = reverse("measuredquantity-list")

    def setUp(self):
        """Set some data up, so that we can work with them in the tests."""
        gl = GlobalProvenance.objects.create(
            id=1,
            name="test global provenance",
            description="test global provenance description",
            uri="test global provenance uri",
        )
        gl.save()
        c = Compartment.objects.create(
            id=1,
            term="Test Compartment",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
        )
        c.save()
        at = AggregationType.objects.create(
            id=1,
            term="Sum",
            definition=None,
            provenance=None,
            provenance_uri=None,
            category=None,
            note=None,
            global_provenance_id=1,
            successor_id=None,
        )
        at.save()
        sm = SamplingMedium.objects.create(
            id=1,
            term="Test SamplingMedium",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            compartment_id=1,
        )
        sm.save()

        mq = MeasuredQuantity.objects.create(
            id=1,
            term="Test MeasuredQuantity",
            definition="Test definition",
            provenance=None,
            provenance_uri=None,
            category="Test",
            note="Test1",
            global_provenance_id=1,
            successor_id=None,
            sampling_media_id=1,
            aggregation_type_id=1,
        )
        mq.save()
        self.detail_url = reverse(
            "measuredquantity-detail", kwargs={"pk": MeasuredQuantity.pk}
        )

    def test_str(self):
        """Test the str() handling."""
        mq = MeasuredQuantity.objects.get(id=1)
        self.assertEqual(mq.term, "Test MeasuredQuantity")

    def test_global_provenance(self):
        """Test the relation to the global provenance."""
        mq = MeasuredQuantity.objects.get(id=1)
        self.assertEqual(mq.global_provenance_id, 1)
        self.assertEqual(mq.global_provenance.name, "test global provenance")

    def test_sampling_medium(self):
        """Test that we have the link to the sampling medium."""
        mq = MeasuredQuantity.objects.get(id=1)
        self.assertEqual(mq.sampling_media_id, 1)
        self.assertEqual(mq.sampling_media.term, "Test SamplingMedium")

    def test_get_all(self):
        """Ensure the result has all attributes in 'MeasuredQuantity'."""
        with override_settings(JSON_API_FORMAT_FIELD_NAMES="dasherize"):
            response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        m = MeasuredQuantity.objects.all()[0]
        expected = {
            "links": {
                "first": "http://testserver/api/v1/measuredquantities/?page%5Bnumber%5D=1",
                "last": "http://testserver/api/v1/measuredquantities/?page%5Bnumber%5D=1",
                "next": None,
                "prev": None,
            },
            "data": [
                {
                    "type": "MeasuredQuantity",
                    "id": encoding.force_str(m.pk),
                    "attributes": {
                        "term": m.term,
                        "definition": m.definition,
                        "provenance": m.provenance,
                        "provenance-uri": m.provenance_uri,
                        "category": m.category,
                        "note": m.note,
                        "status": m.status,
                        "requested-by-email": m.requested_by_email,
                    },
                    "relationships": {
                        "sampling-media": {
                            "links": {
                                "self": "http://testserver/api/v1/measuredquantities/1/relationships/sampling_media",
                                "related": "http://testserver/api/v1/measuredquantities/1/sampling_media/",
                            },
                            "data": {
                                "type": "SamplingMedium",
                                "id": encoding.force_str(m.sampling_media_id),
                            },
                        },
                        "aggregation-type": {
                            "links": {
                                "self": "http://testserver/api/v1/measuredquantities/1/relationships/aggregation_type",
                                "related": "http://testserver/api/v1/measuredquantities/1/aggregation_type/",
                            },
                            "data": {
                                "type": "AggregationType",
                                "id": encoding.force_str(m.aggregation_type_id),
                            },
                        },
                        "global-provenance": {
                            "links": {
                                "self": "http://testserver/api/v1/measuredquantities/1/relationships/global_provenance",
                                "related": "http://testserver/api/v1/measuredquantities/1/global_provenance/",
                            },
                            "data": {
                                "type": "GlobalProvenance",
                                "id": encoding.force_str(m.global_provenance_id),
                            },
                        },
                        "measured-quantity-units": {
                            "links": {
                                "self": "http://testserver/api/v1/measuredquantities/1/relationships/measured_quantity_units",
                                "related": "http://testserver/api/v1/measuredquantities/1/measured_quantity_units/",
                            }
                        },
                        "successor": {
                            "links": {
                                "self": "http://testserver/api/v1/measuredquantities/1/relationships/successor",
                                "related": "http://testserver/api/v1/measuredquantities/1/successor/",
                            }
                        },
                    },
                    "links": {"self": "http://testserver/api/v1/measuredquantities/1/"},
                }
            ],
            "meta": {"pagination": {"page": 1, "pages": 1, "count": 1}},
        }
        assert expected == response.json()

    def test_sort(self):
        """Test the ordering."""
        BaseTestCase().sort(self.list_url, self.client)

    def test_admin(self):
        """Test the admin interface."""
        obj = MeasuredQuantity.objects.get(id=1)
        test_admin = TestAdminInterface().setUpAdmin(self.client, obj)
        # test response list view
        test_admin.list_view_responding()
        # test response change view
        test_admin.add_view_responding()
        # test response change view
        test_admin.change_view_responding()
        # test response delete view
        # test_admin.delete_view_responding()

    def test_rdf_output(self):
        """Ensure we can deliver the data for one measured quantity also in RDF format."""
        mq = MeasuredQuantity.objects.get(id=1)
        response = self.client.get(
            f"{self.list_url}{mq.id}/", headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{mq.id}/"))

        # Now lets check the content.
        self.assertTripleInGraph(
            graph, uri_ref, rdflib.namespace.RDF.type, rdflib.namespace.SKOS.Concept
        )
        self.assertTripleInGraph(
            graph,
            uri_ref,
            rdflib.namespace.SKOS.prefLabel,
            rdflib.Literal(mq.term, lang="en"),
        )
        self.assertTripleInGraph(
            graph,
            uri_ref,
            rdflib.namespace.SKOS.definition,
            rdflib.Literal(mq.definition, lang="en"),
        )
        # And we have the broader definition for the sampling medium
        sampling_medium_list_url = reverse("samplingmedium-list")
        broader_triples = self.assertTripleInGraph(
            graph, uri_ref, rdflib.namespace.SKOS.broader, None
        )
        self.assertEqual(len(broader_triples), 1)
        broader_triples[0][2].toPython().endswith(
            f"{sampling_medium_list_url}{mq.sampling_media_id}/"
        )

        self.assertIsNone(mq.provenance_uri)
        # As we don't have a provenance uri set, we expect that we don't add
        # an exactMatch relation.
        # None is a placeholder for any kind of value.
        self.assertTripleNotInGraph(
            graph, uri_ref, rdflib.namespace.SKOS.exactMatch, None
        )

        # However, we can add it.
        mq.provenance_uri = "https://some.definition.in/the/web"
        mq.save()

        # And fetch the content again...
        response = self.client.get(
            f"{self.list_url}{mq.id}/", headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")

        self.assertTripleInGraph(
            graph,
            uri_ref,
            rdflib.namespace.SKOS.exactMatch,
            rdflib.URIRef(mq.provenance_uri),
        )

        # And we can fetch the content of the list view.
        response = self.client.get(
            self.list_url, headers={"accept": "application/rdf+xml"}
        )
        self.assertEqual(response.status_code, 200)

        graph_for_list = rdflib.Graph()
        graph_for_list.parse(io.BytesIO(response.getvalue()), format="xml")

        # And in the graph for the list endpoint are all the entries of the graph
        # for a single element.
        for subject, predicate, object_ in graph.triples((None, None, None)):
            self.assertTripleInGraph(graph_for_list, subject, predicate, object_)

        # And we want to ensure that we don't adjust the links, due to the .xml format
        # suffix.
        response = self.client.get(f"{self.list_url}{mq.id}.xml/")
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="xml")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{mq.id}/"))

    def test_ttl_output(self):
        """Ensure we can also deliver the RDF data in TTL format."""
        mq = MeasuredQuantity.objects.get(id=1)
        response = self.client.get(
            f"{self.list_url}{mq.id}/", headers={"accept": "application/x-turtle"}
        )
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="ttl")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{mq.id}/"))

        # And the same also for the .ttl format endpoint
        response = self.client.get(f"{self.list_url}{mq.id}.ttl/")
        self.assertEqual(response.status_code, 200)

        graph = rdflib.Graph()
        graph.parse(io.BytesIO(response.getvalue()), format="ttl")

        # We have only one subject in there.
        subjects = set(graph.subjects())
        self.assertEqual(1, len(subjects))

        uri_ref = list(subjects)[0]
        self.assertTrue(uri_ref.toPython().endswith(f"{self.list_url}{mq.id}/"))
