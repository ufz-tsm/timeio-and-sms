# SPDX-FileCopyrightText: 2020 - 2023
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Registration of the admin interface for the models."""

from django.contrib import admin

from app.admin.ActionCategoryAdmin import ActionCategoryAdmin

# admin models
from app.admin.ActionTypeAdmin import ActionTypeAdmin
from app.admin.AggregationTypeAdmin import AggregationTypeAdmin
from app.admin.CommunityAdmin import CommunityAdmin
from app.admin.CompartmentAdmin import CompartmentAdmin
from app.admin.ContactRoleAdmin import ContactRoleAdmin
from app.admin.EquipmentStatusAdmin import EquipmentStatusAdmin
from app.admin.EquipmentTypeAdmin import EquipmentTypeAdmin
from app.admin.GlobalProvenanceAdmin import GlobalProvenanceAdmin
from app.admin.ManufacturerAdmin import ManufacturerAdmin
from app.admin.MeasuredQuantityAdmin import MeasuredQuantityAdmin
from app.admin.PlatformTypeAdmin import PlatformTypeAdmin
from app.admin.SamplingMediumAdmin import SamplingMediumAdmin
from app.admin.SiteTypeAdmin import SiteTypeAdmin
from app.admin.SiteUsageAdmin import SiteUsageAdmin
from app.admin.SoftwareTypeAdmin import SoftwareTypeAdmin
from app.admin.UnitAdmin import UnitAdmin

# models
from app.models import ContactRole, Country, License
from app.models.action_category import ActionCategory
from app.models.action_type import ActionType
from app.models.aggregation_type import AggregationType
from app.models.community import Community
from app.models.compartment import Compartment
from app.models.equipment_status import EquipmentStatus
from app.models.equipment_type import EquipmentType
from app.models.global_provenance import GlobalProvenance
from app.models.manufacturer import Manufacturer
from app.models.measured_quantity import MeasuredQuantity
from app.models.platform_type import PlatformType
from app.models.sampling_medium import SamplingMedium
from app.models.site_type import SiteType
from app.models.site_usage import SiteUsage
from app.models.software_type import SoftwareType
from app.models.unit import Unit

# register
admin.site.register(ActionCategory, ActionCategoryAdmin)
admin.site.register(ActionType, ActionTypeAdmin)
admin.site.register(AggregationType, AggregationTypeAdmin)
admin.site.register(Community, CommunityAdmin)
admin.site.register(Compartment, CompartmentAdmin)
admin.site.register(ContactRole, ContactRoleAdmin)
admin.site.register(Country)
admin.site.register(EquipmentStatus, EquipmentStatusAdmin)
admin.site.register(EquipmentType, EquipmentTypeAdmin)
admin.site.register(GlobalProvenance, GlobalProvenanceAdmin)
admin.site.register(License)
admin.site.register(Manufacturer, ManufacturerAdmin)
admin.site.register(MeasuredQuantity, MeasuredQuantityAdmin)
admin.site.register(PlatformType, PlatformTypeAdmin)
admin.site.register(SamplingMedium, SamplingMediumAdmin)
admin.site.register(SiteType, SiteTypeAdmin)
admin.site.register(SiteUsage, SiteUsageAdmin)
admin.site.register(SoftwareType, SoftwareTypeAdmin)
admin.site.register(Unit, UnitAdmin)

# admin.site.register(ActionCategoryCommunity, ActionCategoryAdmin)
# admin.site.register(ActionTypeCommunity, ActionTypeCommunityAdmin)


# This line here is necessary due to a change in Django 3.2
# https://docs.djangoproject.com/en/3.2/releases/3.2/
# https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#django.contrib.admin.AdminSite.final_catch_all_view
# We need to set it to false in order to allow access of the
# static files when we still want to run the admin panel on the
# root url
# Having this setting true will not allow static files under
# /static/foo/whatever.xyz
# when we run the admin interface under /
# as this setting tries to improve security (otherwise it would be
# possible to get informatuion about how many models are there).
# As all of our models will be publically available we don't need
# to care that much. Even exposing the number of users will not
# be harmful at this point.
# We would be able to work around this by using an nginx for example,
# but for the local development it is easier to stay with django only.
admin.site.final_catch_all_view = False
