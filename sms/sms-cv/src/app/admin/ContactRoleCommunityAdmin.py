# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Community adjustments for the contact role community admin panels."""

from app.admin.TermCommunityAdmin import (
    TermCommunityAdminInlineEdit,
    TermCommunityAdminInlineReadonly,
)
from app.models import ContactRoleCommunity


class ContactRoleCommunityAdmin(TermCommunityAdminInlineEdit):
    """Inline admin panel for the communities."""

    model = ContactRoleCommunity


class ContactRoleCommunityAdminReadonly(TermCommunityAdminInlineReadonly):
    """Readonly admin panel for the communities."""

    model = ContactRoleCommunity
