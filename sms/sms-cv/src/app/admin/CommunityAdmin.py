# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

from app.admin.TermAdmin import TermAdmin
from app.models import Community


class CommunityAdmin(TermAdmin):

    list_display_links = ["term"]

    list_display = (
        "term",
        "definition",
        "provenance",
        "provenance_uri",
        "category",
        "note",
        "term_status",
        "global_provenance",
    )

    fields = [
        "term",
        "definition",
        "provenance",
        "provenance_uri",
        "category",
        "note",
        "status",
        "global_provenance",
    ]

    # Tell Django to join global provenance to save hundreds of db queries
    list_select_related = ["global_provenance"]

    # overwrite making field "term" readonly
    def get_readonly_fields(self, request, obj=None):
        return []

    def get_queryset(self, request):
        qs = super(CommunityAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            communities = list(Community.objects.all().values_list("term", flat=True))
            groups = list(Group.objects.all().values_list("name", flat=True))
            missing_groups = list(set(communities) - set(groups))
            if missing_groups:
                for miss in missing_groups:
                    community_group = Group.objects.create(name=miss)
                    community_group.permissions.set(self._get_permissions())
        return qs

    # pass old object name before update to allocate corresponding group for update
    def get_form(self, request, obj=None, change=False, **kwargs):

        if obj:
            self.object_name = obj.term
        else:
            self.object_name = None
        return super(CommunityAdmin, self).get_form(
            request, obj=None, change=False, **kwargs
        )

    # create and update functionality for corresponding group including permissions
    def save_model(self, request, obj, form, change):
        existing_group = Group.objects.filter(name=self.object_name).first()
        if existing_group:
            existing_group.name = obj.term
            existing_group.save()
        else:
            community_group = Group.objects.create(name=obj.term)
            community_group.permissions.set(self._get_permissions())

        return super(CommunityAdmin, self).save_model(request, obj, form, change)

    # delete functionality for corresponding group
    def delete_model(self, request, obj):
        existing_group = Group.objects.filter(name=self.object_name).first()
        if existing_group:
            existing_group.delete()

        return super(CommunityAdmin, self).delete_model(request, obj)

    # create temporary superuser to get avail permissions and filter required ones
    def _get_permissions(self):
        tmp_superuser = get_user_model()(is_active=True, is_superuser=True)

        content_type = ContentType.objects.get_for_model(self.model)
        app_label = content_type.app_label

        # Due to a reason that is curreently unclear for me, it can be that the permissions that
        # we filter with a user directly with the Permissions filter
        #
        # Permission.object.filter(user=tmp_superuser)
        #
        # can miss some of the permissions to add new entries (we had this
        # for equipment types for example).
        # By using the get_all_permissions from the user object, we can get a complete list
        # that doesn't have this problem.
        # However, we need to filter for the codename then - which is the second part
        # of a string representation of the permission (app.add_equipmenttype)

        # {'app.edit_equipmenttype', 'auth.add_user', ...}
        permissions_str_set = tmp_superuser.get_all_permissions()
        # 'app.'
        app_label_dot = app_label + "."
        # {'edit_equipmenttype', 'view_manufacturer', 'add_community', ...}
        permissions_app_codenames = []
        for perm in permissions_str_set:
            if perm.startswith(app_label_dot):
                _empty_str, codename = perm.split(app_label_dot, 1)
                permissions_app_codenames.append(codename)

        # We need to have the real objects - and not their string representation
        permissions = Permission.objects.filter(
            codename__in=permissions_app_codenames,
            content_type__app_label=app_label,
        )
        permissions = permissions.exclude(
            content_type__model__in=["globalprovenance", "community", "actioncategory"]
        ).all()

        return permissions
