# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.urls import reverse
from django.utils.html import format_html

from app.admin.ActionTypeCommunityAdmin import (
    ActionTypeCommunityAdmin,
    ActionTypeCommunityAdminReadonly,
)
from app.admin.TermCommunityAdmin import TermCommunityAdmin


class ActionTypeAdmin(TermCommunityAdmin):

    # Tell Django to join foreign tables to save hundreds of db queries
    list_select_related = (
        "action_category",
        *TermCommunityAdmin.list_select_related,
    )

    list_filter = ("action_category", *TermCommunityAdmin.list_filter)

    fields = ("action_category", *TermCommunityAdmin.fields)

    def get_list_display(self, request):
        return ("link_action_category",) + super().get_list_display(request)

    def link_action_category(self, obj):
        view_name = f"admin:{obj._meta.app_label}_actioncategory_change"
        link_url = reverse(view_name, args=[obj.action_category_id])
        linkstr = f"<b><a href='{link_url}'>{obj.action_category}</a></b><br>"
        return format_html(linkstr)

    link_action_category.short_description = "Action Category"
    link_action_category.admin_order_field = "action_category__term"

    def get_inlines(self, request, obj):
        if request.user.is_superuser:
            self.inlines = (ActionTypeCommunityAdmin,)
        else:
            self.inlines = (
                ActionTypeCommunityAdmin,
                ActionTypeCommunityAdminReadonly,
            )
        return self.inlines

    def get_readonly_fields(self, request, obj):
        readonly = super().get_readonly_fields(request, obj)
        if obj and not request.user.is_superuser:
            readonly.append("action_category")
        return readonly
