# SPDX-FileCopyrightText: 2020 - 2022
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.urls import reverse
from django.utils.html import format_html

from app.admin.MeasuredQuantityCommunityAdmin import (
    MeasuredQuantityCommunityAdmin,
    MeasuredQuantityCommunityAdminReadonly,
)
from app.admin.QuantityUnitAdmin import QuantityUnitAdmin
from app.admin.TermCommunityAdmin import TermCommunityAdmin
from app.models.measured_quantity_unit import MeasuredQuantityUnit


class MeasuredQuantityAdmin(TermCommunityAdmin):

    # Tell Django to join foreign tables to save hundreds of db queries
    list_select_related = (
        "aggregation_type",
        "sampling_media",
        "sampling_media__compartment",
        *TermCommunityAdmin.list_select_related,
    )

    ordering = ("sampling_media__compartment__term", "sampling_media__term", "term")

    search_fields = (
        *TermCommunityAdmin.search_fields,
        "sampling_media__compartment__term",
        "sampling_media__term",
        "aggregation_type__term",
    )

    list_filter = (
        "sampling_media__compartment",
        "sampling_media",
        "aggregation_type",
        *TermCommunityAdmin.list_filter,
    )

    fields = (
        # 'compartment',
        "sampling_media",
        "aggregation_type",
        *TermCommunityAdmin.fields,
    )

    def get_list_display(self, request):
        return (
            "link_compartment",
            "link_sampling_media",
        ) + super().get_list_display(request)

    def link_compartment(self, obj):
        view_name = f"admin:{obj._meta.app_label}_compartment_change"
        link_url = reverse(view_name, args=[obj.sampling_media.compartment_id])
        linkstr = (
            f"<b><a href='{link_url}'>{obj.sampling_media.compartment}</a></b><br>"
        )
        return format_html(linkstr)

    link_compartment.short_description = "Compartment"
    link_compartment.admin_order_field = "sampling_media__compartment__term"

    def link_sampling_media(self, obj):
        view_name = f"admin:{obj._meta.app_label}_samplingmedium_change"
        link_url = reverse(view_name, args=[obj.sampling_media_id])
        linkstr = f"<b><a href='{link_url}'>{obj.sampling_media}</a></b><br>"
        return format_html(linkstr)

    link_sampling_media.short_description = "Sampling Medium"
    link_sampling_media.admin_order_field = "sampling_media__term"

    # create relation to communities using vocabulary as root and show tags
    def units(self, obj):
        linked_objs = MeasuredQuantityUnit.objects.filter(measured_quantity_id=obj.pk)
        view_name = (
            f"admin:{obj._meta.app_label}_{obj.__class__.__name__.lower()}_change"
        )
        linkstr = str()
        if len(linked_objs) > 1:
            point = "• "
        else:
            point = ""
        for sublink in linked_objs.iterator():
            link_url = reverse(view_name, args=[obj.id])
            linkstr = f"""{linkstr}
             <b><a href="{link_url}#units-tab">{point}
            {sublink.unit}</a></b><br>"""
        return format_html(linkstr)

    units.short_description = "Units"
    units.admin_order_field = "measured_quantity_units__unit"

    def get_inlines(self, request, obj):
        if request.user.is_superuser:
            self.inlines = (
                QuantityUnitAdmin,
                MeasuredQuantityCommunityAdmin,
            )
        else:
            self.inlines = (
                QuantityUnitAdmin,
                MeasuredQuantityCommunityAdmin,
                MeasuredQuantityCommunityAdminReadonly,
            )
        return self.inlines

    def get_readonly_fields(self, request, obj=None):
        readonly = super().get_readonly_fields(request, obj)
        if obj and not request.user.is_superuser:
            readonly.extend(["sampling_media", "aggregation_type"])
        return readonly
