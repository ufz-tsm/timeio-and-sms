# SPDX-FileCopyrightText: 2020 - 2022
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.contrib import admin

from app.models import MeasuredQuantityUnit


class QuantityUnitAdmin(admin.StackedInline):
    model = MeasuredQuantityUnit

    show_change_link = True

    # Tell Django to join foreign tables to save hundreds of db queries
    list_select_related = [
        "measured_quantity__sampling_media__compartment",
        "measured_quantity__sampling_media",
        "measured_quantity",
        "unit",
    ]

    ordering = [
        "measured_quantity__sampling_media__compartment__term",
        "measured_quantity__sampling_media__term",
        "measured_quantity__term",
        "unit__term",
    ]

    list_display = (
        "compartment",
        "sampling_media",
        "measured_quantity",
        "unit",
        "default_limit_min",
        "default_limit_max",
    )

    list_filter = [
        "measured_quantity__sampling_media__compartment",
        "measured_quantity__sampling_media",
    ]

    search_fields = [
        "measured_quantity__sampling_media__compartment__term",
        "measured_quantity__sampling_media__term",
        "measured_quantity__term",
        "unit__term",
    ]

    fields = [
        "unit",
        "default_limit_min",
        "default_limit_max",
    ]

    list_display_links = ["measured_quantity", "unit"]

    def compartment(self, obj):
        return obj.measured_quantity.sampling_media.compartment

    compartment.short_description = "Compartment"
    compartment.admin_order_field = (
        "measured_quantity__sampling_media__compartment__term"
    )

    def sampling_media(self, obj):
        return obj.measured_quantity.sampling_media

    sampling_media.short_description = "Sampling medium"
    sampling_media.admin_order_field = "measured_quantity__sampling_media__term"

    # define amount of add fields based on belonging communities
    def get_extra(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.extra = 1
        return self.extra

    # label inline tab with community name
    def get_formset(self, request, obj=None, **kwargs):
        self.verbose_name_plural = "Units"
        return super().get_formset(request, obj, **kwargs)

    def has_delete_permission(self, request, obj=None):
        if not request.user.is_superuser:
            return False
        else:
            return True

    def has_change_permission(self, request, obj=None):
        if not request.user.is_superuser:
            return False
        else:
            return True

    def has_add_permission(self, request, obj=None):
        return True
