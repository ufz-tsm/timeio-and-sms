# SPDX-FileCopyrightText: 2020 - 2022
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.contrib import admin
from django.db.models.functions import Sign

from app.admin.Admin import linkify


class TermAdmin(admin.ModelAdmin):
    list_display_links = ["term"]
    ordering = ["term"]
    list_filter = ["status", "global_provenance"]
    list_per_page = 15

    # Tell Django to join global provenance to save hundreds of db queries
    list_select_related = ["global_provenance", "successor"]

    fields = [
        "term",
        "definition",
        "provenance",
        "provenance_uri",
        "category",
        "note",
        "status",
        "global_provenance",
        "successor",
    ]
    # readonly_fields = ['term']

    search_fields = [
        "term",
        "definition",
        "provenance",
        "provenance_uri",
        "category",
        "note",
        "status",
        "global_provenance__name",
    ]

    def term_status(self, obj):
        return obj.status

    term_status.short_description = "Status"
    term_status.admin_order_field = "status"

    def is_latest(self, obj):
        return obj.successor is None

    is_latest.short_description = "Latest"
    is_latest.boolean = True
    # Use sign function to achieve something like bool
    is_latest.admin_order_field = Sign("successor")

    # successor.admin_order_field = "status"

    list_display = (
        "term",
        "is_latest",
        "definition",
        "provenance",
        "provenance_uri",
        "category",
        "note",
        "term_status",
        "global_provenance",
        linkify("successor"),
    )

    def get_readonly_fields(self, request, obj=None):
        # if obj:
        #     return ["term"]
        # else:
        return []
