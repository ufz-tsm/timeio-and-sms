# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from app.admin.TermCommunityAdmin import (
    TermCommunityAdminInlineEdit,
    TermCommunityAdminInlineReadonly,
)
from app.models import SamplingMediumCommunity


class SamplingMediumCommunityAdmin(TermCommunityAdminInlineEdit):
    model = SamplingMediumCommunity

    fields = (
        "compartment",
        *TermCommunityAdminInlineEdit.fields,
    )

    def get_formset(self, request, obj=None, **kwargs):
        formset = super().get_formset(request, obj, **kwargs)
        if obj is not None:
            formset.form.base_fields["compartment"].initial = obj.compartment
            formset.form.base_fields["compartment"].disabled = True
        return formset


class SamplingMediumCommunityAdminReadonly(TermCommunityAdminInlineReadonly):
    model = SamplingMediumCommunity

    fields = (
        "compartment",
        *TermCommunityAdminInlineReadonly.fields,
    )
