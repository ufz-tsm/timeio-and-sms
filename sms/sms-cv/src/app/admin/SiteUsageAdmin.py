# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Admin classes for the sites usages."""

from app.admin.SiteUsageCommunityAdmin import (
    SiteUsageCommunityAdmin,
    SiteUsageCommunityAdminReadonly,
)
from app.admin.TermCommunityAdmin import TermCommunityAdmin


class SiteUsageAdmin(TermCommunityAdmin):
    """Model admin class for site usages."""

    def get_inlines(self, request, obj):
        """Return the inline admin modules depending on user permissions."""
        if request.user.is_superuser:
            self.inlines = (SiteUsageCommunityAdmin,)
        else:
            self.inlines = (
                SiteUsageCommunityAdmin,
                SiteUsageCommunityAdminReadonly,
            )
        return self.inlines
