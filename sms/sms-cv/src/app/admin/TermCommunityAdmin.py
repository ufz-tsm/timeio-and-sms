# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.apps import apps

# from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.db.models.functions import Sign
from django.urls import reverse
from django.utils.html import format_html

from app.admin.Admin import get_community_name, linkify
from app.models import Community, ControlledVocabularyCommunity


class TermCommunityAdmin(admin.ModelAdmin):
    ordering = ["term"]
    list_per_page = 15

    # Tell Django to join global provenance to save hundreds of db queries
    list_select_related = (
        "global_provenance",
        "successor",
    )

    fields = [
        "term",
        "definition",
        "provenance",
        "provenance_uri",
        "category",
        "note",
        "status",
        "global_provenance",
        "successor",
        "requested_by_email",
    ]

    # readonly_fields = ["community_tag"]

    search_fields = [
        "term",
        "definition",
        "provenance",
        "provenance_uri",
        "category",
        "note",
        "status",
        "global_provenance__name",
    ]

    def get_list_filter(self, request):
        if request.user.is_superuser:
            return ["status", "global_provenance", "requested_by_email"]
        return ["status", "global_provenance"]

    def get_actions(self, request):
        def add_community_tag(modeladmin, request, queryset):
            # queryset.update(status='p')
            model_object = apps.get_model(
                app_label=queryset.model._meta.app_label,
                model_name=f"{queryset.model.__name__}Community",
            )
            all_fields = [f.name for f in model_object._meta.get_fields()]
            basic_fields = [
                f.name for f in ControlledVocabularyCommunity._meta.get_fields()
            ] + ["root", "id"]
            fields_diff = list(set(all_fields) - set(basic_fields))
            group = request.user.groups.values_list("name", flat=True).first()
            community = Community.objects.filter(term=group).first()
            for obj in queryset:
                if not model_object.objects.filter(
                    root_id=obj.pk, community_id=community.pk
                ).exists():
                    kwargs = {field: getattr(obj, field) for field in fields_diff}
                    model_object.objects.create(
                        root_id=obj.pk, community_id=community.pk, **kwargs
                    )

        def delete_community_tag(modeladmin, request, queryset):
            # queryset.update(status='p')
            model_object = apps.get_model(
                app_label=queryset.model._meta.app_label,
                model_name=f"{queryset.model.__name__}Community",
            )
            group = request.user.groups.values_list("name", flat=True).first()
            community = Community.objects.filter(term=group).first()
            for obj in queryset:
                if model_object.objects.filter(
                    root_id=obj.pk, community_id=community.pk
                ).exists():
                    model_object.objects.filter(
                        root_id=obj.pk, community_id=community.pk
                    ).delete()

        if not request.user.is_superuser:
            community = request.user.groups.values_list("name", flat=True).first()
            return {
                "delete_community_tag": (
                    delete_community_tag,
                    "delete_community_tag",
                    f"Delete for {community}",
                ),
                "add_community_tag": (
                    add_community_tag,
                    "add_community_tag",
                    f"Add for {community}",
                ),
            }
        else:
            return None

    def get_action_choices(self, request, **kwargs):
        choices = super(TermCommunityAdmin, self).get_action_choices(request)
        # choices is a list, just change it.
        # the first is the BLANK_CHOICE_DASH
        choices.pop(0)
        # do something to change the list order
        # the first one in list will be default option
        choices.reverse()
        return choices

    # get list view dynamically
    def get_list_display(self, request):

        if "MeasuredQuantity" in self.model.__name__:
            term_seq = (
                "root_term",
                "units",
            )
        else:
            term_seq = ("root_term",)

        def communities_tag(obj):
            return self._get_communities_tag(request, obj)

        communities_tag.short_description = format_html(
            f"Communities<br><img src='{settings.STATIC_URL}/admin/img/icon-yes.svg'>Synonym"
        )

        if not request.user.is_superuser:
            community = request.user.groups.values_list("name", flat=True).first()

            def community_term(obj):
                self.attribute = "term"
                return self._get_community_attribute(request, obj)

            community_term.short_description = f"{community} Term"

            def community_provenance(obj):
                self.attribute = "provenance"
                return self._get_community_attribute(request, obj)

            community_provenance.short_description = f"{community} Provenance"

            list_display = (
                *term_seq,
                community_term,
                community_provenance,
                communities_tag,
                "term_status",
                "is_latest",
                linkify("successor"),
                "global_provenance",
            )
        else:
            list_display = (
                *term_seq,
                communities_tag,
                "term_status",
                "is_latest",
                linkify("successor"),
                "global_provenance",
                "provenance",
                "provenance_uri",
                "requested_by_email",
            )
        return list_display

    def _get_community_attribute(self, request, obj):
        model_object = apps.get_model(
            app_label=obj._meta.app_label,
            model_name=f"{obj.__class__.__name__}Community",
        )
        linked_obj = model_object.objects.filter(
            root_id=obj.pk, community__term=self.community
        ).first()
        try:
            attribute = getattr(linked_obj, self.attribute)
        except AttributeError:
            return None
        if not attribute:
            return None
        if self.attribute == "term":
            tab_name = str(self.community.lower())
            view_name = (
                f"admin:{obj._meta.app_label}_{obj.__class__.__name__.lower()}_change"
            )
            link_url = reverse(view_name, args=[obj.id])
            linkstr = f"<b><a href='{link_url}#{tab_name}-tab'>{attribute}</a></b><br>"
            return format_html(linkstr)
        else:
            return attribute

    # create relation to communities using vocabulary as root and show tags
    def _get_communities_tag(self, request, obj):
        model_object = apps.get_model(
            app_label=obj._meta.app_label,
            model_name=f"{obj.__class__.__name__}Community",
        )
        linked_objs = model_object.objects.filter(root__pk=obj.pk)
        view_name = (
            f"admin:{obj._meta.app_label}_{obj.__class__.__name__.lower()}_change"
        )
        groups = list(self.request.user.groups.values_list("name", flat=True))
        linkstr = str()
        if len(linked_objs) > 1:
            point = "• "
        else:
            point = ""
        for sublink in linked_objs.iterator():
            if not self.request.user.is_superuser:
                if sublink.community.term in groups:
                    tab_name = (
                        get_community_name(self.request).replace(" ", "-").lower()
                    )
                else:
                    tab_name = "all-communities-read-only"
            else:
                tab_name = "all-communities-admin"
            if sublink.term is not None:
                icon = f'<img src="{settings.STATIC_URL}/admin/img/icon-yes.svg">'
            else:
                icon = str()
            link_url = reverse(view_name, args=[obj.id])
            linkstr = f"""{linkstr}
             <b><a href="{link_url}#{tab_name}-tab">{point}
            {sublink.community} {icon}</a></b><br>"""
        return format_html(linkstr)

    # provide field for original/general/root term
    def root_term(self, obj):
        view_name = (
            f"admin:{obj._meta.app_label}_{obj.__class__.__name__.lower()}_change"
        )
        link_url = reverse(view_name, args=[obj.id])
        linkstr = f"""<b><a href="{link_url}">{obj.term}</a></b><br>"""
        return format_html(linkstr)

    root_term.short_description = "General Term"
    root_term.admin_order_field = "term"

    # check successor field
    def is_latest(self, obj):
        return obj.successor is None

    is_latest.short_description = "Latest"
    is_latest.boolean = True
    # Use sign function to achieve something like bool
    is_latest.admin_order_field = Sign("successor")

    # provide field for status (accepted, pending, ...)
    def term_status(self, obj):
        return obj.status

    term_status.short_description = "Status"
    term_status.admin_order_field = "status"

    # define readonly fields based on permissions
    def get_readonly_fields(self, request, obj):
        if not request.user.is_superuser and obj is not None:
            return [
                "term",
                "definition",
                "provenance",
                "provenance_uri",
                "category",
                "note",
                "status",
                "global_provenance",
                "successor",
                "requested_by_email",
            ]
        elif not request.user.is_superuser and obj is None:
            return [
                "status",
                "successor",
                "requested_by_email",
            ]
        else:
            # if obj:
            #     return []
            # else:
            return [
                "requested_by_email",
            ]

    def has_delete_permission(self, request, obj=None):
        if not request.user.is_superuser:
            return False

    def get_queryset(self, request):
        qs = super(TermCommunityAdmin, self).get_queryset(request)
        self.request = request
        community = request.user.groups.values_list("name", flat=True).first()
        self.community = community
        return qs

    # specify list view title
    def changelist_view(self, request, extra_context=None):
        extra_context = {}
        if request.user.is_superuser:
            extra_context[
                "title"
            ] = f""" Select General Term or
            specific Community to change general or community entries (Synonyms).
            Add new {self.model._meta.verbose_name.capitalize()} if required.
            """
        return super().changelist_view(request, extra_context)

    # specify add/change view title
    def changeform_view(self, request, object_id=None, form_url="", extra_context=None):
        extra_context = {}
        groups = list(request.user.groups.values_list("name", flat=True))
        communities = " | ".join(str(gg) for gg in groups)
        if object_id is not None:
            model_instance = self.model.objects.filter(pk=object_id).first()
            if request.user.is_superuser:
                extra_context[
                    "title"
                ] = f"""Change
                 {self.model._meta.verbose_name.capitalize()}
                 {model_instance.term} | Add/change community tags and synonyms
                """
            else:
                if len(groups) > 1:
                    extra_context[
                        "title"
                    ] = f"""
                    {self.model._meta.verbose_name.capitalize()}
                     {model_instance.term} | Add/change community tags and synonyms for {communities}
                    """
                else:
                    extra_context[
                        "title"
                    ] = f"""
                    {self.model._meta.verbose_name.capitalize()}
                     {model_instance.term} | Add/change the community tag and synonym for {communities}
                    """

        else:
            if request.user.is_superuser:
                extra_context[
                    "title"
                ] = f"""Add
                 {self.model._meta.verbose_name.capitalize()} |
                 Add community tags and synonyms
                """
            else:
                if len(groups) > 1:
                    extra_context[
                        "title"
                    ] = f""" Add
                    {self.model._meta.verbose_name.capitalize()} |
                     Add community tags and synonyms for {communities}
                    """
                else:
                    extra_context[
                        "title"
                    ] = f""" Add
                    {self.model._meta.verbose_name.capitalize()} |
                     Add the community tag and synonyms for {communities}
                    """

        return super().changeform_view(request, object_id, form_url, extra_context)


class TermCommunityAdminInline(admin.StackedInline):
    ordering = ["-pk"]

    show_change_link = True

    list_display = (
        "term",
        "abbreviation",
        "definition",
        "provenance",
        "provenance_uri",
        "note",
    )

    fields = [
        "community",
        "root",
        "term",
        "abbreviation",
        "definition",
        "provenance",
        "provenance_uri",
        "note",
    ]


class TermCommunityAdminInlineEdit(TermCommunityAdminInline):

    # define amount of add fields based on belonging communities
    def get_max_num(self, request, obj=None, **kwargs):
        communities = list(request.user.groups.values_list("name", flat=True))
        if not request.user.is_superuser:
            self.max_num = len(communities)
        return self.max_num

    def get_extra(self, request, obj=None, **kwargs):
        if request.user.is_superuser:
            self.extra = 1
        return self.extra

    # label inline tab with community name
    def get_formset(self, request, obj=None, **kwargs):
        self.verbose_name_plural = get_community_name(request)
        return super().get_formset(request, obj, **kwargs)

    # filter admin list view after group -> community affiliation. Only if not superuser
    def get_queryset(self, request):
        # For Django < 1.6, override queryset instead of get_queryset
        groups = list(request.user.groups.values_list("name", flat=True))
        qs = super(TermCommunityAdminInlineEdit, self).get_queryset(request)
        if not request.user.is_superuser:
            return qs.filter(community__term__in=groups)
        else:
            return qs.all()

    # filter admin add/change view after group -> community affiliation. Only if not superuser
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        groups = [group for group in request.user.groups.all()]
        if db_field.name == "community" and not request.user.is_superuser:
            kwargs["queryset"] = Community.objects.filter(term__in=groups)
        return super(TermCommunityAdminInlineEdit, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )


class TermCommunityAdminInlineReadonly(TermCommunityAdminInline):
    # inline tab title
    verbose_name_plural = "All Communities (read-only)"

    # define permissions - make readonly
    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False
