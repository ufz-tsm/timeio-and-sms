# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Admin panels for the contact role model."""

from app.admin.ContactRoleCommunityAdmin import (
    ContactRoleCommunityAdmin,
    ContactRoleCommunityAdminReadonly,
)
from app.admin.TermCommunityAdmin import TermCommunityAdmin


class ContactRoleAdmin(TermCommunityAdmin):
    """Adjusted admin interface for the contact role model."""

    def get_inlines(self, request, obj):
        """Add the inlines for the community entries."""
        if request.user.is_superuser:
            self.inlines = (ContactRoleCommunityAdmin,)
        else:
            self.inlines = (
                ContactRoleCommunityAdmin,
                ContactRoleCommunityAdminReadonly,
            )
        return self.inlines
