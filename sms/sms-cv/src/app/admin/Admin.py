# SPDX-FileCopyrightText: 2020 - 2022
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.utils.html import format_html


# still required?
class Admin(admin.ModelAdmin):
    # save_as = True # Doesnt seems to work with WORMED (write once, read many) term field
    list_display_links = ["term"]


admin.site.disable_action("delete_selected")


# utility function to get object and properties from field name
def linkify(field_name):
    def _linkify(obj):
        if getattr(obj, field_name):
            content_type = ContentType.objects.get_for_model(obj)
            app_label = content_type.app_label
            linked_obj = getattr(obj, field_name)
            linked_content_type = ContentType.objects.get_for_model(linked_obj)
            model_name = linked_content_type.model
            view_name = f"admin:{app_label}_{model_name}_change"
            link_url = reverse(view_name, args=[linked_obj.pk])
            return format_html('<a href="{}">{}</a>', link_url, linked_obj)

    _linkify.short_description = field_name.replace("_", " ").capitalize()
    _linkify.admin_order_field = "successor__term"
    return _linkify


# utility function to get a curators community name string
def get_community_name(request):
    community = request.user.groups.values_list("name", flat=True).first()
    if not request.user.is_superuser:
        if community:
            community_name = community
        else:
            community_name = "Community missing"
    else:
        community_name = "All Communities (admin)"
    return community_name
