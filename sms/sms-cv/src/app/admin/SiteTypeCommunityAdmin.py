# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Admin model classes for site type community entries."""

from app.admin.TermCommunityAdmin import (
    TermCommunityAdminInlineEdit,
    TermCommunityAdminInlineReadonly,
)
from app.models import SiteTypeCommunity


class SiteTypeCommunityAdmin(TermCommunityAdminInlineEdit):
    """Edit community admin interface."""

    model = SiteTypeCommunity


class SiteTypeCommunityAdminReadonly(TermCommunityAdminInlineReadonly):
    """Readonly community admin interface."""

    model = SiteTypeCommunity
