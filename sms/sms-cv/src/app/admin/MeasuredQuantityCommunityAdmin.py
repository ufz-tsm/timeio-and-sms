# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from app.admin.TermCommunityAdmin import (
    TermCommunityAdminInlineEdit,
    TermCommunityAdminInlineReadonly,
)
from app.models import MeasuredQuantityCommunity


class MeasuredQuantityCommunityAdmin(TermCommunityAdminInlineEdit):
    model = MeasuredQuantityCommunity

    fields = (
        # 'compartment',
        "sampling_media",
        "aggregation_type",
        *TermCommunityAdminInlineEdit.fields,
    )

    def get_formset(self, request, obj=None, **kwargs):
        formset = super().get_formset(request, obj, **kwargs)
        if obj is not None:
            formset.form.base_fields["sampling_media"].initial = obj.sampling_media
            formset.form.base_fields["sampling_media"].disabled = True
            formset.form.base_fields["aggregation_type"].initial = obj.aggregation_type
            formset.form.base_fields["aggregation_type"].disabled = True
        return formset


class MeasuredQuantityCommunityAdminReadonly(TermCommunityAdminInlineReadonly):
    model = MeasuredQuantityCommunity

    fields = (
        # 'compartment',
        "sampling_media",
        "aggregation_type",
        *TermCommunityAdminInlineEdit.fields,
    )
