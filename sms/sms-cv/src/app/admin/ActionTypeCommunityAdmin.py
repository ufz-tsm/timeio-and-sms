# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from app.admin.TermCommunityAdmin import (
    TermCommunityAdminInlineEdit,
    TermCommunityAdminInlineReadonly,
)
from app.models import ActionTypeCommunity


class ActionTypeCommunityAdmin(TermCommunityAdminInlineEdit):
    model = ActionTypeCommunity

    fields = (
        "action_category",
        *TermCommunityAdminInlineEdit.fields,
    )

    def get_formset(self, request, obj=None, **kwargs):
        formset = super().get_formset(request, obj, **kwargs)
        if obj is not None:
            formset.form.base_fields["action_category"].initial = obj.action_category
            formset.form.base_fields["action_category"].disabled = True
        return formset


class ActionTypeCommunityAdminReadonly(TermCommunityAdminInlineReadonly):
    model = ActionTypeCommunity

    fields = (
        "action_category",
        *TermCommunityAdminInlineReadonly.fields,
    )
