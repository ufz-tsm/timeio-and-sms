# SPDX-FileCopyrightText: 2020 - 2022
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.urls import reverse
from django.utils.html import format_html

from app.admin.SamplingMediumCommunityAdmin import (
    SamplingMediumCommunityAdmin,
    SamplingMediumCommunityAdminReadonly,
)
from app.admin.TermCommunityAdmin import TermCommunityAdmin


class SamplingMediumAdmin(TermCommunityAdmin):
    # Tell Django to join foreign tables to save hundreds of db queries
    list_select_related = (
        "compartment",
        *TermCommunityAdmin.list_select_related,
    )

    ordering = (
        "compartment",
        "term",
    )

    list_filter = ("compartment", *TermCommunityAdmin.list_filter)

    fields = (
        "compartment",
        *TermCommunityAdmin.fields,
    )

    def get_list_display(self, request):
        return ("link_compartment",) + super().get_list_display(request)

    def link_compartment(self, obj):
        view_name = f"admin:{obj._meta.app_label}_compartment_change"
        link_url = reverse(view_name, args=[obj.compartment_id])
        linkstr = f"<b><a href='{link_url}'>{obj.compartment}</a></b><br>"
        return format_html(linkstr)

    link_compartment.short_description = "Compartment"
    link_compartment.admin_order_field = "compartment__term"

    def get_inlines(self, request, obj):
        if request.user.is_superuser:
            self.inlines = (SamplingMediumCommunityAdmin,)
        else:
            self.inlines = (
                SamplingMediumCommunityAdmin,
                SamplingMediumCommunityAdminReadonly,
            )
        return self.inlines

    def get_readonly_fields(self, request, obj):
        readonly = super().get_readonly_fields(request, obj)
        if obj and not request.user.is_superuser:
            readonly.append("compartment")
        return readonly
