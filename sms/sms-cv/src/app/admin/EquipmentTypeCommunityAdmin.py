# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from app.admin.TermCommunityAdmin import (
    TermCommunityAdminInlineEdit,
    TermCommunityAdminInlineReadonly,
)
from app.models import EquipmentTypeCommunity


class EquipmentTypeCommunityAdmin(TermCommunityAdminInlineEdit):
    model = EquipmentTypeCommunity


class EquipmentTypeCommunityAdminReadonly(TermCommunityAdminInlineReadonly):
    model = EquipmentTypeCommunity
