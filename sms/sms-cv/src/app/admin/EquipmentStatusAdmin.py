# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from app.admin.EquipmentStatusCommunityAdmin import (
    EquipmentStatusCommunityAdmin,
    EquipmentStatusCommunityAdminReadonly,
)
from app.admin.TermCommunityAdmin import TermCommunityAdmin


class EquipmentStatusAdmin(TermCommunityAdmin):
    def get_inlines(self, request, obj):
        if request.user.is_superuser:
            self.inlines = (EquipmentStatusCommunityAdmin,)
        else:
            self.inlines = (
                EquipmentStatusCommunityAdmin,
                EquipmentStatusCommunityAdminReadonly,
            )
        return self.inlines
