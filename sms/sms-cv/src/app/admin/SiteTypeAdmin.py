# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Admin classes for the site types."""

from django.urls import reverse
from django.utils.html import format_html

from app.admin.SiteTypeCommunityAdmin import (
    SiteTypeCommunityAdmin,
    SiteTypeCommunityAdminReadonly,
)
from app.admin.TermCommunityAdmin import TermCommunityAdmin


class SiteTypeAdmin(TermCommunityAdmin):
    """Model admin class for site types."""

    # Tell Django to join foreign tables to save hundreds of db queries
    list_select_related = (
        "site_usage",
        *TermCommunityAdmin.list_select_related,
    )

    ordering = (
        "site_usage",
        "term",
    )

    list_filter = ("site_usage", *TermCommunityAdmin.list_filter)
    fields = (
        "site_usage",
        *TermCommunityAdmin.fields,
    )

    def get_list_display(self, request):
        """Return the lsit of associated fields to display."""
        return ("link_site_usage",) + super().get_list_display(request)

    def link_site_usage(self, obj):
        """Create a link to the site usage."""
        view_name = f"admin:{obj._meta.app_label}_siteusage_change"
        link_url = reverse(view_name, args=[obj.site_usage_id])
        linkstr = f"<b><a href='{link_url}'>{obj.site_usage}</a></b><br>"
        return format_html(linkstr)

    link_site_usage.short_description = "Site Usage"
    link_site_usage.admin_order_field = "site_usage__term"

    def get_inlines(self, request, obj):
        """Return the inline admin modules depending on user permissions."""
        if request.user.is_superuser:
            self.inlines = (SiteTypeCommunityAdmin,)
        else:
            self.inlines = (
                SiteTypeCommunityAdmin,
                SiteTypeCommunityAdminReadonly,
            )
        return self.inlines

    def get_readonly_fields(self, request, obj):
        """Return the read only fields."""
        readonly = super().get_readonly_fields(request, obj)
        if obj and not request.user.is_superuser:
            readonly.append("site_usage")
        return readonly
