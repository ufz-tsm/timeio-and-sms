# SPDX-FileCopyrightText: 2022 - 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Signals for the cv models."""

from django.core.cache import cache
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from . import models

models_to_invalidate_cache_on_changes = [
    models.ActionCategory,
    models.ActionCategoryCommunity,
    models.ActionType,
    models.ActionTypeCommunity,
    models.AggregationType,
    models.AggregationTypeCommunity,
    models.Community,
    models.Compartment,
    models.CompartmentCommunity,
    models.ContactRole,
    models.ContactRoleCommunity,
    models.Country,
    models.EquipmentStatus,
    models.EquipmentStatusCommunity,
    models.EquipmentType,
    models.EquipmentTypeCommunity,
    models.GlobalProvenance,
    models.Manufacturer,
    models.ManufacturerCommunity,
    models.MeasuredQuantity,
    models.MeasuredQuantityCommunity,
    models.MeasuredQuantityUnit,
    models.PlatformType,
    models.PlatformTypeCommunity,
    models.SamplingMedium,
    models.SamplingMediumCommunity,
    models.SiteType,
    models.SiteTypeCommunity,
    models.SiteUsage,
    models.SiteUsageCommunity,
    models.SoftwareType,
    models.SoftwareTypeCommunity,
    models.Unit,
    models.UnitCommunity,
]


def clear_cache(*args, **kwargs):
    """Clear the cache of the app."""
    cache.clear()


for model in models_to_invalidate_cache_on_changes:

    # using the @receiver decorator explicitly
    receiver(post_save, sender=model)(clear_cache)
    receiver(post_delete, sender=model)(clear_cache)
