# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import (
    HyperlinkedRelatedField,
    ResourceRelatedField,
)

from app.models import ActionCategory, ActionType, GlobalProvenance
from app.serializers.Base_serializer import BaseSerializer


class ActionCategorySerializer(BaseSerializer):
    class Meta(BaseSerializer.Meta):
        model = ActionCategory

    global_provenance = ResourceRelatedField(
        model=GlobalProvenance,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=GlobalProvenance.objects.all(),
        self_link_view_name="action_category-relationships",
        related_link_view_name="action_category-related",
    )
    action_types = ResourceRelatedField(
        model=ActionType,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=ActionType.objects.all(),
        self_link_view_name="action_category-relationships",
        related_link_view_name="action_category-related",
    )
    successor = HyperlinkedRelatedField(
        many=False,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="action_category-relationships",
        related_link_view_name="action_category-related",
    )
    included_serializers = {
        "global_provenance": "app.serializers.GlobalProvenanceSerializer",
        "action_types": "app.serializers.ActionTypeSerializer",
        "successor": "app.serializers.ActionCategorySerializer",
    }
