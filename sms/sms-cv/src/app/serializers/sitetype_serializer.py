# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Serializer classes for the site types."""

from rest_framework_json_api.relations import (
    HyperlinkedRelatedField,
    ResourceRelatedField,
)

from app.models import GlobalProvenance, SiteType, SiteUsage
from app.serializers.Base_serializer import BaseSerializer


class SiteTypeSerializer(BaseSerializer):
    """Serializer for the site type model."""

    class Meta(BaseSerializer.Meta):
        """Meta class forthe site type serializer."""

        model = SiteType

    global_provenance = ResourceRelatedField(
        model=GlobalProvenance,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=GlobalProvenance.objects.all(),
        self_link_view_name="site_type-relationships",
        related_link_view_name="site_type-related",
    )
    successor = HyperlinkedRelatedField(
        many=False,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="site_type-relationships",
        related_link_view_name="site_type-related",
    )
    site_usage = ResourceRelatedField(
        model=SiteUsage,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=SiteUsage.objects.all(),
        self_link_view_name="site_type-relationships",
        related_link_view_name="site_type-related",
    )
    included_serializers = {
        "global_provenance": "app.serializers.GlobalProvenanceSerializer",
        "successor": "app.serializers.SiteTypeSerializer",
        "site_usage": "app.serializers.SiteUsageSerializer",
    }
