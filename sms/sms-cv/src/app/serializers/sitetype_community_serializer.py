# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Serializer classes for site type community entries."""

from rest_framework_json_api.relations import ResourceRelatedField

from app.models import Community, SiteType, SiteTypeCommunity
from app.serializers.Base_serializer import CommunityBaseSerializer


class SiteTypeCommunitySerializer(CommunityBaseSerializer):
    """Serializer for the site type community model."""

    class Meta(CommunityBaseSerializer.Meta):
        """Meta class for the site type community serializer."""

        model = SiteTypeCommunity

    root = ResourceRelatedField(
        model=SiteType,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=SiteType.objects.all(),
        self_link_view_name="site_type_community-relationships",
        related_link_view_name="site_type_community-related",
    )
    community = ResourceRelatedField(
        model=Community,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=Community.objects.all(),
        self_link_view_name="site_type_community-relationships",
        related_link_view_name="site_type_community-related",
    )
    included_serializers = {
        "root": "app.serializers.SiteTypeSerializer",
        "community": "app.serializers.CommunitySerializer",
    }
