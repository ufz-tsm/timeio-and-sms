# SPDX-FileCopyrightText: 2020
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import (
    HyperlinkedRelatedField,
    ResourceRelatedField,
)

from app.models import AggregationType, GlobalProvenance, MeasuredQuantity
from app.serializers.Base_serializer import BaseSerializer


class AggregationTypeSerializer(BaseSerializer):
    class Meta(BaseSerializer.Meta):
        model = AggregationType

    measured_quantities = ResourceRelatedField(
        model=MeasuredQuantity,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=MeasuredQuantity.objects.all(),
        self_link_view_name="aggregation_type-relationships",
        related_link_view_name="aggregation_type-related",
    )

    global_provenance = ResourceRelatedField(
        model=GlobalProvenance,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=GlobalProvenance.objects.all(),
        self_link_view_name="aggregation_type-relationships",
        related_link_view_name="aggregation_type-related",
    )
    successor = HyperlinkedRelatedField(
        many=False,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="aggregation_type-relationships",
        related_link_view_name="aggregation_type-related",
    )
    included_serializers = {
        "global_provenance": "app.serializers.GlobalProvenanceSerializer",
        "measured_quantity": "app.serializers.MeasuredQuantitySerializer",
        "successor": "app.serializers.AggregationTypeSerializer",
    }
