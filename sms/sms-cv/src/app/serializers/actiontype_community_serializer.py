# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import ResourceRelatedField

from app.models import ActionCategory, ActionType, ActionTypeCommunity, Community
from app.serializers.Base_serializer import CommunityBaseSerializer


class ActionTypeCommunitySerializer(CommunityBaseSerializer):
    class Meta(CommunityBaseSerializer.Meta):
        model = ActionTypeCommunity

    root = ResourceRelatedField(
        model=ActionType,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=ActionType.objects.all(),
        self_link_view_name="action_type_community-relationships",
        related_link_view_name="action_type_community-related",
    )
    action_category = ResourceRelatedField(
        model=ActionCategory,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=ActionCategory.objects.all(),
        self_link_view_name="action_type_community-relationships",
        related_link_view_name="action_type_community-related",
    )
    community = ResourceRelatedField(
        model=Community,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=Community.objects.all(),
        self_link_view_name="action_type_community-relationships",
        related_link_view_name="action_type_community-related",
    )
    included_serializers = {
        "root": "app.serializers.ActionTypeSerializer",
        "action_category": "app.serializers.ActionCategorySerializer",
        "community": "app.serializers.CommunitySerializer",
    }
