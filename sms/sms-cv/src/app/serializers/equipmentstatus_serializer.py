# SPDX-FileCopyrightText: 2020
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import (
    HyperlinkedRelatedField,
    ResourceRelatedField,
)

from app.models import EquipmentStatus, GlobalProvenance
from app.serializers.Base_serializer import BaseSerializer


class EquipmentStatusSerializer(BaseSerializer):
    class Meta(BaseSerializer.Meta):
        model = EquipmentStatus

    global_provenance = ResourceRelatedField(
        model=GlobalProvenance,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=GlobalProvenance.objects.all(),
        self_link_view_name="equipment_status-relationships",
        related_link_view_name="equipment_status-related",
    )
    successor = HyperlinkedRelatedField(
        many=False,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="equipment_status-relationships",
        related_link_view_name="equipment_status-related",
    )

    included_serializers = {
        "global_provenance": "app.serializers.GlobalProvenanceSerializer",
        "successor": "app.serializers.EquipmentStatusSerializer",
    }
