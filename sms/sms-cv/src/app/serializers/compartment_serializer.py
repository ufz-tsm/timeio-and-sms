# SPDX-FileCopyrightText: 2020 - 2021
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import (
    HyperlinkedRelatedField,
    ResourceRelatedField,
)

from app.models import Compartment, GlobalProvenance, SamplingMedium
from app.serializers.Base_serializer import BaseSerializer


class CompartmentSerializer(BaseSerializer):
    class Meta(BaseSerializer.Meta):
        model = Compartment

    global_provenance = ResourceRelatedField(
        model=GlobalProvenance,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=GlobalProvenance.objects.all(),
        self_link_view_name="compartment-relationships",
        related_link_view_name="compartment-related",
    )
    sampling_media = ResourceRelatedField(
        model=SamplingMedium,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=SamplingMedium.objects.all(),
        self_link_view_name="compartment-relationships",
        related_link_view_name="compartment-related",
    )
    successor = HyperlinkedRelatedField(
        many=False,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="compartment-relationships",
        related_link_view_name="compartment-related",
    )
    included_serializers = {
        "global_provenance": "app.serializers.GlobalProvenanceSerializer",
        "sampling_media": "app.serializers.SamplingMediumSerializer",
        "successor": "app.serializers.CompartmentSerializer",
    }
