# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Serializer for the contact role community model."""

from rest_framework_json_api.relations import ResourceRelatedField

from app.models import Community, ContactRole, ContactRoleCommunity
from app.serializers.Base_serializer import CommunityBaseSerializer


class ContactRoleCommunitySerializer(CommunityBaseSerializer):
    """Serializer class for the contact role community model."""

    class Meta(CommunityBaseSerializer.Meta):
        """Meta class for the contact role community serializer."""

        model = ContactRoleCommunity

    root = ResourceRelatedField(
        model=ContactRole,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=ContactRole.objects.all(),
        self_link_view_name="contact_roles_community-relationships",
        related_link_view_name="contact_roles_community-related",
    )
    community = ResourceRelatedField(
        model=Community,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=Community.objects.all(),
        self_link_view_name="contact_roles_community-relationships",
        related_link_view_name="contact_roles_community-related",
    )
    included_serializers = {
        "community": "app.serializers.CommunitySerializer",
        "root": "app.serializers.ContactRoleSerializer",
    }
