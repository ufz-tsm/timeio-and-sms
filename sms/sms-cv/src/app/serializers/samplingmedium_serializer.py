# SPDX-FileCopyrightText: 2020 - 2021
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import (
    HyperlinkedRelatedField,
    ResourceRelatedField,
)

from app.models import Compartment, GlobalProvenance, MeasuredQuantity, SamplingMedium
from app.serializers.Base_serializer import BaseSerializer


class SamplingMediumSerializer(BaseSerializer):
    class Meta(BaseSerializer.Meta):
        model = SamplingMedium

    measured_quantities = ResourceRelatedField(
        model=MeasuredQuantity,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=MeasuredQuantity.objects.all(),
        self_link_view_name="sampling_medium-relationships",
        related_link_view_name="sampling_medium-related",
    )
    global_provenance = ResourceRelatedField(
        model=GlobalProvenance,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=GlobalProvenance.objects.all(),
        self_link_view_name="sampling_medium-relationships",
        related_link_view_name="sampling_medium-related",
    )
    compartment = ResourceRelatedField(
        model=Compartment,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=Compartment.objects.all(),
        self_link_view_name="sampling_medium-relationships",
        related_link_view_name="sampling_medium-related",
    )
    successor = HyperlinkedRelatedField(
        many=False,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="sampling_medium-relationships",
        related_link_view_name="sampling_medium-related",
    )
    included_serializers = {
        "global_provenance": "app.serializers.GlobalProvenanceSerializer",
        "measured_quantities": "app.serializers.MeasuredQuantitySerializer",
        "compartment": "app.serializers.CompartmentSerializer",
        "successor": "app.serializers.SamplingMediumSerializer",
    }
