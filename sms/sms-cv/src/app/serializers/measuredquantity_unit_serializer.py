# SPDX-FileCopyrightText: 2020
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import ResourceRelatedField

from app.models import MeasuredQuantity, MeasuredQuantityUnit, Unit
from app.serializers.Base_serializer import BaseSerializer


class MeasuredQuantityUnitSerializer(BaseSerializer):
    class Meta(BaseSerializer.Meta):
        model = MeasuredQuantityUnit

    unit = ResourceRelatedField(
        model=Unit,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=Unit.objects.all(),
        self_link_view_name="measured_quantity_unit-relationships",
        related_link_view_name="measured_quantity_unit-related",
    )
    measured_quantity = ResourceRelatedField(
        model=MeasuredQuantity,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=MeasuredQuantity.objects.all(),
        self_link_view_name="measured_quantity_unit-relationships",
        related_link_view_name="measured_quantity_unit-related",
    )

    included_serializers = {
        "measured_quantity": "app.serializers.MeasuredQuantitySerializer",
        "unit": "app.serializers.UnitSerializer",
    }
