# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Serializer classes for site usage community entries."""

from rest_framework_json_api.relations import ResourceRelatedField

from app.models import Community, SiteUsage, SiteUsageCommunity
from app.serializers.Base_serializer import CommunityBaseSerializer


class SiteUsageCommunitySerializer(CommunityBaseSerializer):
    """Serializer for the site usage community model."""

    class Meta(CommunityBaseSerializer.Meta):
        """Meta class for the site usage community serializer."""

        model = SiteUsageCommunity

    root = ResourceRelatedField(
        model=SiteUsage,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=SiteUsage.objects.all(),
        self_link_view_name="site_usage_community-relationships",
        related_link_view_name="site_usage_community-related",
    )
    community = ResourceRelatedField(
        model=Community,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=Community.objects.all(),
        self_link_view_name="site_usage_community-relationships",
        related_link_view_name="site_usage_community-related",
    )
    included_serializers = {
        "root": "app.serializers.SiteUsageSerializer",
        "community": "app.serializers.CommunitySerializer",
    }
