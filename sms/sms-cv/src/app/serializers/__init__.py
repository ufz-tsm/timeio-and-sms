# SPDX-FileCopyrightText: 2020 - 2023
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

# community serializers
from .actioncategory_community_serializer import ActionCategoryCommunitySerializer
from .actioncategory_serializer import ActionCategorySerializer
from .actiontype_community_serializer import ActionTypeCommunitySerializer
from .actiontype_serializer import ActionTypeSerializer
from .aggregationtype_community_serializer import AggregationTypeCommunitySerializer
from .aggregationtype_serializer import AggregationTypeSerializer
from .community_serializer import CommunitySerializer
from .compartment_community_serializer import CompartmentCommunitySerializer
from .compartment_serializer import CompartmentSerializer
from .contact_role_community_serializer import ContactRoleCommunitySerializer
from .contact_role_serializer import ContactRoleSerializer
from .equipmentstatus_community_serializer import EquipmentStatusCommunitySerializer
from .equipmentstatus_serializer import EquipmentStatusSerializer
from .equipmenttype_community_serializer import EquipmentTypeCommunitySerializer
from .equipmenttype_serializer import EquipmentTypeSerializer
from .globalprovenance_serializer import GlobalProvenanceSerializer
from .license_serializer import LicenseSerializer
from .manufacturer_community_serializer import ManufacturerCommunitySerializer
from .manufacturer_serializer import ManufacturerSerializer
from .measuredquantity_community_serializer import MeasuredQuantityCommunitySerializer
from .measuredquantity_serializer import MeasuredQuantitySerializer
from .measuredquantity_unit_serializer import MeasuredQuantityUnitSerializer
from .platformtype_community_serializer import PlatformTypeCommunitySerializer
from .platformtype_serializer import PlatformTypeSerializer
from .samplingmedium_community_serializer import SamplingMediumCommunitySerializer
from .samplingmedium_serializer import SamplingMediumSerializer
from .sitetype_community_serializer import SiteTypeCommunitySerializer
from .sitetype_serializer import SiteTypeSerializer
from .siteusage_community_serializer import SiteUsageCommunitySerializer
from .siteusage_serializer import SiteUsageSerializer
from .softwaretype_community_serializer import SoftwareTypeCommunitySerializer
from .softwaretype_serializer import SoftwareTypeSerializer
from .unit_community_serializer import UnitCommunitySerializer
from .unit_serializer import UnitSerializer
