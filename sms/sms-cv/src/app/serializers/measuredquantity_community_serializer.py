# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import ResourceRelatedField

from app.models import (
    AggregationType,
    Community,
    MeasuredQuantity,
    MeasuredQuantityCommunity,
    SamplingMedium,
)
from app.serializers.Base_serializer import CommunityBaseSerializer


class MeasuredQuantityCommunitySerializer(CommunityBaseSerializer):
    class Meta(CommunityBaseSerializer.Meta):
        model = MeasuredQuantityCommunity

    sampling_media = ResourceRelatedField(
        model=SamplingMedium,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=SamplingMedium.objects.all(),
        self_link_view_name="measured_quantity_community-relationships",
        related_link_view_name="measured_quantity_community-related",
    )
    aggregation_type = ResourceRelatedField(
        model=AggregationType,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=AggregationType.objects.all(),
        self_link_view_name="measured_quantity_community-relationships",
        related_link_view_name="measured_quantity_community-related",
    )
    # measured_quantity_units = HyperlinkedRelatedField(
    #     many=True,
    #     read_only=True,
    #     allow_null=True,
    #     required=False,
    #     self_link_view_name="measured_quantity_community-relationships",
    #     related_link_view_name="measured_quantity_community-related",
    # )
    root = ResourceRelatedField(
        model=MeasuredQuantity,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=MeasuredQuantity.objects.all(),
        self_link_view_name="measured_quantity_community-relationships",
        related_link_view_name="measured_quantity_community-related",
    )
    community = ResourceRelatedField(
        model=Community,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=Community.objects.all(),
        self_link_view_name="measured_quantity_community-relationships",
        related_link_view_name="measured_quantity_community-related",
    )
    included_serializers = {
        "sampling_media": "app.serializers.SamplingMediumSerializer",
        "aggregation_type": "app.serializers.AggregationTypeSerializer",
        # "measured_quantity_units": "app.serializers.MeasuredQuantityUnitSerializer",
        "roott": "app.serializers.MeasuredQuantitySerializer",
        "community": "app.serializers.CommunitySerializer",
    }
