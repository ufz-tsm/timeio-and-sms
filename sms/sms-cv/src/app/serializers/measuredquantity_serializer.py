# SPDX-FileCopyrightText: 2020 - 2021
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import (
    HyperlinkedRelatedField,
    ResourceRelatedField,
)

from app.models import (
    AggregationType,
    GlobalProvenance,
    MeasuredQuantity,
    SamplingMedium,
)
from app.serializers.Base_serializer import BaseSerializer


class MeasuredQuantitySerializer(BaseSerializer):
    class Meta(BaseSerializer.Meta):
        model = MeasuredQuantity

    sampling_media = ResourceRelatedField(
        model=SamplingMedium,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=SamplingMedium.objects.all(),
        self_link_view_name="measured_quantity-relationships",
        related_link_view_name="measured_quantity-related",
    )
    aggregation_type = ResourceRelatedField(
        model=AggregationType,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=AggregationType.objects.all(),
        self_link_view_name="measured_quantity-relationships",
        related_link_view_name="measured_quantity-related",
    )
    global_provenance = ResourceRelatedField(
        model=GlobalProvenance,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=GlobalProvenance.objects.all(),
        self_link_view_name="measured_quantity-relationships",
        related_link_view_name="measured_quantity-related",
    )
    measured_quantity_units = HyperlinkedRelatedField(
        many=True,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="measured_quantity-relationships",
        related_link_view_name="measured_quantity-related",
    )
    successor = HyperlinkedRelatedField(
        many=False,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="measured_quantity-relationships",
        related_link_view_name="measured_quantity-related",
    )

    included_serializers = {
        "global_provenance": "app.serializers.GlobalProvenanceSerializer",
        "sampling_media": "app.serializers.SamplingMediumSerializer",
        "aggregation_type": "app.serializers.AggregationTypeSerializer",
        "measured_quantity_unit": "app.serializers.MeasuredQuantityUnitSerializer",
        "successor": "app.serializers.MeasuredQuantitySerializer",
    }
