# SPDX-FileCopyrightText: 2020 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.apps import apps
from rest_framework_json_api.serializers import HyperlinkedModelSerializer


class BaseSerializer(HyperlinkedModelSerializer):
    """
    Common serializer class for all model serializers.
    """

    class Meta:
        """
        In order for this Meta inner class to be inherited by the various serializers,
        one must explicitly inherit it as in this example::

            class MySerializer(BaseSerializer):
                class Meta(BaseSerializer.Meta):
                    model = MyModel
        """

        #: serialize all model fields unless otherwise overridden

        fields = "__all__"
        #: mark these fields as read-only
        read_only_fields = ["status", "requested_by_email"]


class CommunityBaseSerializer(HyperlinkedModelSerializer):
    """
    If no synonym is provided (<Model>Community.term == None), the serializers
    attributes are overwritten by its origin entries (<Model>.term).
    """

    class Meta:
        fields = "__all__"

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if not data["term"]:
            model_name = self.Meta.model.__name__.replace("Community", "")
            model_object = apps.get_model(app_label="app", model_name=model_name)
            model_instance = model_object.objects.filter(id=data["root"]["id"]).first()
            data["term"] = model_instance.term
            data["definition"] = model_instance.definition
            data["provenance"] = model_instance.provenance
            data["provenance_uri"] = model_instance.provenance_uri
            data["note"] = model_instance.note

        return data
