# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import ResourceRelatedField

from app.models import Community, EquipmentType, EquipmentTypeCommunity
from app.serializers.Base_serializer import CommunityBaseSerializer


class EquipmentTypeCommunitySerializer(CommunityBaseSerializer):
    class Meta(CommunityBaseSerializer.Meta):
        model = EquipmentTypeCommunity

    root = ResourceRelatedField(
        model=EquipmentType,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=EquipmentType.objects.all(),
        self_link_view_name="equipment_type_community-relationships",
        related_link_view_name="equipment_type_community-related",
    )
    community = ResourceRelatedField(
        model=Community,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=Community.objects.all(),
        self_link_view_name="equipment_type_community-relationships",
        related_link_view_name="equipment_type_community-related",
    )
    included_serializers = {
        "root": "app.serializers.EquipmentTypeSerializer",
        "community": "app.serializers.CommunitySerializer",
    }
