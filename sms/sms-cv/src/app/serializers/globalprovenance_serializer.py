# SPDX-FileCopyrightText: 2020 - 2022
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import ResourceRelatedField

from app.models import (
    ActionCategory,
    ActionType,
    AggregationType,
    Community,
    Compartment,
    EquipmentStatus,
    EquipmentType,
    GlobalProvenance,
    Manufacturer,
    MeasuredQuantity,
    PlatformType,
    SamplingMedium,
    Unit,
)
from app.serializers.Base_serializer import BaseSerializer


class GlobalProvenanceSerializer(BaseSerializer):
    class Meta(BaseSerializer.Meta):
        model = GlobalProvenance

    action_categories = ResourceRelatedField(
        source="actioncategorys",
        model=ActionCategory,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=ActionCategory.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    action_types = ResourceRelatedField(
        source="actiontypes",
        model=ActionType,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=ActionType.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    aggregation_types = ResourceRelatedField(
        source="aggregationtypes",
        model=AggregationType,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=AggregationType.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    compartments = ResourceRelatedField(
        model=Compartment,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=Compartment.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    equipment_status = ResourceRelatedField(
        source="equipmentstatuss",
        model=EquipmentStatus,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=EquipmentStatus.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    equipment_types = ResourceRelatedField(
        source="equipmenttypes",
        model=EquipmentType,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=EquipmentType.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    manufacturers = ResourceRelatedField(
        model=Manufacturer,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=Manufacturer.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    communities = ResourceRelatedField(
        source="communitys",
        model=Community,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=Community.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    measured_quantities = ResourceRelatedField(
        source="measuredquantitys",
        model=MeasuredQuantity,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=MeasuredQuantity.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    platform_types = ResourceRelatedField(
        source="platformtypes",
        model=PlatformType,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=PlatformType.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    sampling_media = ResourceRelatedField(
        source="samplingmediums",
        model=SamplingMedium,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=SamplingMedium.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )
    units = ResourceRelatedField(
        model=Unit,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=Unit.objects.all(),
        self_link_view_name="global_provenance-relationships",
        related_link_view_name="global_provenance-related",
    )

    included_serializers = {
        "action_categories": "app.serializers.ActionCategorySerializer",
        "action_types": "app.serializers.ActionTypeSerializer",
        "aggregation_types": "app.serializers.AggregationTypeSerializer",
        "compartments": "app.serializers.CompartmentSerializer",
        "equipment_status": "app.serializers.EquipmentStatusSerializer",
        "equipment_types": "app.serializers.EquipmentTypeSerializer",
        "manufacturers": "app.serializers.ManufacturerSerializer",
        "communities": "app.serializers.CommunitySerializer",
        "measured_quantities": "app.serializers.MeasuredQuantitySerializer",
        "platform_types": "app.serializers.PlatformTypeSerializer",
        "sampling_medium": "app.serializers.SamplingMediumSerializer",
        "units": "app.serializers.UnitSerializer",
    }
