# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Serializer classes for the licenses."""

from rest_framework_json_api.relations import (
    HyperlinkedRelatedField,
    ResourceRelatedField,
)

from app.models import GlobalProvenance, License
from app.serializers.Base_serializer import BaseSerializer


class LicenseSerializer(BaseSerializer):
    """Serializer for the license model."""

    class Meta(BaseSerializer.Meta):
        """Meta class for the license serializer."""

        model = License

    global_provenance = ResourceRelatedField(
        model=GlobalProvenance,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=GlobalProvenance.objects.all(),
        self_link_view_name="license-relationships",
        related_link_view_name="license-related",
    )
    successor = HyperlinkedRelatedField(
        many=False,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="license-relationships",
        related_link_view_name="license-related",
    )
    included_serializers = {
        "global_provenance": "app.serializers.GlobalProvenanceSerializer",
        "successor": "app.serializers.LicenseSerializer",
    }
