# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import ResourceRelatedField

from app.models import Community, Compartment, SamplingMedium, SamplingMediumCommunity
from app.serializers.Base_serializer import CommunityBaseSerializer


class SamplingMediumCommunitySerializer(CommunityBaseSerializer):
    class Meta(CommunityBaseSerializer.Meta):
        model = SamplingMediumCommunity

    # measured_quantities = ResourceRelatedField(
    #     model=MeasuredQuantity,
    #     many=True,
    #     read_only=False,
    #     allow_null=True,
    #     required=False,
    #     queryset=MeasuredQuantity.objects.all(),
    #     self_link_view_name="sampling_medium_community-relationships",
    #     related_link_view_name="sampling_medium_community-related",
    # )
    compartment = ResourceRelatedField(
        model=Compartment,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=Compartment.objects.all(),
        self_link_view_name="sampling_medium_community-relationships",
        related_link_view_name="sampling_medium_community-related",
    )
    root = ResourceRelatedField(
        model=SamplingMedium,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=SamplingMedium.objects.all(),
        self_link_view_name="sampling_medium_community-relationships",
        related_link_view_name="sampling_medium_community-related",
    )
    community = ResourceRelatedField(
        model=Community,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=Community.objects.all(),
        self_link_view_name="sampling_medium_community-relationships",
        related_link_view_name="sampling_medium_community-related",
    )
    included_serializers = {
        # "measured_quantities": "app.serializers.MeasuredQuantitySerializer",
        "compartment": "app.serializers.CompartmentSerializer",
        "root": "app.serializers.SamplingMediumSerializer",
        "community": "app.serializers.CommunitySerializer",
    }
