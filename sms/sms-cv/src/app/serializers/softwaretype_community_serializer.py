# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.relations import ResourceRelatedField

from app.models import Community, SoftwareType, SoftwareTypeCommunity
from app.serializers.Base_serializer import CommunityBaseSerializer


class SoftwareTypeCommunitySerializer(CommunityBaseSerializer):
    class Meta(CommunityBaseSerializer.Meta):
        model = SoftwareTypeCommunity

    root = ResourceRelatedField(
        model=SoftwareType,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=SoftwareType.objects.all(),
        self_link_view_name="software_type_community-relationships",
        related_link_view_name="software_type_community-related",
    )
    community = ResourceRelatedField(
        model=Community,
        many=False,
        read_only=False,
        allow_null=False,
        required=True,
        queryset=Community.objects.all(),
        self_link_view_name="software_type_community-relationships",
        related_link_view_name="software_type_community-related",
    )
    included_serializers = {
        "root": "app.serializers.SoftwareTypeSerializer",
        "community": "app.serializers.CommunitySerializer",
    }
