# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Serializer for the contact role model."""

from rest_framework_json_api.relations import (
    HyperlinkedRelatedField,
    ResourceRelatedField,
)

from app.models import ContactRole, GlobalProvenance
from app.serializers.Base_serializer import BaseSerializer


class ContactRoleSerializer(BaseSerializer):
    """Serializer class for the contact role model."""

    class Meta(BaseSerializer.Meta):
        """Meta class for the contact role serializer."""

        model = ContactRole

    global_provenance = ResourceRelatedField(
        model=GlobalProvenance,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=GlobalProvenance.objects.all(),
        self_link_view_name="contact_roles-relationships",
        related_link_view_name="contact_roles-related",
    )
    successor = HyperlinkedRelatedField(
        many=False,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="contact_roles-relationships",
        related_link_view_name="contact_roles-related",
    )
    included_serializers = {
        "global_provenance": "app.serializers.GlobalProvenanceSerializer",
        "successor": "app.serializers.ContactRoleSerializer",
    }
