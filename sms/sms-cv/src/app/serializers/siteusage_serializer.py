# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Serializer classes for the site usages."""

from rest_framework_json_api.relations import (
    HyperlinkedRelatedField,
    ResourceRelatedField,
)

from app.models import GlobalProvenance, SiteType, SiteUsage
from app.serializers.Base_serializer import BaseSerializer


class SiteUsageSerializer(BaseSerializer):
    """Serializer for the site usage model."""

    class Meta(BaseSerializer.Meta):
        """Meta class forthe site usage serializer."""

        model = SiteUsage

    global_provenance = ResourceRelatedField(
        model=GlobalProvenance,
        many=False,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=GlobalProvenance.objects.all(),
        self_link_view_name="site_usage-relationships",
        related_link_view_name="site_usage-related",
    )
    successor = HyperlinkedRelatedField(
        many=False,
        read_only=True,
        allow_null=True,
        required=False,
        self_link_view_name="site_usage-relationships",
        related_link_view_name="site_usage-related",
    )
    site_types = ResourceRelatedField(
        model=SiteType,
        many=True,
        read_only=False,
        allow_null=True,
        required=False,
        queryset=SiteType.objects.all(),
        self_link_view_name="site_usage-relationships",
        related_link_view_name="site_usage-related",
    )
    included_serializers = {
        "global_provenance": "app.serializers.GlobalProvenanceSerializer",
        "successor": "app.serializers.SiteUsageSerializer",
        "site_types": "app.serializers.SiteTypeSerializer",
    }
