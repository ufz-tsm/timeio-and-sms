# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import PlatformTypeCommunity
from app.serializers.platformtype_community_serializer import (
    PlatformTypeCommunitySerializer,
)
from app.views.Base_viewset import CommunityFilterViewSet


class PlatformTypeCommunityViewSet(CommunityFilterViewSet):
    """
    API endpoint that allows Platform Types of Communities to be viewed or edited.
    """

    queryset = PlatformTypeCommunity.objects.all()
    serializer_class = PlatformTypeCommunitySerializer


class PlatformTypeCommunityRelationshipView(RelationshipView):
    """
    view for relationships.platform_type_community
    """

    queryset = PlatformTypeCommunity.objects
    self_link_view_name = "platform_type_community-relationships"
    http_method_names = ["get"]
