# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Viewsets for the site usage community model."""

from rest_framework_json_api.views import RelationshipView

from app.models import SiteUsageCommunity
from app.serializers.siteusage_community_serializer import SiteUsageCommunitySerializer
from app.views.Base_viewset import CommunityFilterViewSet


class SiteUsageCommunityViewSet(CommunityFilterViewSet):
    """API endpoint that allows Site usages of communities to be viewed or edited."""

    queryset = SiteUsageCommunity.objects.all()
    serializer_class = SiteUsageCommunitySerializer


class SiteUsageCommunityRelationshipView(RelationshipView):
    """View for the relationships of site usage community entries."""

    queryset = SiteUsageCommunity.objects
    self_link_view_name = "site_usage_community-relationships"
    http_method_names = ["get"]
