# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import EquipmentStatusCommunity
from app.serializers.equipmentstatus_community_serializer import (
    EquipmentStatusCommunitySerializer,
)
from app.views.Base_viewset import CommunityFilterViewSet


class EquipmentStatusCommunityViewSet(CommunityFilterViewSet):
    """
    API endpoint that allows EquipmentStatus of Communities to be viewed or edited.
    """

    queryset = EquipmentStatusCommunity.objects.all()
    serializer_class = EquipmentStatusCommunitySerializer


class EquipmentStatusCommunityRelationshipView(RelationshipView):
    """
    view for relationships.equipment_status_community
    """

    queryset = EquipmentStatusCommunity.objects
    self_link_view_name = "equipment_status_community-relationships"
    http_method_names = ["get"]
