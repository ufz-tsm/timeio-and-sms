# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Viewsets for countries."""

from rest_framework_json_api.views import RelationshipView

from app.models import Country
from app.serializers.country_serializer import CountrySerializer

from ..views.Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "iso_code": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class CountryViewSet(BaseFilterViewSet):
    """Modelviewset for countries."""

    queryset = Country.objects
    serializer_class = CountrySerializer
    filterset_fields = filterset_fields


class CountryRelationshipView(RelationshipView):
    """
    view for relationships.country
    """

    queryset = Country.objects
    self_link_view_name = "country-relationships"
    http_method_names = ["get"]
