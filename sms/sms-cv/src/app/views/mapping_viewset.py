# SPDX-FileCopyrightText: 2020 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from ..models import Mapping
from ..serializers.mapping_serializer import MappingSerializer
from .Base_viewset import BaseFilterViewSet, base_filterset_fields


class MappingViewSet(BaseFilterViewSet):
    """
    API endpoint that allows Mapping to be viewed or edited.
    """

    queryset = Mapping.objects.all()
    serializer_class = MappingSerializer
    filterset_fields = base_filterset_fields


class MappingRelationshipView(RelationshipView):
    """
    view for relationships.mapping
    """

    queryset = Mapping.objects
    self_link_view_name = "mapping-relationships"
    http_method_names = ["get"]
