# SPDX-FileCopyrightText: 2020 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import Manufacturer
from app.serializers.manufacturer_serializer import ManufacturerSerializer

from .Base_viewset import BaseFilterViewSet, base_filterset_fields


class ManufacturerViewSet(BaseFilterViewSet):
    """
    API endpoint that allows Manufacturer to be viewed or edited.
    """

    queryset = Manufacturer.objects.all()
    serializer_class = ManufacturerSerializer
    filterset_fields = base_filterset_fields


class ManufacturerRelationshipView(RelationshipView):
    """
    view for relationships.manufacturer
    """

    queryset = Manufacturer.objects
    self_link_view_name = "manufacturer-relationships"
    http_method_names = ["get"]
