# SPDX-FileCopyrightText: 2020 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import Unit
from app.serializers.unit_serializer import UnitSerializer
from app.views.Base_viewset import BaseFilterViewSet, base_filterset_fields


class UnitViewSet(BaseFilterViewSet):
    """
    API endpoint that allows units Types to be viewed or edited.
    """

    queryset = Unit.objects.all()
    serializer_class = UnitSerializer
    filterset_fields = base_filterset_fields


class UnitRelationshipView(RelationshipView):
    """
    view for relationships.unit
    """

    queryset = Unit.objects
    self_link_view_name = "unit-relationships"
    http_method_names = ["get"]
