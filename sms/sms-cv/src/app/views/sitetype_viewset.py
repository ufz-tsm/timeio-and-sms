# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Viewsets for the site types."""

from rest_framework_json_api.views import RelationshipView

from app.models import SiteType
from app.serializers.sitetype_serializer import SiteTypeSerializer

from .Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "site_usage__term": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class SiteTypeViewSet(BaseFilterViewSet):
    """API endpoint that allows site types to be viewed or edited."""

    queryset = SiteType.objects.all()
    serializer_class = SiteTypeSerializer
    filterset_fields = filterset_fields


class SiteTypeRelationshipView(RelationshipView):
    """View for relationships of site type entries."""

    queryset = SiteType.objects
    self_link_view_name = "site_type-relationships"
    http_method_names = ["get"]
