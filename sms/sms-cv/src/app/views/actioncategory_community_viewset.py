# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import ActionCategoryCommunity
from app.serializers.actioncategory_community_serializer import (
    ActionCategoryCommunitySerializer,
)
from app.views.Base_viewset import CommunityFilterViewSet


class ActionCategoryCommunityViewSet(CommunityFilterViewSet):
    """
    API endpoint that allows Action Categories of Communities to be viewed or edited.
    """

    queryset = ActionCategoryCommunity.objects.all()
    serializer_class = ActionCategoryCommunitySerializer


class ActionCategoryCommunityRelationshipView(RelationshipView):
    """
    view for relationships.action_category_community
    """

    queryset = ActionCategoryCommunity.objects
    self_link_view_name = "action_category_community-relationships"
    http_method_names = ["get"]
