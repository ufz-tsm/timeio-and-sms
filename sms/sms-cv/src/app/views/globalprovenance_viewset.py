# SPDX-FileCopyrightText: 2020 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework.filters import SearchFilter
from rest_framework_json_api.django_filters import DjangoFilterBackend
from rest_framework_json_api.filters import (
    OrderingFilter,
    QueryParameterValidationFilter,
)
from rest_framework_json_api.views import RelationshipView

from ..models import GlobalProvenance
from ..serializers.globalprovenance_serializer import GlobalProvenanceSerializer
from .Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "name": text_rels + usual_rels,
    "description": text_rels + usual_rels,
    "uri": text_rels + usual_rels,
    "aggregationtypes__term": text_rels + usual_rels,
    "compartments__term": text_rels + usual_rels,
    "manufacturers__term": text_rels + usual_rels,
    "communitys__term": text_rels + usual_rels,
    "platformtypes__term": text_rels + usual_rels,
    "units__term": text_rels + usual_rels,
}


class GlobalProvenanceViewSet(BaseFilterViewSet):
    """
    API endpoint that allows Global provenance to be viewed or edited.
    """

    queryset = GlobalProvenance.objects.all()
    serializer_class = GlobalProvenanceSerializer

    base_search_fields = ("id", "name", "description", "uri")
    filter_backends = (
        QueryParameterValidationFilter,
        OrderingFilter,
        DjangoFilterBackend,
        SearchFilter,
    )

    filterset_fields = filterset_fields
    search_fields = base_search_fields


class GlobalProvenanceRelationshipView(RelationshipView):
    """
    view for relationships.global_provenance
    """

    queryset = GlobalProvenance.objects
    self_link_view_name = "global_provenance-relationships"
    http_method_names = ["get"]
