# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import SoftwareType
from app.serializers.softwaretype_serializer import SoftwareTypeSerializer

from .Base_viewset import BaseFilterViewSet, base_filterset_fields


class SoftwareTypeViewSet(BaseFilterViewSet):
    """
    API endpoint that allows Software Types to be viewed or edited.
    """

    queryset = SoftwareType.objects.all()
    serializer_class = SoftwareTypeSerializer
    filterset_fields = base_filterset_fields


class SoftwareTypeRelationshipView(RelationshipView):
    """
    view for relationships.software_type
    """

    queryset = SoftwareType.objects
    self_link_view_name = "software_type-relationships"
    http_method_names = ["get"]
