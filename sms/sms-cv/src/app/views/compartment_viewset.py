# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Viewset for the compartments."""

import rdflib
from django.urls import reverse
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework_json_api.renderers import JSONRenderer
from rest_framework_json_api.views import RelationshipView

from app.renderers import RdfRenderer, TtlRenderer

from ..models import Compartment
from ..serializers.compartment_serializer import CompartmentSerializer
from ..views.Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "sampling_media__term": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class CompartmentViewSet(BaseFilterViewSet):
    """API endpoint that allows Compartment to be viewed or edited."""

    queryset = Compartment.objects.all()
    serializer_class = CompartmentSerializer
    renderer_classes = [JSONRenderer, BrowsableAPIRenderer, RdfRenderer, TtlRenderer]
    filterset_fields = filterset_fields

    def add_rdf_relationships(self, graph, data, main_element, renderer_context):
        """Add entries to the knowledge graph that are specific for the compartments."""
        if data.get("sampling_media", []):
            for sm in data.get("sampling_media"):
                sampling_medium_id = sm["id"]
                sampling_medium_uri = renderer_context["request"].build_absolute_uri(
                    reverse("samplingmedium-detail", kwargs={"pk": sampling_medium_id})
                )
                graph.add(
                    (
                        main_element,
                        rdflib.namespace.SKOS.narrower,
                        rdflib.URIRef(sampling_medium_uri),
                    )
                )


class CompartmentRelationshipView(RelationshipView):
    """View for relationships of a compartment."""

    queryset = Compartment.objects
    self_link_view_name = "compartment-relationships"
    http_method_names = ["get"]
