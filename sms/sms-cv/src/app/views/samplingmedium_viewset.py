# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Viewset for the sampling media."""

import rdflib
from django.urls import reverse
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework_json_api.renderers import JSONRenderer
from rest_framework_json_api.views import RelationshipView

from app.models import SamplingMedium
from app.renderers import RdfRenderer, TtlRenderer
from app.serializers.samplingmedium_serializer import SamplingMediumSerializer

from .Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "compartment__term": text_rels + usual_rels,
    "measured_quantities__term": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class SamplingMediumViewSet(BaseFilterViewSet):
    """API endpoint that allows sampling medium Types to be viewed or edited."""

    queryset = SamplingMedium.objects.all()
    serializer_class = SamplingMediumSerializer
    renderer_classes = [JSONRenderer, BrowsableAPIRenderer, RdfRenderer, TtlRenderer]
    filterset_fields = filterset_fields

    def add_rdf_relationships(self, graph, data, main_element, renderer_context):
        """Add entries to the knowledge graph that are specific for the sampling media."""
        if data.get("compartment", {}).get("id", None):
            compartment_id = data["compartment"]["id"]
            compartment_uri = renderer_context["request"].build_absolute_uri(
                reverse("compartment-detail", kwargs={"pk": compartment_id})
            )
            graph.add(
                (
                    main_element,
                    rdflib.namespace.SKOS.broader,
                    rdflib.URIRef(compartment_uri),
                )
            )
        if data.get("measured_quantities", []):
            for mq in data.get("measured_quantities"):
                measured_quantity_id = mq["id"]
                measured_quantity_uri = renderer_context["request"].build_absolute_uri(
                    reverse(
                        "measuredquantity-detail", kwargs={"pk": measured_quantity_id}
                    )
                )
                graph.add(
                    (
                        main_element,
                        rdflib.namespace.SKOS.narrower,
                        rdflib.URIRef(measured_quantity_uri),
                    )
                )


class SamplingMediumRelationshipView(RelationshipView):
    """Wiew for relationships of sampling medium."""

    queryset = SamplingMedium.objects
    self_link_view_name = "sampling_medium-relationships"
    http_method_names = ["get"]
