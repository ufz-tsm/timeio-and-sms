# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Viewsets for licenses."""

from rest_framework_json_api.views import RelationshipView

from app.models import License
from app.serializers.license_serializer import LicenseSerializer

from ..views.Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class LicenseViewSet(BaseFilterViewSet):
    """Modelviewset for licenses."""

    queryset = License.objects
    serializer_class = LicenseSerializer
    filterset_fields = filterset_fields


class LicenseRelationshipView(RelationshipView):
    """View for relationships of licenses."""

    queryset = License.objects
    self_link_view_name = "license-relationships"
    http_method_names = ["get"]
