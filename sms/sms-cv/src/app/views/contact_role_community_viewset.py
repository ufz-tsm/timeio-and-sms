# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Classes for the contact role community viewsets."""

from rest_framework_json_api.views import RelationshipView

from app.models import ContactRoleCommunity
from app.serializers.contact_role_community_serializer import (
    ContactRoleCommunitySerializer,
)
from app.views.Base_viewset import CommunityFilterViewSet


class ContactRoleCommunityViewSet(CommunityFilterViewSet):
    """API endpoint that allows contact roles to be viewed or edited."""

    queryset = ContactRoleCommunity.objects.all()
    serializer_class = ContactRoleCommunitySerializer


class ContactRoleCommunityRelationshipView(RelationshipView):
    """View for the contact role relationship."""

    queryset = ContactRoleCommunity.objects
    self_link_view_name = "contact_roles_community-relationships"
    http_method_names = ["get"]
