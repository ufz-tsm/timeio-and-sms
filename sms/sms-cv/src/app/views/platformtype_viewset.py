# SPDX-FileCopyrightText: 2020 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import PlatformType
from app.serializers.platformtype_serializer import PlatformTypeSerializer

from .Base_viewset import BaseFilterViewSet, base_filterset_fields


class PlatformTypeViewSet(BaseFilterViewSet):
    """
    API endpoint that allows Platform Types to be viewed or edited.
    """

    queryset = PlatformType.objects.all()
    serializer_class = PlatformTypeSerializer
    filterset_fields = base_filterset_fields


class PlatformTypeRelationshipView(RelationshipView):
    """
    view for relationships.platform_type
    """

    queryset = PlatformType.objects
    self_link_view_name = "platform_type-relationships"
    http_method_names = ["get"]
