# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import UnitCommunity
from app.serializers.unit_community_serializer import UnitCommunitySerializer
from app.views.Base_viewset import CommunityFilterViewSet


class UnitCommunityViewSet(CommunityFilterViewSet):
    """
    API endpoint that allows units Types of Communities to be viewed or edited.
    """

    queryset = UnitCommunity.objects.all()
    serializer_class = UnitCommunitySerializer


class UnitCommunityRelationshipView(RelationshipView):
    """
    view for relationships.unit_community
    """

    queryset = UnitCommunity.objects
    self_link_view_name = "unit_community-relationships"
    http_method_names = ["get"]
