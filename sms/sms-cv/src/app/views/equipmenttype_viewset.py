# SPDX-FileCopyrightText: 2020 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from ..models import EquipmentType
from ..serializers.equipmenttype_serializer import EquipmentTypeSerializer
from .Base_viewset import BaseFilterViewSet, base_filterset_fields


class EquipmentTypeViewSet(BaseFilterViewSet):
    """
    API endpoint that allows Equipment Types to be viewed or edited.
    """

    queryset = EquipmentType.objects.all()
    serializer_class = EquipmentTypeSerializer
    filterset_fields = base_filterset_fields


class EquipmentTypeRelationshipView(RelationshipView):
    """
    view for relationships.equipment_type
    """

    queryset = EquipmentType.objects
    self_link_view_name = "equipment_type-relationships"
    http_method_names = ["get"]
