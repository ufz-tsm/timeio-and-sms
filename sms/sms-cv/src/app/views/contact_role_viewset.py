# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Viewsets for the contact roles."""

from rest_framework_json_api.views import RelationshipView

from ..models import ContactRole
from ..serializers.contact_role_serializer import ContactRoleSerializer
from ..views.Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class ContactRoleViewSet(BaseFilterViewSet):
    """API endpoint that allows contact roles to be viewed or edited."""

    queryset = ContactRole.objects.all()
    serializer_class = ContactRoleSerializer
    filterset_fields = filterset_fields


class ContactRoleRelationshipView(RelationshipView):
    """View for relationships contact roles."""

    queryset = ContactRole.objects
    self_link_view_name = "contact_roles-relationships"
    http_method_names = ["get"]
