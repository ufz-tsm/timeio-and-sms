# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models.aggregation_type_community import AggregationTypeCommunity
from app.serializers.aggregationtype_community_serializer import (
    AggregationTypeCommunitySerializer,
)
from app.views.Base_viewset import CommunityFilterViewSet


class AggregationTypeCommunityViewSet(CommunityFilterViewSet):
    """
    API endpoint that allows Aggregation Types of Communities to be viewed or edited.
    """

    queryset = AggregationTypeCommunity.objects.all()
    serializer_class = AggregationTypeCommunitySerializer


class AggregationTypeCommunityRelationshipView(RelationshipView):
    """
    view for relationships.aggregation_type_community
    """

    queryset = AggregationTypeCommunity.objects
    self_link_view_name = "aggregation_type_community-relationships"
    http_method_names = ["get"]
