# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Viewsets for the site usages."""

from rest_framework_json_api.views import RelationshipView

from app.models import SiteUsage
from app.serializers.siteusage_serializer import SiteUsageSerializer

from .Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "site_types__term": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class SiteUsageViewSet(BaseFilterViewSet):
    """API endpoint that allows site usages to be viewed or edited."""

    queryset = SiteUsage.objects.all()
    serializer_class = SiteUsageSerializer
    filterset_fields = filterset_fields


class SiteUsageRelationshipView(RelationshipView):
    """View for relationships of site usage entries."""

    queryset = SiteUsage.objects
    self_link_view_name = "site_usage-relationships"
    http_method_names = ["get"]
