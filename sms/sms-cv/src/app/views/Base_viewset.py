# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""
Base viewset classes.

The settings here affect more or less all the other (real) viewsets.
"""

import datetime

import requests
from django.conf import settings
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.reverse import reverse
from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework_json_api.django_filters import DjangoFilterBackend
from rest_framework_json_api.filters import (
    OrderingFilter,
    QueryParameterValidationFilter,
)
from rest_framework_json_api.views import ModelViewSet

from ..auth.external import (
    HifisDevIdpAccessTokenAuthentification,
    HifisProdIdpAccessTokenAuthentification,
    SkipAuthOnReadOnlyRequestDecorator,
    SmsLocalApiKeyAuthenification,
    UfzIdpAccessTokenAuthentification,
)
from ..permissions import PostRequest, ReadOnly

usual_rels = ("exact", "lt", "gt", "gte", "lte", "in")
# See https://docs.djangoproject.com/en/3.0/ref/models/querysets/#field-lookups
# for all the possible filters.
# icontains: Case-insensitive containment test.
# iexact: Case-insensitive exact match
# contains: Case-sensitive containment test.
# See https://django-rest-framework-json-api.readthedocs.io/en/stable/usage.html#djangofilterbackend
# for how the URL filtering works

text_rels = ("icontains", "iexact", "contains", "isnull")

base_filterset_fields = {
    "id": usual_rels,
    "global_provenance__name": text_rels + usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
}
base_search_fields = (
    "id",
    "term",
    "definition",
    "provenance",
    "provenance_uri",
    "category",
    "status",
    "note",
    "global_provenance__name",
)


class BaseFilterViewSet(NestedViewSetMixin, ModelViewSet):
    """
    Base view set for most of the views.

    This cares about custom filtering options, about permissions
    to query or create entries by using this api - and
    about how the authentication works.
    """

    filter_backends = (
        QueryParameterValidationFilter,
        OrderingFilter,
        DjangoFilterBackend,
        SearchFilter,
    )
    search_fields = base_search_fields

    # The main stuff that we want to allow is querying.
    # So all the readonly calls are file.
    # But we also want the users to send new suggestions - so
    # to create (post) new entries.
    # But we want to check that the users are somehow allowed to do so.
    # We use external authentication for that.
    # All other methods (patch, delete, put) should not be allowed via
    # the api for the moment).
    permission_classes = [ReadOnly | (IsAuthenticated & PostRequest)]
    # As the requests to post will come from the sms, we are going to
    # support the IDPs that we trust for the sms.
    # As it can happen that we want to use the CV from various sms instances
    # in the future, we need to support multiple IDPs (hifis-dev, hifis,
    # ufz, ...)
    #
    # I also added the SMS instance of the docker-compose. It started to be
    # just for fun, but maybe it is possible to use that also in the future,
    # especially for script based processing.
    # TODO: Add or remove auth classes.
    authentication_classes = [
        SmsLocalApiKeyAuthenification,
        HifisProdIdpAccessTokenAuthentification,
        HifisDevIdpAccessTokenAuthentification,
        UfzIdpAccessTokenAuthentification,
    ]

    def get_authenticators(self):
        """Get the authentication mechanisms to try one afer another."""
        # Please note: Our external auth mechanisms run quite long,
        # so we put them in a wrapper that run them only when we need
        # them (for write requests).
        return [
            SkipAuthOnReadOnlyRequestDecorator(auth())
            for auth in self.authentication_classes
        ]

    def perform_create(self, serializer):
        """Run the creation of the model instance."""
        # See `CreateModelMixin` in the django-rest-framework code
        # https://github.com/encode/django-rest-framework/blob/master/rest_framework/mixins.py#L23
        context = serializer.context
        request = context["request"]
        user = request.user
        email = user.email
        # The requested_by_email is an readonly field for the serializer.
        # By setting it in the validated data, we don't need to care about
        # this setting - and we can still save it here.
        serializer.validated_data["requested_by_email"] = email
        # And this is the original call that is done in the "normal"
        # processing in the CreateModelMixin.
        obj = serializer.save()

        if settings.GITLAB_TOKEN:
            # If we have an gitlab token we want to try to create an issues
            # in the gitlab board, so that the persons can discuss about the
            # new proposal, until they accept it - or deny.
            # For that we create an issue with some links into the CV.
            #
            # See https://stackoverflow.com/questions/694477/getting-django-admin-url-for-an-object
            link = reverse(
                "admin:%s_%s_change" % (obj._meta.app_label, obj._meta.model_name),
                args=[obj.id],
                request=request,
            )
            model_name = obj._meta.verbose_name
            term = obj.term
            title = f"Suggestion for new {model_name}: {term}"
            description = "\n\n".join(
                [
                    f"There is a suggestion for a new {model_name} '{term}' by {email}.",
                    f"Check it out at {link}.",
                ]
            )

            due_date = datetime.date.today() + datetime.timedelta(days=28)

            # You can create a token in your repo > settings > access token
            # in the base settings you can see the project id.
            resp = requests.post(
                "https://codebase.helmholtz.cloud/api/v4/projects/3260/issues",
                {
                    "title": title,
                    "description": description,
                    "labels": ",".join(
                        [
                            "SMS-CV",
                            "Suggestion",
                            # Manufacturer / Unit / Equipment type / ...
                            obj._meta.verbose_name.capitalize(),
                        ]
                    ),
                    "due_date": due_date.isoformat(),
                },
                headers={
                    "PRIVATE-TOKEN": settings.GITLAB_TOKEN,
                },
            )
            resp.raise_for_status()


class CommunityFilterViewSet(BaseFilterViewSet):
    """Extended base view set for the community related views."""

    filterset_fields = {
        "id": usual_rels,
        "term": text_rels + usual_rels,
        "abbreviation": text_rels + usual_rels,
        "definition": text_rels + usual_rels,
        "provenance": text_rels + usual_rels,
        "provenance_uri": text_rels + usual_rels,
        "note": text_rels + usual_rels,
        "community__term": text_rels + usual_rels,
        "root__status": text_rels + usual_rels,
        "root__term": text_rels + usual_rels,
    }
    search_fields = (
        "id",
        "term",
        "abbreviation",
        "definition",
        "provenance",
        "provenance_uri",
        "note",
        "community__term",
        "root__status",
        "root__term",
    )
