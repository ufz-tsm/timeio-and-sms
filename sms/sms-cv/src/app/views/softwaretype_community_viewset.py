# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import SoftwareTypeCommunity
from app.serializers.softwaretype_community_serializer import (
    SoftwareTypeCommunitySerializer,
)
from app.views.Base_viewset import CommunityFilterViewSet


class SoftwareTypeCommunityViewSet(CommunityFilterViewSet):
    """
    API endpoint that allows Software Types of Communities to be viewed or edited.
    """

    queryset = SoftwareTypeCommunity.objects.all()
    serializer_class = SoftwareTypeCommunitySerializer


class SoftwareTypeCommunityRelationshipView(RelationshipView):
    """
    view for relationships.software_type_community
    """

    queryset = SoftwareTypeCommunity.objects
    self_link_view_name = "software_type_community-relationships"
    http_method_names = ["get"]
