# SPDX-FileCopyrightText: 2020 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""View set for thr measured quantity."""

import rdflib
from django.urls import reverse
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework_json_api.renderers import JSONRenderer
from rest_framework_json_api.views import RelationshipView

from app.models import MeasuredQuantity
from app.renderers import RdfRenderer, TtlRenderer
from app.serializers.measuredquantity_serializer import MeasuredQuantitySerializer

from .Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "sampling_media__term": text_rels + usual_rels,
    "aggregation_type__term": text_rels + usual_rels,
    "measured_quantity_units__default_limit_min": usual_rels,
    "measured_quantity_units__default_limit_max": usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class MeasuredQuantityViewSet(BaseFilterViewSet):
    """API endpoint that allows MeasuredQuantity to be viewed or edited."""

    queryset = MeasuredQuantity.objects.all()
    serializer_class = MeasuredQuantitySerializer
    # Extended to support RDF (XML) and ttl renderer.
    renderer_classes = [JSONRenderer, BrowsableAPIRenderer, RdfRenderer, TtlRenderer]
    filterset_fields = filterset_fields

    def add_rdf_relationships(self, graph, data, main_element, renderer_context):
        """Add entries to the knowledge graph that are specific for the measured quantities."""
        if data.get("sampling_media", {}).get("id", None):
            sampling_medium_id = data["sampling_media"]["id"]
            sampling_medium_uri = renderer_context["request"].build_absolute_uri(
                reverse("samplingmedium-detail", kwargs={"pk": sampling_medium_id})
            )
            graph.add(
                (
                    main_element,
                    rdflib.namespace.SKOS.broader,
                    rdflib.URIRef(sampling_medium_uri),
                )
            )


class MeasuredQuantityRelationshipView(RelationshipView):
    """View for relationships of measured quantity."""

    queryset = MeasuredQuantity.objects
    self_link_view_name = "measured_quantity-relationships"
    http_method_names = ["get"]
