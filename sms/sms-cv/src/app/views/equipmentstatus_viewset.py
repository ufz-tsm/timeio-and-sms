# SPDX-FileCopyrightText: 2020 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from ..models import EquipmentStatus
from ..serializers.equipmentstatus_serializer import EquipmentStatusSerializer
from .Base_viewset import BaseFilterViewSet, base_filterset_fields


class EquipmentStatusViewSet(BaseFilterViewSet):
    """
    API endpoint that allows EquipmentStatus to be viewed or edited.
    """

    queryset = EquipmentStatus.objects.all()
    serializer_class = EquipmentStatusSerializer
    filterset_fields = base_filterset_fields


class EquipmentStatusRelationshipView(RelationshipView):
    """
    view for relationships.equipment_status
    """

    queryset = EquipmentStatus.objects
    self_link_view_name = "equipment_status-relationships"
    http_method_names = ["get"]
