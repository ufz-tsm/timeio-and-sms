# SPDX-FileCopyrightText: 2020 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework.filters import SearchFilter
from rest_framework_json_api.django_filters import DjangoFilterBackend
from rest_framework_json_api.filters import (
    OrderingFilter,
    QueryParameterValidationFilter,
)
from rest_framework_json_api.views import ModelViewSet, RelationshipView

from app.auth.external import SkipAuthOnReadOnlyRequestDecorator
from app.models import MeasuredQuantityUnit
from app.serializers.measuredquantity_unit_serializer import (
    MeasuredQuantityUnitSerializer,
)

from .Base_viewset import BaseFilterViewSet, text_rels, usual_rels


class MeasuredQuantityUnitViewSet(ModelViewSet):
    """
    API endpoint that allows MeasuredQuantityUnits to be viewed or edited.
    """

    queryset = MeasuredQuantityUnit.objects.all()
    serializer_class = MeasuredQuantityUnitSerializer

    filter_backends = (
        QueryParameterValidationFilter,
        OrderingFilter,
        DjangoFilterBackend,
        SearchFilter,
    )

    base_filterset_fields = {
        "id": usual_rels,
        "measured_quantities__term": text_rels + usual_rels,
        "units__term": text_rels + usual_rels,
        "default_limit_min": usual_rels,
        "default_limit_max": usual_rels,
    }
    base_search_fields = (
        "id",
        "measured_quantity__term",
        "unit__term",
        "default_limit_min",
        "provenance_uri",
        "default_limit_max",
    )
    permission_classes = BaseFilterViewSet.permission_classes
    authentication_classes = BaseFilterViewSet.authentication_classes

    def get_authenticators(self):
        """Get the authentication mechanisms to try one afer another."""
        # Please note: Our external auth mechanisms run quite long,
        # so we put them in a wrapper that run them only when we need
        # them (for write requests).
        return [
            SkipAuthOnReadOnlyRequestDecorator(auth())
            for auth in self.authentication_classes
        ]


class MeasuredQuantityUnitRelationshipView(RelationshipView):
    """
    view for relationships.measured_quantity_unit
    """

    queryset = MeasuredQuantityUnit.objects
    self_link_view_name = "measured_quantity_unit-relationships"
    http_method_names = ["get"]
