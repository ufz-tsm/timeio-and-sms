# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import ManufacturerCommunity
from app.serializers.manufacturer_community_serializer import (
    ManufacturerCommunitySerializer,
)
from app.views.Base_viewset import CommunityFilterViewSet


class ManufacturerCommunityViewSet(CommunityFilterViewSet):
    """
    API endpoint that allows Manufacturer of Communities to be viewed or edited.
    """

    queryset = ManufacturerCommunity.objects.all()
    serializer_class = ManufacturerCommunitySerializer


class ManufacturerCommunityRelationshipView(RelationshipView):
    """
    view for relationships.manufacturer_community
    """

    queryset = ManufacturerCommunity.objects
    self_link_view_name = "manufacturer_community-relationships"
    http_method_names = ["get"]
