# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from ..models import ActionType
from ..serializers.actiontype_serializer import ActionTypeSerializer
from ..views.Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "action_category__term": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class ActionTypeViewSet(BaseFilterViewSet):
    """
    API endpoint that allows Equipment Types to be viewed or edited.
    """

    queryset = ActionType.objects.all()
    serializer_class = ActionTypeSerializer
    filterset_fields = filterset_fields


class ActionTypeRelationshipView(RelationshipView):
    """
    view for relationships.action_type
    """

    queryset = ActionType.objects
    self_link_view_name = "action_type-relationships"
    http_method_names = ["get"]
