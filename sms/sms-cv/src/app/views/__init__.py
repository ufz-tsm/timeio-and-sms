# SPDX-FileCopyrightText: 2020 - 2023
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

# community views
from .actioncategory_community_viewset import (
    ActionCategoryCommunityRelationshipView,
    ActionCategoryCommunityViewSet,
)
from .actioncategory_viewset import (
    ActionCategoryRelationshipView,
    ActionCategoryViewSet,
)
from .actiontype_community_viewset import (
    ActionTypeCommunityRelationshipView,
    ActionTypeCommunityViewSet,
)
from .actiontype_viewset import ActionTypeRelationshipView, ActionTypeViewSet
from .aggregationtype_community_viewset import (
    AggregationTypeCommunityRelationshipView,
    AggregationTypeCommunityViewSet,
)
from .aggregationtype_viewset import (
    AggregationTypeRelationshipView,
    AggregationTypeViewSet,
)
from .community_viewset import CommunityRelationshipView, CommunityViewSet
from .compartment_community_viewset import (
    CompartmentCommunityRelationshipView,
    CompartmentCommunityViewSet,
)
from .compartment_viewset import CompartmentRelationshipView, CompartmentViewSet
from .contact_role_community_viewset import (
    ContactRoleCommunityRelationshipView,
    ContactRoleCommunityViewSet,
)
from .contact_role_viewset import ContactRoleRelationshipView, ContactRoleViewSet
from .country_viewset import CountryRelationshipView, CountryViewSet
from .equipmentstatus_community_viewset import (
    EquipmentStatusCommunityRelationshipView,
    EquipmentStatusCommunityViewSet,
)
from .equipmentstatus_viewset import (
    EquipmentStatusRelationshipView,
    EquipmentStatusViewSet,
)
from .equipmenttype_community_viewset import (
    EquipmentTypeCommunityRelationshipView,
    EquipmentTypeCommunityViewSet,
)
from .equipmenttype_viewset import EquipmentTypeRelationshipView, EquipmentTypeViewSet
from .globalprovenance_viewset import (
    GlobalProvenanceRelationshipView,
    GlobalProvenanceViewSet,
)
from .license_viewset import LicenseRelationshipView, LicenseViewSet
from .manufacturer_community_viewset import (
    ManufacturerCommunityRelationshipView,
    ManufacturerCommunityViewSet,
)
from .manufacturer_viewset import ManufacturerRelationshipView, ManufacturerViewSet
from .measuredquantity_community_viewset import (
    MeasuredQuantityCommunityRelationshipView,
    MeasuredQuantityCommunityViewSet,
)
from .measuredquantity_unit_viewset import (
    MeasuredQuantityUnitRelationshipView,
    MeasuredQuantityUnitViewSet,
)
from .measuredquantity_viewset import (
    MeasuredQuantityRelationshipView,
    MeasuredQuantityViewSet,
)
from .platformtype_community_viewset import (
    PlatformTypeCommunityRelationshipView,
    PlatformTypeCommunityViewSet,
)
from .platformtype_viewset import PlatformTypeRelationshipView, PlatformTypeViewSet
from .samplingmedium_community_viewset import (
    SamplingMediumCommunityRelationshipView,
    SamplingMediumCommunityViewSet,
)
from .samplingmedium_viewset import (
    SamplingMediumRelationshipView,
    SamplingMediumViewSet,
)
from .sitetype_community_viewset import (
    SiteTypeCommunityRelationshipView,
    SiteTypeCommunityViewSet,
)
from .sitetype_viewset import SiteTypeRelationshipView, SiteTypeViewSet
from .siteusage_community_viewset import (
    SiteUsageCommunityRelationshipView,
    SiteUsageCommunityViewSet,
)
from .siteusage_viewset import SiteUsageRelationshipView, SiteUsageViewSet
from .softwaretype_community_viewset import (
    SoftwareTypeCommunityRelationshipView,
    SoftwareTypeCommunityViewSet,
)
from .softwaretype_viewset import SoftwareTypeRelationshipView, SoftwareTypeViewSet
from .unit_community_viewset import UnitCommunityRelationshipView, UnitCommunityViewSet
from .unit_viewset import UnitRelationshipView, UnitViewSet
