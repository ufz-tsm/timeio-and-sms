# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Viewsets for the site type community model."""

from rest_framework_json_api.views import RelationshipView

from app.models import SiteTypeCommunity
from app.serializers.sitetype_community_serializer import SiteTypeCommunitySerializer
from app.views.Base_viewset import CommunityFilterViewSet


class SiteTypeCommunityViewSet(CommunityFilterViewSet):
    """API endpoint that allows Site types of communities to be viewed or edited."""

    queryset = SiteTypeCommunity.objects.all()
    serializer_class = SiteTypeCommunitySerializer


class SiteTypeCommunityRelationshipView(RelationshipView):
    """View for the relationships of site type community entries."""

    queryset = SiteTypeCommunity.objects
    self_link_view_name = "site_type_community-relationships"
    http_method_names = ["get"]
