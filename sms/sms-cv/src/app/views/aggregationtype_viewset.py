# SPDX-FileCopyrightText: 2020 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from ..models.aggregation_type import AggregationType
from ..serializers.aggregationtype_serializer import AggregationTypeSerializer
from ..views.Base_viewset import BaseFilterViewSet, text_rels, usual_rels

filterset_fields = {
    "id": usual_rels,
    "term": text_rels + usual_rels,
    "definition": text_rels + usual_rels,
    "provenance": text_rels + usual_rels,
    "provenance_uri": text_rels + usual_rels,
    "category": text_rels + usual_rels,
    "status": text_rels + usual_rels,
    "note": text_rels + usual_rels,
    "measured_quantities__term": text_rels + usual_rels,
    "global_provenance__name": text_rels + usual_rels,
}


class AggregationTypeViewSet(BaseFilterViewSet):
    """
    API endpoint that allows Aggregation Types to be viewed or edited.
    """

    queryset = AggregationType.objects.all()
    serializer_class = AggregationTypeSerializer
    filterset_fields = filterset_fields


class AggregationTypeRelationshipView(RelationshipView):
    """
    view for relationships.aggregation_type
    """

    queryset = AggregationType.objects
    self_link_view_name = "aggregation_type-relationships"
    http_method_names = ["get"]
