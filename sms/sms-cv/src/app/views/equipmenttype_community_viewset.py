# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import EquipmentTypeCommunity
from app.serializers.equipmenttype_community_serializer import (
    EquipmentTypeCommunitySerializer,
)
from app.views.Base_viewset import CommunityFilterViewSet


class EquipmentTypeCommunityViewSet(CommunityFilterViewSet):
    """
    API endpoint that allows Equipment Types of Communities to be viewed or edited.
    """

    queryset = EquipmentTypeCommunity.objects.all()
    serializer_class = EquipmentTypeCommunitySerializer


class EquipmentTypeCommunityRelationshipView(RelationshipView):
    """
    view for relationships.equipment_type_community
    """

    queryset = EquipmentTypeCommunity.objects
    self_link_view_name = "equipment_type_community-relationships"
    http_method_names = ["get"]
