# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import SamplingMediumCommunity
from app.serializers.samplingmedium_community_serializer import (
    SamplingMediumCommunitySerializer,
)
from app.views.Base_viewset import CommunityFilterViewSet


class SamplingMediumCommunityViewSet(CommunityFilterViewSet):
    """
    API endpoint that allows sampling medium Types of Communities to be viewed or edited.
    """

    queryset = SamplingMediumCommunity.objects.all()
    serializer_class = SamplingMediumCommunitySerializer


class SamplingMediumCommunityRelationshipView(RelationshipView):
    """
    view for relationships.sampling_medium_community
    """

    queryset = SamplingMediumCommunity.objects
    self_link_view_name = "sampling_medium_community-relationships"
    http_method_names = ["get"]
