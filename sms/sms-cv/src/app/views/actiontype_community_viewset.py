# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from rest_framework_json_api.views import RelationshipView

from app.models import ActionTypeCommunity
from app.serializers.actiontype_community_serializer import (
    ActionTypeCommunitySerializer,
)
from app.views.Base_viewset import CommunityFilterViewSet


class ActionTypeCommunityViewSet(CommunityFilterViewSet):
    """
    API endpoint that allows Equipment Types of Communities to be viewed or edited.
    """

    queryset = ActionTypeCommunity.objects.all()
    serializer_class = ActionTypeCommunitySerializer


class ActionTypeCommunityRelationshipView(RelationshipView):
    """
    view for relationships.action_type
    """

    queryset = ActionTypeCommunity.objects
    self_link_view_name = "action_type_community-relationships"
    http_method_names = ["get"]
