# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Classes to handle authorization with external services."""

import abc

import requests
from rest_framework import authentication
from rest_framework.permissions import SAFE_METHODS


class AbstractExternalUser(abc.ABC):
    """Abstract base class to represent a user from an external system."""

    @property
    def is_authenticated(self):
        """
        Return always true for the authentication test.

        Either we have a concrete user (then we have an instance).
        Or we don't have a user & the auth mechanisms return None.
        """
        return True

    @property
    @abc.abstractmethod
    def email(self):
        """Return the email of the user."""
        pass


# You may ask why there are multiple (fake) user models here.
# Explanation is in so far simple as the various systems return different
# data - both in what information they provide & how they structure it.
# We have different classes here in case we need to return or store some
# information about those users in the further process. This way we
# can share an interface, but use different implementations to get on
# the user data (email for example).


class IdpUser(AbstractExternalUser):
    """User in an idp system."""

    def __init__(self, userinfo):
        """Init the object."""
        self.userinfo = userinfo

    def __eq__(self, other):
        """Return true if both are equal."""
        if type(other) is not type(self):
            return False
        return self.userinfo == other.userinfo

    @property
    def email(self):
        """Return the email from the userinfo."""
        return self.userinfo["email"]


class SmsUser(AbstractExternalUser):
    """User in the sms."""

    def __init__(self, userinfo):
        """Init the object."""
        self.userinfo = userinfo

    def __eq__(self, other):
        """Return true if both are equal."""
        if type(other) is not type(self):
            return False
        return self.userinfo == other.userinfo

    @property
    def email(self):
        """Return the subject for the email."""
        return self.userinfo["attributes"]["subject"]


class AbstractIdpAccessTokenAuthentification(authentication.BaseAuthentication):
    """
    Authentification mechanism to ask an IDP based on a token.

    We check here if we get an token that we can send to one of the IDPs
    that we trust.
    If the token is validated there, we can aggree that we really have
    a user here that we can use in the further process.

    You may want to check out:
    https://www.django-rest-framework.org/api-guide/authentication/#custom-authentication
    in order to understand what we do here.
    """

    def __init__(self):
        """Init the object."""
        self.__idp_config = None

    def idp_config(self):
        """
        Return the idp config.

        This method is lazy & makes sure that we don't have to ask the IDP
        for its config over and over again.
        """
        if self.__idp_config:
            return self.__idp_config
        resp_idp_config = requests.get(self.well_known_url)
        resp_idp_config.raise_for_status()

        self.__idp_config = resp_idp_config.json()
        return self.__idp_config

    def authenticate(self, request):
        """Check if we can authenticate a user with our idp."""
        headers = request.headers
        authorization_header = headers.get("Authorization")
        if not authorization_header:
            return None
        if not authorization_header.startswith("Bearer "):
            return None

        try:
            userinfo_endpoint = self.idp_config()["userinfo_endpoint"]
            resp_userinfo = requests.get(
                userinfo_endpoint, headers={"Authorization": authorization_header}
            )
            resp_userinfo.raise_for_status()

            data_userinfo = resp_userinfo.json()
            return (IdpUser(data_userinfo), None)
        except Exception:
            return None


class HifisProdIdpAccessTokenAuthentification(AbstractIdpAccessTokenAuthentification):
    """IDP access token authentication with the productive IDP by HIFIS."""

    # TODO: Maybe put it into the settings file & read it from the env variables.
    well_known_url = (
        "https://login.helmholtz.de/oauth2/.well-known/openid-configuration"
    )


# You may ask, why dev & prod - and not just one.
# Thing here is that we may want to use the CV for multiple sms instances.
# We could also use it for sms staging or development versions.
# So, the dev idp should be supported as well.
class HifisDevIdpAccessTokenAuthentification(AbstractIdpAccessTokenAuthentification):
    """IDP access token authentication with the dev IDP by HIFIS."""

    # TODO: Maybe put it into the settings file & read it from the env variables.
    well_known_url = (
        "https://login-dev.helmholtz.de/oauth2/.well-known/openid-configuration"
    )


class UfzIdpAccessTokenAuthentification(AbstractIdpAccessTokenAuthentification):
    """IDP access token authentication with the productive IDP by UFZ."""

    # TODO: Maybe put it into the settings file & read it from the env variables.
    well_known_url = (
        "https://idc.ufz.de/realms/webapp-base-extern/.well-known/openid-configuration"
    )


class SmsLocalApiKeyAuthenification(authentication.BaseAuthentication):
    """Authentification mechanism that uses the apikeys of the sms."""

    # Honestly, I created it just for fun in the first time.
    # However, when I think about it, it could make sense to have an
    # option to provide support for scripting with those apikeys that
    # we have for the sms anyway.

    # TODO: If wanted, it should be an env variable.
    # Also please note: I think it is very specific for my (nils) setting
    # here in the orchestration repo.
    # It should consider the path variables that we have for the sms backend.
    #
    # Please also note as well that this communication is for the dev
    # version of the sms orchestration container intern - so there
    # is an url that points to the backend container & not to localhost.
    # For a deployed sms version it should not be problem to use the
    # real domain name of the sms instance.
    #
    # Also the question is - if we want it to - which sms instances to support.
    # With 128 chars there is not a large risk for double apikeys between
    # different instances. But maybe on has to think about it.
    userinfo_endpoint = "http://backend:5000/backend/rdm/svm-api/v1/user-info"

    def authenticate(self, request):
        """Authenticate with an apikey."""
        headers = request.headers
        apikey_header = headers.get("X-APIKEY")
        if not apikey_header:
            return None
        try:
            resp = requests.get(
                self.userinfo_endpoint,
                headers={"X-APIKEY": apikey_header},
            )
            resp.raise_for_status()

            return (SmsUser(resp.json()["data"]), None)
        except Exception:
            return None


class SkipAuthOnReadOnlyRequestDecorator(authentication.BaseAuthentication):
    """Decorator to skip long running auth mechanism for read methods."""

    def __init__(self, inner_auth):
        """Init the object with an implementation with real logic."""
        super().__init__()
        self.inner_auth = inner_auth

    def authenticate(self, request):
        """Run the authentification only if write access is wanted."""
        # It is a little bit mixed (auth & permissions), but this
        # will give us the chance to have it just as an decorator,
        # while the "normal" auth can be used as it is.
        #
        # It is intended to be used in an get_authenticators
        # method call to wrap the existing authentification classes.
        #
        # This should be a specific implementation for a view, so
        # that the decision if auth needs to be done, is done there
        # (and not in a "normal" authentification class).
        #
        # See https://github.com/encode/django-rest-framework/blob/master/rest_framework/views.py#L268
        if request.method not in SAFE_METHODS:
            return self.inner_auth.authenticate(request)
        return None
