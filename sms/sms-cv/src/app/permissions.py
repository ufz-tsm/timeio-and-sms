# SPDX-FileCopyrightText: 2022
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Custom permissions for our views."""

from rest_framework.permissions import SAFE_METHODS, BasePermission


class ReadOnly(BasePermission):
    """
    This class here allows readonly requests.

    You can also find the snippet for this code here:
    https://www.django-rest-framework.org/api-guide/permissions/
    """

    def has_permission(self, request, view):
        """Return true if it is a readonly (get, head, option) request."""
        return request.method in SAFE_METHODS


class PostRequest(BasePermission):
    """Similar class to the ReadOnly permission, but allows only POST requests."""

    def has_permission(self, request, view):
        """Return true if it is a post request."""
        return request.method == "POST"
