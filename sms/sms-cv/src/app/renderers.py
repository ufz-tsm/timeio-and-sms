# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Additional classes to render our content."""

import rdflib
from rest_framework import renderers

from .utils import kw_alias


class GraphMixin:
    """Mixin class to transform the serializer output into a knowledge graph."""

    def convert_to_graph(self, data, renderer_context):
        """Convert the serializer output data into a graph."""
        graph = rdflib.Graph()
        if "results" in data.keys():
            for element in data["results"]:
                self.add(element, into=graph, renderer_context=renderer_context)
        else:
            self.add(data, into=graph, renderer_context=renderer_context)

        return graph

    @kw_alias({"into": "graph"})
    def add(self, data, graph, renderer_context):
        """Insert the data into the graph."""
        url = data["url"]
        if url.endswith(".xml") or url.endswith(".ttl"):
            url = url[:-4]

        if not url.endswith("/"):
            url += "/"

        main_element = rdflib.URIRef(url)

        graph.add(
            (main_element, rdflib.namespace.RDF.type, rdflib.namespace.SKOS.Concept)
        )
        if data.get("term"):
            graph.add(
                (
                    main_element,
                    rdflib.namespace.SKOS.prefLabel,
                    rdflib.Literal(data["term"], lang="en"),
                )
            )
        if data.get("definition"):
            graph.add(
                (
                    main_element,
                    rdflib.namespace.SKOS.definition,
                    rdflib.Literal(data["definition"], lang="en"),
                )
            )

        if data.get("provenance_uri"):
            graph.add(
                (
                    main_element,
                    rdflib.namespace.SKOS.exactMatch,
                    rdflib.URIRef(data["provenance_uri"]),
                )
            )

        renderer_context["view"].add_rdf_relationships(
            graph, data, main_element, renderer_context
        )


class RdfRenderer(GraphMixin, renderers.BaseRenderer):
    """Renderer that can trnasform the serializer output into RDF in XML encoding."""

    media_type = "application/rdf+xml"
    format = "xml"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """Return the content as xml."""
        graph = self.convert_to_graph(data, renderer_context=renderer_context)
        result = graph.serialize(format=self.format)
        return result


class TtlRenderer(GraphMixin, renderers.BaseRenderer):
    """Renderer that can transform the serializer output into ttl."""

    media_type = "application/x-turtle"
    format = "ttl"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """Return the content as ttl."""
        graph = self.convert_to_graph(data, renderer_context=renderer_context)
        result = graph.serialize(format=self.format)
        return result
