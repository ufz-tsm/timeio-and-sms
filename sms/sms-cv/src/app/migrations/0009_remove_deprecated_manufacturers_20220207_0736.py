# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""
Custom migration to handle Manufacturer changes.

In https://gitlab.hzdr.de/hub-terra/sms/sms-cv/-/merge_requests/59
we need to delete manufacturers.
"""

from django.db import migrations


def forwards_func(apps, schema_editor):
    ManufacturerCommunity = apps.get_model("app", "ManufacturerCommunity")
    Manufacturer = apps.get_model("app", "Manufacturer")
    ids_to_delete = [4, 64]
    for manufacturer_community in ManufacturerCommunity.objects.filter(
        root__in=ids_to_delete
    ):
        manufacturer_community.delete()
    for manufacturer in Manufacturer.objects.filter(id__in=ids_to_delete):
        manufacturer.delete()


def reverse_func(app, schema_editor):
    Manufacturer = apps.get_model("app", "Manufacturer")
    Community = apps.get_model("app", "Community")
    ManufacturerCommunity = apps.get_model("app", "ManufacturerCommunity")
    GlobalProvenance = apps.get_model("app", "GlobalProvenance")

    gp2 = GlobalProvenance.objects.get(pk=2)
    gp3 = GlobalProvenance.objects.get(pk=3)

    default_community = Community.objects.get(pk=1)

    hobo = Manufacturer.objects.create(
        pk=4,
        category=None,
        definition=None,
        global_provenance=gp2,
        note=None,
        provenance=None,
        provenance_uri=None,
        status="ACCEPTED",
        successor=None,
        # Term here is not that simple.
        # Before  https://gitlab.hzdr.de/hub-terra/sms/sms-cv/-/merge_requests/59
        # it was "Hobo", but there should be the change to "Onset Computer Corporation".
        # However Onset is already part of the database (& in use) for another entry.
        term="Hobo",
    )
    ugt = Manufacturer.objects.create(
        pk=64,
        category=None,
        definition=None,
        global_provenance=gp3,
        note=None,
        provenance=None,
        provenance_uri=None,
        status="ACCEPTED",
        successor=None,
        term="Umwelt-Geräte-Technik",
    )
    ManufacturerCommunity.objects.create(
        pk=4,
        root=hobo,
        community=default_community,
        abbreviation=None,
        definition=None,
        note=None,
        provenance=None,
        provenance_uri=None,
        term=None,
    )
    ManufacturerCommunity.objects.create(
        pk=64,
        root=ugt,
        community=default_community,
        abbreviation=None,
        definition=None,
        note=None,
        provenance=None,
        provenance_uri=None,
        term=None,
    )


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0008_remove_deprecated_unit_relationships_20220222_1337"),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
