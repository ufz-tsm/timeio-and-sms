# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""
Custom migration to handle MeasuredQuantityUnit changes.

In https://gitlab.hzdr.de/hub-terra/sms/sms-cv/-/merge_requests/62
we need to delete the assignments of units to measured quantities,
as we split the pressure into two measured quantities (relativ vs absolute).

Due to this change we need to remove the relative units from the
measured quantity for the absolute pressure.
"""

from django.db import migrations


def forwards_func(apps, schema_editor):
    MeasuredQuantityUnit = apps.get_model("app", "MeasuredQuantityUnit")
    ids_to_delete = [189, 190]
    for mqu in MeasuredQuantityUnit.objects.filter(id__in=ids_to_delete):
        mqu.delete()


def reverse_func(app, schema_editor):
    MeasuredQuantityUnit = apps.get_model("app", "MeasuredQuantityUnit")
    MeasuredQuantity = apps.get_model("app", "MeasuredQuantity")
    Unit = apps.get_model("app", "Unit")

    m106 = MeasuredQuantity.objects.get(pk=106)

    u5 = Unit.objects.get(pk=5)
    u3 = Unit.objects.get(pk=3)

    MeasuredQuantityUnit.objects.create(
        pk=189,
        default_limit_max=None,
        default_limit_min=None,
        measured_quantity=m106,
        unit=u5,
    )
    MeasuredQuantityUnit.objects.create(
        pk=190,
        default_limit_max=None,
        default_limit_min=None,
        measured_quantity=m106,
        unit=u3,
    )


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0007_auto_20210608_1051"),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
