# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

import copy
from json import dump, load
from shutil import copyfile

# mapping of root and corresponding community model to be considered for default
community_models = {
    "actioncategory": "actioncategorycommunity",
    "actiontype": "actiontypecommunity",
    "aggregationtype": "aggregationtypecommunity",
    "compartment": "compartmentcommunity",
    "equipmentstatus": "equipmentstatuscommunity",
    "equipmenttype": "equipmenttypecommunity",
    "manufacturer": "manufacturercommunity",
    "measuredquantity": "measuredquantitycommunity",
    "platformtype": "platformtypecommunity",
    "samplingmedium": "samplingmediumcommunity",
    "softwaretype": "softwaretypecommunity",
    "unit": "unitcommunity",
    "contactrole": "contactrolecommunity",
}
# primary key of default community
default_pk = 1

# attributes to not be overwritten by initial_data.json
ignore_fields = [
    # attributes to be None/NULL
    "term",
    "definition",
    "provenance",
    "provenance_uri",
    "category",
    "note",
    "status",
    "global_provenance",
    "successor",
    # currently not existing in root models
    "abbreviation",
    # attributes already being assigned by function
    "root",
    "community",
]


# util function to create default community fixture based on initial_data.json
def create_defaultcommunityfixture(
    path="./app/fixtures/",
    community="default_community",
    models=community_models,
    community_pk=default_pk,
    ignore_fields=ignore_fields,
):
    # copy original fixtures file to be used as base
    copyfile(f"{path}initial_data.json", f"{path}{community}_data.json")
    # get fixtures object
    fixtures_file = open(f"{path}{community}_data.json", "rb")
    fixtures_object = load(fixtures_file)
    fixtures_file.close()
    # iterate through entries and modify
    for entry in fixtures_object:
        if community == "default_community":
            root = entry["pk"]
        else:
            root = copy.deepcopy(entry["pk"])
            # !! note that this workaround may lead to duplicates in the db !!
            # entry["pk"] = None
        model_name = entry["model"].replace("app.", "")
        # use provided mapping to check for required data
        if model_name in models and entry["fields"]["status"] == "ACCEPTED":
            # corresponding community model
            entry["model"] = f"app.{models[model_name]}"
            # specify fields data - mostly empty to provide default as tag
            new_fields = {
                "term": None,
                "abbreviation": None,
                "definition": None,
                "provenance": None,
                "provenance_uri": None,
                "note": None,
                "root": root,
                "community": community_pk,
            }
            for field in entry["fields"]:
                if field not in ignore_fields:
                    new_fields[field] = entry["fields"][field]
            entry["fields"] = new_fields
        else:
            # delete non included community model data
            del entry
    # overwrite original (simply copied) and prettify data in json
    fixtures_file = open(f"{path}{community}_data.json", "w")
    dump(fixtures_object, fixtures_file, indent=4, sort_keys=True)
    fixtures_file.close()


# apply function if called
if __name__ == "__main__":
    create_defaultcommunityfixture()
