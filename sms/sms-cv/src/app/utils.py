# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Utility functions for various usages."""

import functools


def kw_alias(aliases):
    """Decorate a function to use keywords with a different wording - a bit like in SmallTalk.

    The idea can be shown in the best way in a kind of example:

    @kw_alias({
        "the_string": "value_to_be_replaced",
        "with_value": "replacement",
        "in_text": "text",
    })
    def replace(text, value_to_be_replaced, replacement):
        return text.replace(value_to_be_replaced, replacement)

    and the usage:

    replace(the_string="foo", with_value="bar", in_text="example value is foo")

    Used properly, it can help to improve the readability of the code.
    But be aware that it comes with some small runtime cost.
    """

    def inner(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            changed_kwargs = {}
            for k, v in kwargs.items():
                changed_kwargs[aliases.get(k, k)] = v
            result = f(*args, **changed_kwargs)
            return result

        return wrapper

    return inner
