# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.db import models

from app.models.controlled_vocabulary_community import ControlledVocabularyCommunity
from app.models.software_type import SoftwareType


class SoftwareTypeCommunity(ControlledVocabularyCommunity):

    root = models.ForeignKey(
        SoftwareType, models.DO_NOTHING, related_name="software_types_communities"
    )

    class Meta(ControlledVocabularyCommunity.Meta):
        db_table = "software_type_community"
        unique_together = ("root", "community")
        verbose_name_plural = "software types communities"
