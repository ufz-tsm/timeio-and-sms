# SPDX-FileCopyrightText: 2020 - 2024
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.db import models
from simple_history.models import HistoricalRecords

from app.models.measured_quantity import MeasuredQuantity
from app.models.unit import Unit


class MeasuredQuantityUnit(models.Model):
    id = models.AutoField(primary_key=True)
    measured_quantity = models.ForeignKey(
        MeasuredQuantity, models.DO_NOTHING, related_name="measured_quantity_units"
    )
    unit = models.ForeignKey(
        Unit, models.DO_NOTHING, related_name="measured_quantity_units"
    )

    default_limit_min = models.DecimalField(
        max_digits=19, decimal_places=10, blank=True, null=True
    )
    default_limit_max = models.DecimalField(
        max_digits=19, decimal_places=10, blank=True, null=True
    )
    history = HistoricalRecords()

    def __str__(self):
        return "{quantity} [{unit}]".format(
            quantity=self.measured_quantity.term, unit=self.unit.term
        )

    class Meta:
        db_table = "measured_quantity_unit"
        unique_together = (("measured_quantity", "unit"),)
        ordering = ["measured_quantity__term", "unit__term"]
