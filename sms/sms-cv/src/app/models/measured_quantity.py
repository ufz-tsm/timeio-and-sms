# SPDX-FileCopyrightText: 2020 - 2022
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.db import models

from app.models.aggregation_type import AggregationType
from app.models.controlled_vocabulary import ControlledVocabulary, Successor
from app.models.sampling_medium import SamplingMedium


class MeasuredQuantity(ControlledVocabulary, Successor):

    sampling_media = models.ForeignKey(
        SamplingMedium,
        models.DO_NOTHING,
        related_name="measured_quantities",
    )
    aggregation_type = models.ForeignKey(
        AggregationType,
        models.DO_NOTHING,
        related_name="measured_quantities",
    )

    class Meta(Successor.Meta):
        db_table = "measured_quantity"
        unique_together = ("term", "sampling_media")
        verbose_name_plural = "measured quantities"
