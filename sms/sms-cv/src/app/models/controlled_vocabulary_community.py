# SPDX-FileCopyrightText: 2021 - 2024
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.db import models
from simple_history.models import HistoricalRecords

from app.models.community import Community


class ControlledVocabularyCommunity(models.Model):
    """
    Community version of Controlled Vocabulary
    """

    term = models.CharField(max_length=255, null=True, blank=True)
    abbreviation = models.CharField(max_length=255, blank=True, null=True)
    definition = models.TextField(blank=True, null=True)
    provenance = models.CharField(max_length=255, blank=True, null=True)
    provenance_uri = models.CharField(max_length=255, blank=True, null=True)
    note = models.TextField(blank=True, null=True)

    community = models.ForeignKey(Community, models.DO_NOTHING)
    history = HistoricalRecords(inherit=True)

    def __str__(self):
        if self.term:
            return self.term
        else:
            return self.root.term

    class Meta:
        ordering = ["root__term", "term"]
        abstract = True
