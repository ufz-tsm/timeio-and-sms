# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Models for the contact role communities."""
from django.db import models

from app.models.contact_role import ContactRole
from app.models.controlled_vocabulary_community import ControlledVocabularyCommunity


class ContactRoleCommunity(ControlledVocabularyCommunity):
    """Community class for the contact roles."""

    root = models.ForeignKey(
        ContactRole, models.DO_NOTHING, related_name="contact_role_communities"
    )

    class Meta(ControlledVocabularyCommunity.Meta):
        """Meta class for the contact role comminity model."""

        db_table = "contact_roles_community"
        unique_together = (("root", "community"),)
        verbose_name_plural = "contact role communities"
