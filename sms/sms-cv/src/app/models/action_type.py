# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.db import models

from .action_category import ActionCategory
from .controlled_vocabulary import ControlledVocabulary, Successor


class ActionType(ControlledVocabulary, Successor):
    action_category = models.ForeignKey(
        ActionCategory, models.DO_NOTHING, related_name="action_types"
    )

    class Meta(Successor.Meta):
        db_table = "action_type"
        unique_together = ("term", "action_category")
