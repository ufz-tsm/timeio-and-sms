# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Contact role model."""
from app.models.controlled_vocabulary import ControlledVocabulary, Successor


class ContactRole(ControlledVocabulary, Successor):
    """Contact role model (data scientist, service provider, ...)."""

    class Meta(Successor.Meta):
        """Meta class for the contact role model."""

        db_table = "contact_role"
        verbose_name = "Contact role"
        verbose_name_plural = "Contact roles"
