# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Model classes for the site type community entries."""

from django.db import models

from app.models.controlled_vocabulary_community import ControlledVocabularyCommunity
from app.models.site_type import SiteType


class SiteTypeCommunity(ControlledVocabularyCommunity):
    """Community entries for the sites."""

    root = models.ForeignKey(
        SiteType, models.DO_NOTHING, related_name="site_types_communities"
    )

    class Meta(ControlledVocabularyCommunity.Meta):
        """Meta class for the site type community model."""

        db_table = "site_type_community"
        unique_together = (("root", "community"),)
        verbose_name_plural = "site types communities"
