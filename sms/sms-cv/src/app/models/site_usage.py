# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Model classes for the site usages."""
from app.models.controlled_vocabulary import ControlledVocabulary, Successor


class SiteUsage(ControlledVocabulary, Successor):
    """Model class for the site usage."""

    class Meta(Successor.Meta):
        """Meta class for the site usage model."""

        db_table = "site_usage"
