# SPDX-FileCopyrightText: 2021 - 2022
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.db import models

from app.models.controlled_vocabulary_community import ControlledVocabularyCommunity
from app.models.manufacturer import Manufacturer


class ManufacturerCommunity(ControlledVocabularyCommunity):

    root = models.ForeignKey(
        Manufacturer, models.DO_NOTHING, related_name="manufacturers_communities"
    )

    class Meta(ControlledVocabularyCommunity.Meta):
        db_table = "manufacturer_community"
        unique_together = (("root", "community"),)
