# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Model classes for the site types."""
from django.db import models

from app.models.controlled_vocabulary import ControlledVocabulary, Successor
from app.models.site_usage import SiteUsage


class SiteType(ControlledVocabulary, Successor):
    """Model class for the site types."""

    site_usage = models.ForeignKey(
        SiteUsage,
        models.DO_NOTHING,
        related_name="site_types",
    )

    class Meta(Successor.Meta):
        """Meta class for the site type model."""

        unique_together = ("term", "site_usage")
        db_table = "site_type"
