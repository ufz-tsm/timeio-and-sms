# SPDX-FileCopyrightText: 2020 - 2021
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.db import models

from app.models.compartment import Compartment
from app.models.controlled_vocabulary import ControlledVocabulary, Successor


class SamplingMedium(ControlledVocabulary, Successor):

    compartment = models.ForeignKey(
        Compartment,
        models.DO_NOTHING,
        related_name="sampling_media",
    )

    class Meta(Successor.Meta):
        db_table = "sampling_medium"
        unique_together = ("term", "compartment")
        verbose_name_plural = "sampling media"
