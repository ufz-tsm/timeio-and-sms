# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from app.models.controlled_vocabulary import ControlledVocabulary, Successor


class SoftwareType(ControlledVocabulary, Successor):
    class Meta(Successor.Meta):
        db_table = "software_type"
