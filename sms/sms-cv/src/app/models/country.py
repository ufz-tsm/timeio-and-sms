# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Model for a list of countries."""

from django.db import models

from app.models.controlled_vocabulary import ControlledVocabulary, Successor


class Country(ControlledVocabulary, Successor):
    """Country model."""

    iso_code = models.CharField(max_length=3, blank=False, null=False)

    class Meta(Successor.Meta):
        """Meta class for the country model."""

        verbose_name_plural = "Countries"
        db_table = "country"
