# SPDX-FileCopyrightText: 2020 - 2024
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Luca Johannes Nendel <luca-johannes.nendel@ufz.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from django.db import models
from django.db.models import CheckConstraint, F, Q
from simple_history.models import HistoricalRecords

from app.models.global_provenance import GlobalProvenance


class ControlledVocabulary(models.Model):
    """
    Abstract model with common fields for all other Models:
    By default, Django gives each model the following field:
    id = models.AutoField(primary_key=True)
    """

    STATUS_CHOICES = (
        ("PENDING", "Pending"),
        ("REJECTED", "Rejected"),
        ("ACCEPTED", "Accepted"),
        ("ARCHIVED", "Archived"),
        ("UPDATED", "Updated"),
    )

    term = models.CharField(max_length=255)
    definition = models.TextField(blank=True, null=True)
    provenance = models.CharField(max_length=255, blank=True, null=True)
    provenance_uri = models.CharField(max_length=255, blank=True, null=True)
    category = models.CharField(max_length=255, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    status = models.CharField(
        max_length=255, choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0]
    )
    global_provenance = models.ForeignKey(
        GlobalProvenance,
        related_name="%(class)ss",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    requested_by_email = models.CharField(
        max_length=512, blank=True, null=True, verbose_name="Requested by"
    )
    history = HistoricalRecords(inherit=True)

    def __str__(self):
        return self.term

    class Meta:
        abstract = True


class Successor(models.Model):
    successor = models.ForeignKey("self", models.DO_NOTHING, blank=True, null=True)

    class Meta:
        ordering = ["term"]
        abstract = True
        constraints = [
            CheckConstraint(
                name="%(class)s_successor_not_a_reference_to_self",
                check=~Q(successor_id=F("id")),
            )
        ]
