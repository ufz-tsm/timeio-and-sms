# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

"""Model for a list of licenses."""

from app.models.controlled_vocabulary import ControlledVocabulary, Successor


class License(ControlledVocabulary, Successor):
    """License model."""

    class Meta(Successor.Meta):
        """Meta class for the license model."""

        db_table = "license"
