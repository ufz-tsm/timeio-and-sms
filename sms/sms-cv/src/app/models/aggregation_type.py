# SPDX-FileCopyrightText: 2020
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from app.models.controlled_vocabulary import ControlledVocabulary, Successor


class AggregationType(ControlledVocabulary, Successor):
    class Meta(Successor.Meta):
        db_table = "aggregation_type"
