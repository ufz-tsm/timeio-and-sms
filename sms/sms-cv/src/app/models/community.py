# SPDX-FileCopyrightText: 2021 - 2022
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

from app.models.controlled_vocabulary import ControlledVocabulary


class Community(ControlledVocabulary):
    class Meta:
        db_table = "community"
        ordering = ["term"]
        verbose_name_plural = "communities"
