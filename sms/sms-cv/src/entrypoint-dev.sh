#!/bin/sh

# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

set -e

apk add --no-cache --virtual .build-deps \
    gcc \
    python3-dev \
    musl-dev \
    postgresql-dev

pip install --upgrade pip
pip install -r requirements.txt

./manage.py migrate
./manage.py loaddata initial_data.json
./manage.py loaddata default_community_data.json
./manage.py loaddata hydro_community_data.json
./manage.py collectstatic --noinput
./manage.py populate_history --auto
exec gunicorn --timeout 1000 --workers 1 cv.wsgi:application --bind 0:8000 --reload --log-level debug
