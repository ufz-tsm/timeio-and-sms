# SPDX-FileCopyrightText: 2020 - 2022
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

import django.views.static
from cv import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path
from django.urls import path

urlpatterns = [
    re_path(
        r"^{cv_base_url}api/v1/".format(cv_base_url=settings.CV_BASE_URL),
        include("app.urls"),
    ),
    re_path(
        r"^{cv_base_url}api-auth/v1/".format(cv_base_url=settings.CV_BASE_URL),
        include("rest_framework.urls"),
    ),
    path("{cv_base_url}".format(cv_base_url=settings.CV_BASE_URL), admin.site.urls),
]
urlpatterns += staticfiles_urlpatterns()

urlpatterns += [
    re_path(
        r"^static/(?P<path>.*)$",
        django.views.static.serve,
        {"document_root": settings.STATIC_ROOT, "show_indexes": settings.DEBUG},
    )
]
