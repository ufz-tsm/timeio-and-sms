# SPDX-FileCopyrightText: 2020 - 2024
# - Martin Abbrent <martin.abbrent@ufz.de>
# - Kotyba Alhaj Taha <kotyba.alhaj-taha@ufz.de>
# - Jannes Breier <jannes.breier@gfz-potsdam.de>
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
# - Helmholtz Centre for Environmental Research GmbH - UFZ (UFZ, https://www.ufz.de)
#
# SPDX-License-Identifier: HEESIL-1.0

import os

import environ
from django.utils.html import format_html

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False),
    GITLAB_TOKEN=(str, ""),
    SQL_ENGINE=(str, "django.db.backends.postgresql"),
    SQL_DATABASE=(str, "postgres"),
    SQL_USER=(str, "postgres"),
    SQL_PASSWORD=(str, "postgres"),
    SQL_HOST=(str, "localhost"),
    PORT=(int, 5432),
    CORS_ORIGIN_WHITELIST=(tuple, ()),
    CSRF_TRUSTED_ORIGINS=(tuple, ()),
    STATIC_URL=(str, ""),
    CACHE_TIMEOUT=(int, 600),
    CACHE_MAX_ENTRIES=(int, 10000),
)
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
# Raises django's ImproperlyConfigured exception if SECRET_KEY not in os.environ
SECRET_KEY = env("SECRET_KEY")

# 'DJANGO_ALLOWED_HOSTS' should be a single string of hosts with a space between each.
# For example: 'DJANGO_ALLOWED_HOSTS=localhost 127.0.0.1 [::1]'
# ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS").split(" ")

DEBUG = env("DEBUG")

ALLOWED_HOSTS = ["*"]

# Specify a list of origin hostnames that are authorized to make a cross-site HTTP request

CORS_ORIGIN_REGEX_WHITELIST = env.list("CORS_ORIGIN_WHITELIST")
CSRF_TRUSTED_ORIGINS = env.list("CSRF_TRUSTED_ORIGINS")

# Application definition

INSTALLED_APPS = [
    "jazzmin",
    "django.contrib.admin",
    # 'app.admin.adjustedAdminSite',  # replaces 'django.contrib.admin'
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "rest_framework",
    "drf_yasg",
    "corsheaders",
    "django_filters",
    "app",
    "simple_history",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    # For the caching middleware see
    # https://docs.djangoproject.com/en/4.0/topics/cache/#the-per-site-cache-1
    "django.middleware.cache.UpdateCacheMiddleware",
    "django.middleware.cache.FetchFromCacheMiddleware",
    # We do not use the HistoryRequestMiddleware as this is incompatible
    # with the user model we use when we get suggestions via api and
    # post requests (it tries to set the user that made the change,
    # while there is no such entry in the user table).
    #"simple_history.middleware.HistoryRequestMiddleware",
]

ROOT_URLCONF = "cv.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "cv.wsgi.application"

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": env("SQL_ENGINE"),
        "NAME": env("SQL_DATABASE"),
        "USER": env("SQL_USER"),
        "PASSWORD": env("SQL_PASSWORD"),
        "HOST": env("SQL_HOST"),
        "PORT": env.int("SQL_PORT"),
    }
}


# Gitlab token to create issues
GITLAB_TOKEN = env("GITLAB_TOKEN")

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

# Configure a base URL to run the app in subdirectories like http://example.com/subdirectory/
CV_BASE_URL = env("CV_BASE_URL", default="")
STATIC_URL = "/static/"
if env("STATIC_URL", default=""):
    STATIC_URL = env("STATIC_URL")

STATIC_ROOT = f"{BASE_DIR}/static"
if env("STATIC_ROOT", default=""):
    STATIC_ROOT = env("STATIC_ROOT")

# Trust tse the hostname given by the reverse proxy in front of the app
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

# configuration for REST framework

REST_FRAMEWORK = {
    "PAGE_SIZE": 10,
    "MAX_PAGE_SIZE": 10000,
    "DEFAULT_SCHEMA_CLASS": "rest_framework.schemas.coreapi.AutoSchema",
    "EXCEPTION_HANDLER": "rest_framework_json_api.exceptions.exception_handler",
    "DEFAULT_PAGINATION_CLASS": "app.pagination.LargePagination",
    "DEFAULT_PARSER_CLASSES": (
        "rest_framework_json_api.parsers.JSONParser",
        "rest_framework.parsers.FormParser",
        "rest_framework.parsers.MultiPartParser",
    ),
    "DEFAULT_RENDERER_CLASSES": (
        "rest_framework_json_api.renderers.JSONRenderer",  # application/vnd.apijson
        "rest_framework.renderers.BrowsableAPIRenderer",  # text/html: ?format=api
    ),
    "DEFAULT_METADATA_CLASS": "rest_framework_json_api.metadata.JSONAPIMetadata",
    "DEFAULT_FILTER_BACKENDS": (
        # Makes sure only valid {json:api} query parameters are provided
        "rest_framework_json_api.filters.QueryParameterValidationFilter",
        # Implements sorting
        "rest_framework_json_api.filters.OrderingFilter",
        # for `filter[field]` filtering
        "rest_framework_json_api.django_filters.DjangoFilterBackend",
        # for keyword filtering across multiple fields
        "rest_framework.filters.SearchFilter",
    ),
    "SEARCH_PARAM": "filter[search]",
    "TEST_REQUEST_RENDERER_CLASSES": (
        "rest_framework_json_api.renderers.JSONRenderer",
    ),
    "TEST_REQUEST_DEFAULT_FORMAT": "vnd.api+json",
}

# debug logging
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_true": {
            "()": "django.utils.log.RequireDebugTrue",
        }
    },
    "formatters": {
        "verbose": {"format": "%(asctime)s %(message)s"},
        "simple": {"format": "%(levelname)s %(message)s"},
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "filters": ["require_debug_true"],
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        }
    },
    "loggers": {
        "django.db.backends": {
            "level": "DEBUG",
            "handlers": ["console"],
        },
        "app": {
            "level": "INFO",
            "handlers": ["console"],
        },
    },
}
# Extending drg-yasg configuration
# https://pypi.org/project/drf-yasg-json-api/
SWAGGER_SETTINGS = {
    "DEFAULT_AUTO_SCHEMA_CLASS": "drf_yasg_json_api.inspectors.SwaggerAutoSchema",  # Overridden
    "DEFAULT_FIELD_INSPECTORS": [
        "drf_yasg_json_api.inspectors.NamesFormatFilter",  # Replaces CamelCaseJSONFilter
        "drf_yasg.inspectors.RecursiveFieldInspector",
        "drf_yasg_json_api.inspectors.XPropertiesFilter",  # Added
        "drf_yasg_json_api.inspectors.JSONAPISerializerSmartInspector",  # Added
        "drf_yasg.inspectors.ReferencingSerializerInspector",
        "drf_yasg_json_api.inspectors.IntegerIDFieldInspector",  # Added
        "drf_yasg.inspectors.ChoiceFieldInspector",
        "drf_yasg.inspectors.FileFieldInspector",
        "drf_yasg.inspectors.DictFieldInspector",
        "drf_yasg.inspectors.JSONFieldInspector",
        "drf_yasg.inspectors.HiddenFieldInspector",
        "drf_yasg_json_api.inspectors.ManyRelatedFieldInspector",  # Added
        "drf_yasg_json_api.inspectors.IntegerPrimaryKeyRelatedFieldInspector",  # Added
        "drf_yasg.inspectors.RelatedFieldInspector",
        "drf_yasg.inspectors.SerializerMethodFieldInspector",
        "drf_yasg.inspectors.SimpleFieldInspector",
        "drf_yasg.inspectors.StringDefaultFieldInspector",
    ],
    "DEFAULT_FILTER_INSPECTORS": [
        "drf_yasg_json_api.inspectors.DjangoFilterInspector",
        # Added (optional), requires django_filter
        "drf_yasg.inspectors.CoreAPICompatInspector",
    ],
    "DEFAULT_PAGINATOR_INSPECTORS": [
        "drf_yasg_json_api.inspectors.DjangoRestResponsePagination",  # Added
        "drf_yasg.inspectors.DjangoRestResponsePagination",
        "drf_yasg.inspectors.CoreAPICompatInspector",
    ],
}
# During testing Django forces DEBUG = False
DEBUG_TOOLBAR = False
if env.bool("DEBUG_TOOLBAR", default=False):
    DEBUG_TOOLBAR = env("DEBUG_TOOLBAR")
# Can override using the debug toolbar here
if DEBUG_TOOLBAR:
    TEMPLATE_DEBUG = True
    INTERNAL_IPS = [
        "127.0.0.1",
    ]
    MIDDLEWARE += ("debug_toolbar.middleware.DebugToolbarMiddleware",)
    INSTALLED_APPS += ["debug_toolbar"]
    DEBUG_TOOLBAR_PANELS = [
        # 'debug_toolbar.panels.versions.VersionsPanel',
        "debug_toolbar.panels.timer.TimerPanel",
        "debug_toolbar.panels.settings.SettingsPanel",
        "debug_toolbar.panels.headers.HeadersPanel",
        "debug_toolbar.panels.request.RequestPanel",
        "debug_toolbar.panels.sql.SQLPanel",
        "debug_toolbar.panels.staticfiles.StaticFilesPanel",
        "debug_toolbar.panels.templates.TemplatesPanel",
        "debug_toolbar.panels.cache.CachePanel",
        "debug_toolbar.panels.signals.SignalsPanel",
        "debug_toolbar.panels.logging.LoggingPanel",
        "debug_toolbar.panels.redirects.RedirectsPanel",
        "debug_toolbar.panels.profiling.ProfilingPanel",
    ]
    DEBUG_TOOLBAR_CONFIG = {
        "SHOW_TOOLBAR_CALLBACK": lambda request: True,
    }

JAZZMIN_SETTINGS = {
    # title of the window (Will default to current_admin_site.site_title if absent or None)
    "site_title": "SMS Vocabulary",
    # Title on the brand, and login screen (19 chars max) (defaults to current_admin_site.site_header if absent or None)
    "site_header": format_html("SMS Vocabulary"),
    # Relative path to a favicon for your site, will default to site_logo if absent (ideally 32x32 px)
    "site_icon": None,
    # Welcome text on the login screen
    "welcome_sign": format_html(
        "Welcome to the Admin Interface<br>for the <b>Controlled Vocabulary</b><br>of the Sensor Management System<br><b>SMS</b>"
    )
    # https://django-jazzmin.readthedocs.io/configuration.html
}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

CACHE_TIMEOUT = env.int("CACHE_TIMEOUT")
CACHE_MAX_ENTRIES = env.int("CACHE_MAX_ENTRIES")
# See https://docs.djangoproject.com/en/4.0/topics/cache/#filesystem-caching
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.filebased.FileBasedCache",
        "LOCATION": "/tmp/sms-cv-cache",
        "TIMEOUT": CACHE_TIMEOUT,
        "OPTIONS": {
            "MAX_ENTRIES": CACHE_MAX_ENTRIES,
        },
    },
}
