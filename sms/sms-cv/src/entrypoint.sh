#!/bin/sh

# SPDX-FileCopyrightText: 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: HEESIL-1.0

set -e

if ! [ -x "$(command -v nproc)" ]; then
    CPUS=2
else
    CPUS=$(nproc)
fi
WORKERS=$(( 2 * $CPUS + 1 ))

./manage.py migrate
./manage.py populate_history --auto
exec gunicorn --workers=$WORKERS cv.wsgi:application --bind 0:8000
